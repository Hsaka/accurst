import Utils from 'utils/utils';
import GameCalculations from 'utils/gameCalculations';

export default class SkillHandler {
  constructor() {}

  static getSkillAction(
    id,
    apply,
    targetManager,
    target,
    instigatorManager,
    instigator
  ) {
    var damage = 0;
    var result = '';
    //TODO: Add skill functionality here
    var skill = Utils.GlobalSettings.skillsList[id];
    switch (id) {
      case 0: //Stun
        if (skill.status_effect && skill.status_effect.length > 0) {
          result = skill.status_effect;
        }
        break;

      case 3: //Slow
        damage = Math.ceil(target.speed / 2);
        if (damage < 1) {
          damage = 1;
        }
        result = 'Speed -' + damage;
        if (skill.status_effect && skill.status_effect.length > 0) {
          result += '\n' + skill.status_effect;
        }

        if (apply) {
          if (targetManager) {
            targetManager.reduceSpeed(damage, target);
          }
        }
        break;

      case 4: //Weaken
        damage = Math.ceil(target.power / 2);
        if (damage < 1) {
          damage = 1;
        }
        result = 'Power -' + damage;
        if (skill.status_effect && skill.status_effect.length > 0) {
          result += '\n' + skill.status_effect;
        }

        if (apply) {
          if (targetManager) {
            targetManager.reducePower(damage, target);
          }
        }
        break;

      case 5: //Slash
        damage = GameCalculations.calculateDamage(
          skill,
          instigator,
          target,
          apply
        );
        result = 'HP -' + damage;
        if (skill.status_effect && skill.status_effect.length > 0) {
          result += '\n' + skill.status_effect;
        }
        if (apply) {
          if (targetManager) {
            targetManager.reduceHealth(damage, target);
            if (skill.status_effect && skill.status_effect.length > 0) {
              targetManager.applyStatusEffect(
                skill.status_effect,
                skill.status_effect_duration,
                target
              );
            }
          }
        }
        break;

      default:
        damage = GameCalculations.calculateDamage(
          skill,
          instigator,
          target,
          apply
        );
        result = 'HP -' + damage;
        if (apply) {
          if (targetManager) {
            targetManager.reduceHealth(damage, target);
          }
        }
        break;
    }

    return {
      damage: damage,
      result: result,
    };
  }
}
