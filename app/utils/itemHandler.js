import Utils from 'utils/utils';

export default class ItemHandler {
  constructor() {}

  static getItemAction(id, apply, targetManager, target) {
    var damage = 0;
    var result = '';
    //TODO: Add item functionality here
    switch (id) {
      case 0: //Small Health Potion
        damage = 10;
        result = 'HP +' + damage;
        if (apply) {
          if (targetManager) {
            targetManager.increaseHealth(damage, target);
          }
        }
        break;

      case 1: //Medium Health Potion
        damage = 20;
        result = 'HP +' + damage;
        if (apply) {
          if (targetManager) {
            targetManager.increaseHealth(damage, target);
          }
        }
        break;

      case 2: //Large Health Potion
        damage = 50;
        result = 'HP +' + damage;
        if (apply) {
          if (targetManager) {
            targetManager.increaseHealth(damage, target);
          }
        }
        break;

      case 3: //Small Magic Potion
        damage = 5;
        result = 'MP +' + damage;
        if (apply) {
          if (targetManager) {
            targetManager.increaseMagic(damage, target);
          }
        }
        break;

      case 4: //Medium Magic Potion
        damage = 15;
        result = 'MP +' + damage;
        if (apply) {
          if (targetManager) {
            targetManager.increaseMagic(damage, target);
          }
        }
        break;

      case 5: //Large Magic Potion
        damage = 40;
        result = 'MP +' + damage;
        if (apply) {
          if (targetManager) {
            targetManager.increaseMagic(damage, target);
          }
        }
        break;

      case 6: //Small Power Potion
        damage = 5;
        result = 'POW +' + damage;
        if (apply) {
          if (targetManager) {
            targetManager.increasePower(damage, target);
          }
        }
        break;

      case 7: //Medium Power Potion
        damage = 20;
        result = 'POW +' + damage;
        if (apply) {
          if (targetManager) {
            targetManager.increasePower(damage, target);
          }
        }
        break;

      case 8: //Large Power Potion
        damage = 50;
        result = 'POW +' + damage;
        if (apply) {
          if (targetManager) {
            targetManager.increasePower(damage, target);
          }
        }
        break;
    }

    return {
      damage: damage,
      result: result,
    };
  }
}
