import Utils from 'utils/utils';

export default class GameCalculations {
  constructor() {}

  static calculateExperienceGain(level) {
    return Utils.getEXP(level, 2.7);
  }

  static calculateGoldGain(level) {
    return Utils.getCalculatedValue(level, 1000, 2.6);
  }

  static calculateHealthGain(level, max = 200) {
    return Utils.getCalculatedValue(level, max, 1.5);
  }

  static calculateMagicGain(level, max = 150) {
    return Utils.getCalculatedValue(level, max, 2.0);
  }

  static calculatePowerGain(level, max = 250) {
    return Utils.getCalculatedValue(level, max, 1.8);
  }

  static calculateSpeedGain(level, max = 100) {
    return Utils.getCalculatedValue(level, max, 2.5);
  }

  static increaseExperience(player, amount, expRate) {
    player.experience += amount;
    if (player.experience >= player.requiredExperience) {
      player.experience = 0;
      player.level++;
      player.levelUp = true;
      player.requiredExperience = Utils.getEXP(player.level + 1, expRate);
    }
  }

  static getMaxHealth(entity) {
    var entityWeapon = Utils.GlobalSettings.weaponsList[entity.weapon];
    var hpBoost = entityWeapon.health_boost;
    var weaponMultiplier = 1;
    var entityProficiency = entity.weaponProficiency;
    if (
      entityProficiency &&
      entityProficiency.length > 0 &&
      entityProficiency !== entityWeapon.type
    ) {
      weaponMultiplier = 0.5;
    }
    hpBoost = Math.floor(hpBoost * weaponMultiplier);

    return entity.maxHealth + hpBoost;
  }

  static getMaxMagic(entity) {
    var entityWeapon = Utils.GlobalSettings.weaponsList[entity.weapon];
    var mpBoost = entityWeapon.magic_boost;
    var weaponMultiplier = 1;
    var entityProficiency = entity.weaponProficiency;
    if (
      entityProficiency &&
      entityProficiency.length > 0 &&
      entityProficiency !== entityWeapon.type
    ) {
      weaponMultiplier = 0.5;
    }
    mpBoost = Math.floor(mpBoost * weaponMultiplier);

    return entity.maxMagic + mpBoost;
  }

  static getMaxPower(entity) {
    var entityWeapon = Utils.GlobalSettings.weaponsList[entity.weapon];
    var powBoost = entityWeapon.power_boost;
    var weaponMultiplier = 1;
    var entityProficiency = entity.weaponProficiency;
    if (
      entityProficiency &&
      entityProficiency.length > 0 &&
      entityProficiency !== entityWeapon.type
    ) {
      weaponMultiplier = 0.5;
    }
    powBoost = Math.floor(powBoost * weaponMultiplier);

    return entity.maxPower + powBoost;
  }

  static getSpeed(entity) {
    var entityWeapon = Utils.GlobalSettings.weaponsList[entity.weapon];
    var spdBoost = entityWeapon.speed_boost;
    var weaponMultiplier = 1;
    var entityProficiency = entity.weaponProficiency;
    if (
      entityProficiency &&
      entityProficiency.length > 0 &&
      entityProficiency !== entityWeapon.type
    ) {
      weaponMultiplier = 0.5;
    }
    spdBoost = Math.floor(spdBoost * weaponMultiplier);

    return entity.speed + spdBoost;
  }

  static getStatusComparison(statusEffect, currentStatus) {
    if (statusEffect && statusEffect.length > 0) {
      if (currentStatus === Utils.GlobalSettings.text.battle.status_normal) {
        currentStatus = statusEffect;
      } else {
        //stunned > poisoned > bleeding > slowed > weakened
        switch (statusEffect) {
          case Utils.GlobalSettings.text.battle.status_stunned:
            currentStatus = Utils.GlobalSettings.text.battle.status_stunned;
            break;

          case Utils.GlobalSettings.text.battle.status_poisoned:
            if (
              currentStatus !== Utils.GlobalSettings.text.battle.status_stunned
            ) {
              currentStatus = Utils.GlobalSettings.text.battle.status_poisoned;
            }
            break;

          case Utils.GlobalSettings.text.battle.status_bleeding:
            if (
              currentStatus !==
                Utils.GlobalSettings.text.battle.status_stunned &&
              currentStatus !== Utils.GlobalSettings.text.battle.status_poisoned
            ) {
              currentStatus = Utils.GlobalSettings.text.battle.status_bleeding;
            }
            break;

          case Utils.GlobalSettings.text.battle.status_slowed:
            if (
              currentStatus !==
                Utils.GlobalSettings.text.battle.status_stunned &&
              currentStatus !==
                Utils.GlobalSettings.text.battle.status_poisoned &&
              currentStatus !== Utils.GlobalSettings.text.battle.status_bleeding
            ) {
              currentStatus = Utils.GlobalSettings.text.battle.status_slowed;
            }
            break;

          case Utils.GlobalSettings.text.battle.status_weakened:
            if (
              currentStatus !==
                Utils.GlobalSettings.text.battle.status_stunned &&
              currentStatus !==
                Utils.GlobalSettings.text.battle.status_poisoned &&
              currentStatus !==
                Utils.GlobalSettings.text.battle.status_bleeding &&
              currentStatus !== Utils.GlobalSettings.text.battle.status_slowed
            ) {
              currentStatus = Utils.GlobalSettings.text.battle.status_weakened;
            }
            break;
        }
      }
    }

    return currentStatus;
  }

  static getStatus(entity) {
    var entityWeapon = Utils.GlobalSettings.weaponsList[entity.weapon];
    var statusEffect = entityWeapon.status_effect;
    var currentStatus = entity.status;

    var resultStatus = this.getStatusComparison(statusEffect, currentStatus);

    return resultStatus;
  }

  static getRandomStatus() {
    var statuses = [
      Utils.GlobalSettings.text.battle.status_stunned,
      Utils.GlobalSettings.text.battle.status_poisoned,
      Utils.GlobalSettings.text.battle.status_bleeding,
      Utils.GlobalSettings.text.battle.status_slowed,
      Utils.GlobalSettings.text.battle.status_weakened,
    ];

    return Utils.GlobalSettings.rnd.pick(statuses);
  }

  static calculateDamage(action, instigator, target, apply) {
    if (action.direct_damage) {
      if (action.level_scaled_damage) {
        return action.damage * instigator.level;
      } else {
        return action.damage;
      }
    } else {
      var instigatorWeapon =
        Utils.GlobalSettings.weaponsList[instigator.weapon];
      var weaponDamage = instigatorWeapon.damage_boost;
      var weaponDamageMultiplier = 1;
      var instigatorProficiency = instigator.weaponProficiency;
      if (
        instigatorProficiency &&
        instigatorProficiency.length > 0 &&
        instigatorProficiency !== instigatorWeapon.type
      ) {
        weaponDamageMultiplier = 0.5;
      }
      weaponDamage = Math.floor(weaponDamage * weaponDamageMultiplier);

      var powerDamage =
        (instigator.power +
          instigator.powerBonus +
          instigatorWeapon.power_boost) /
        4;
      var damage = Math.ceil(action.damage + powerDamage + weaponDamage);

      if (action.level_scaled_damage) {
        damage = damage * instigator.level;
      }

      var targetWeapon = Utils.GlobalSettings.weaponsList[target.weapon];
      var weaponDefense = targetWeapon.defense_boost;
      var weaponDefenseMultiplier = 1;
      var targetProficiency = target.weaponProficiency;
      if (
        targetProficiency &&
        targetProficiency.length > 0 &&
        targetProficiency !== targetWeapon.type
      ) {
        weaponDefenseMultiplier = 0.5;
      }
      weaponDefense = Math.floor(weaponDefense * weaponDefenseMultiplier);

      var defense = target.defenseBonus + weaponDefense;

      if (apply) {
        if (target.defenseBonus > 0) {
          target.defenseBonus -= damage;
          if (target.defenseBonus < 0) {
            target.defenseBonus = 0;
          }
        }
      }

      var afterDefense = damage - defense;
      if (afterDefense < 0) {
        afterDefense = 0;
      }
      return afterDefense;
    }
  }

  static getItemReward(amount) {
    //TODO: Get items for floor/level
    var floor = Utils.GlobalSettings.gameState.floor;
    var items = [];

    for (
      var i = 0;
      i < Object.keys(Utils.GlobalSettings.itemsList).length;
      i++
    ) {
      items.push(i);
    }

    items = Utils.GlobalSettings.rnd.shuffle(items);

    if (amount > items.length) {
      amount = items.length;
    }

    var reward = [];
    for (var i = 0; i < amount; i++) {
      reward.push(items[i]);
    }

    return reward;
  }

  static getWeaponReward(chestType, currentWeaponID, weaponProficiency) {
    var floor = Utils.GlobalSettings.gameState.floor;
    var currentWeapon = Utils.GlobalSettings.weaponsList[currentWeaponID];
    var weapons = [];

    for (
      var i = 0;
      i < Object.keys(Utils.GlobalSettings.weaponsList).length;
      i++
    ) {
      var wep = Utils.GlobalSettings.weaponsList[i];
      if (wep.type === weaponProficiency || wep.type === 'none') {
        switch (chestType) {
          case 'green_chest':
            if (
              wep.level >= currentWeapon.level - 1 &&
              wep.level <= currentWeapon.level + 1
            ) {
              weapons.push(i);
            }
            break;
          case 'blue_chest':
            if (
              wep.level >= currentWeapon.level &&
              wep.level <= currentWeapon.level + 2
            ) {
              weapons.push(i);
            }
            break;
          case 'red_chest':
            if (
              wep.level >= currentWeapon.level &&
              wep.level <= currentWeapon.level + 3
            ) {
              weapons.push(i);
            }
            break;
        }
      }
    }

    weapons = Utils.GlobalSettings.rnd.shuffle(weapons);
    return weapons[0];
  }
}
