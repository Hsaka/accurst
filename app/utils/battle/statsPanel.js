import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import GameCalculations from 'utils/gameCalculations';

export default class StatsPanel {
  constructor(scene) {
    this.scene = scene;
  }

  setupStatsPanel() {
    this.statsPanelBg = UIHelper.createDialog(
      this.scene,
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      500,
      600,
      'dialog9patch2',
      15
    );
    this.statsPanelBg.alpha = 0;
    this.statsPanelBg.visible = false;

    this.statsPanelIcon = this.scene.add.image(
      this.statsPanelBg.x - this.statsPanelBg.width / 2 + 20,
      this.statsPanelBg.y - this.statsPanelBg.height / 2 + 20,
      'atlas1',
      'hero_knight'
    );
    this.statsPanelIcon.setScale(0.5);
    this.statsPanelIcon.alpha = 0;
    this.statsPanelIcon.visible = false;

    this.statsPanelTitle = this.scene.add.bitmapText(
      this.statsPanelBg.x,
      this.statsPanelBg.y - this.statsPanelBg.height / 2 + 40,
      'sakkalbold',
      '',
      60
    );
    this.statsPanelTitle.setOrigin(0.5);
    this.statsPanelTitle.alpha = 0;
    this.statsPanelTitle.visible = false;

    this.statsPanelStats = this.scene.add.bitmapText(
      this.statsPanelTitle.x,
      this.statsPanelTitle.y + 40,
      'sakkalbold',
      '',
      40
    );
    this.statsPanelStats.alpha = 0;
    this.statsPanelStats.setOrigin(0.5, 0);
    this.statsPanelStats.setCenterAlign();
    this.statsPanelStats.visible = false;

    this.statsPanelCancelButton = UIHelper.createMenuButton(
      this.scene,
      'menubutton',
      this.statsPanelBg.y + this.statsPanelBg.height / 2 + 20,
      Utils.GlobalSettings.text.battle.close,
      false
    );
    this.statsPanelCancelButton.visible = false;
    this.statsPanelCancelButton.on('pointerup', () => {
      this.hideStatsPanel();
    });
  }

  toggleStatsPanelVisibility(visible) {
    this.statsPanelBg.visible = visible;
    this.statsPanelIcon.visible = visible;
    this.statsPanelTitle.visible = visible;
    this.statsPanelStats.visible = visible;
    this.statsPanelCancelButton.visible = visible;
    this.statsPanelCancelButton.buttonTxt.visible = visible;
  }

  toggleStatsPanelAlpha(alpha) {
    this.statsPanelBg.alpha = alpha;
    this.statsPanelIcon.alpha = alpha;
    this.statsPanelTitle.alpha = alpha;
    this.statsPanelStats.alpha = alpha;
    this.statsPanelCancelButton.alpha = alpha;
    this.statsPanelCancelButton.buttonTxt.alpha = alpha;
  }

  hideStatsPanel() {
    this.scene.bgOverlay.alpha = 0.8;
    this.scene.bgOverlay.visible = true;

    this.toggleStatsPanelAlpha(1);
    this.toggleStatsPanelVisibility(true);

    this.scene.tweens.add({
      targets: [this.scene.bgOverlay],
      alpha: { from: 0.8, to: 0 },
      ease: 'Linear.easeOut',
      duration: 100,
      onComplete: () => {
        this.scene.bgOverlay.visible = false;
      }
    });

    this.scene.tweens.add({
      targets: [
        this.statsPanelBg,
        this.statsPanelIcon,
        this.statsPanelTitle,
        this.statsPanelStats,
        this.statsPanelCancelButton,
        this.statsPanelCancelButton.buttonTxt
      ],
      alpha: { from: 1, to: 0 },
      ease: 'Linear.easeOut',
      duration: 100,
      onComplete: () => {
        this.toggleStatsPanelVisibility(false);
      }
    });
  }

  showStatsPanel() {
    this.toggleStatsPanelAlpha(0);
    this.toggleStatsPanelVisibility(true);

    var endsWithS = UIHelper.endsWithS(this.scene.currentPlayer.name);

    this.statsPanelTitle.setText(
      endsWithS
        ? this.scene.currentPlayer.name +
            "'  " +
            Utils.GlobalSettings.text.battle.stats
        : this.scene.currentPlayer.name +
            "'s  " +
            Utils.GlobalSettings.text.battle.stats
    );

    this.statsPanelIcon.setFrame(
      this.scene.battleUI.playerPanel.playerIcon.frame.name
    );

    this.statsPanelStats.setText(
      Utils.GlobalSettings.text.battle.level +
        ':  ' +
        this.scene.currentPlayer.level +
        '\n' +
        Utils.GlobalSettings.text.battle.experience +
        ':  ' +
        this.scene.currentPlayer.experience +
        '/' +
        this.scene.currentPlayer.requiredExperience +
        '\n' +
        Utils.GlobalSettings.text.battle.health +
        ':  ' +
        this.scene.currentPlayer.health +
        '/' +
        GameCalculations.getMaxHealth(this.scene.currentPlayer) +
        '\n' +
        Utils.GlobalSettings.text.battle.power +
        ':  ' +
        this.scene.currentPlayer.power +
        '/' +
        GameCalculations.getMaxPower(this.scene.currentPlayer) +
        '\n' +
        Utils.GlobalSettings.text.battle.magic +
        ':  ' +
        this.scene.currentPlayer.magic +
        '/' +
        GameCalculations.getMaxMagic(this.scene.currentPlayer) +
        '\n' +
        Utils.GlobalSettings.text.battle.defense_bonus +
        ':  ' +
        this.scene.currentPlayer.defenseBonus +
        '\n' +
        Utils.GlobalSettings.text.battle.attack_bonus +
        ':  ' +
        this.scene.currentPlayer.powerBonus +
        '\n' +
        Utils.GlobalSettings.text.battle.speed +
        ':  ' +
        GameCalculations.getSpeed(this.scene.currentPlayer) +
        '\n' +
        Utils.GlobalSettings.text.battle.gold +
        ':  ' +
        this.scene.currentPlayer.gold +
        '\n' +
        Utils.GlobalSettings.text.battle.weapon_proficiency +
        ':  ' +
        this.scene.currentPlayer.weaponProficiency +
        '\n' +
        Utils.GlobalSettings.text.battle.weapon +
        ':  ' +
        Utils.GlobalSettings.weaponsList[this.scene.currentPlayer.weapon].name
          .split(' ')
          .join('  ') +
        '\n' +
        Utils.GlobalSettings.text.battle.status +
        ':  ' +
        GameCalculations.getStatus(this.scene.currentPlayer) +
        '\n'
    );

    this.scene.bgOverlay.alpha = 0;
    this.scene.bgOverlay.visible = true;

    this.scene.tweens.add({
      targets: [this.scene.bgOverlay],
      alpha: { from: 0, to: 0.8 },
      ease: 'Linear.easeOut',
      duration: 500
    });

    this.scene.tweens.add({
      targets: [
        this.statsPanelBg,
        this.statsPanelIcon,
        this.statsPanelTitle,
        this.statsPanelStats,
        this.statsPanelCancelButton,
        this.statsPanelCancelButton.buttonTxt
      ],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500
    });
  }

  destroy() {
    if (this.statsPanelBg) {
      this.statsPanelBg.destroy();
      this.statsPanelBg = null;
    }

    if (this.statsPanelIcon) {
      this.statsPanelIcon.destroy();
      this.statsPanelIcon = null;
    }

    if (this.statsPanelTitle) {
      this.statsPanelTitle.destroy();
      this.statsPanelTitle = null;
    }

    if (this.statsPanelStats) {
      this.statsPanelStats.destroy();
      this.statsPanelStats = null;
    }

    if (this.statsPanelCancelButton && this.statsPanelCancelButton.buttonTxt) {
      this.statsPanelCancelButton.buttonTxt.destroy();
      this.statsPanelCancelButton.buttonTxt = null;
      this.statsPanelCancelButton.destroy();
      this.statsPanelCancelButton = null;
    }

    console.log('kill stats panel');
  }
}
