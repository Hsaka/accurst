import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import GameCalculations from 'utils/gameCalculations';

export default class RewardPanel {
  constructor(scene) {
    this.scene = scene;
  }

  setupRewardPanel() {
    this.rewardPanelBg = UIHelper.createDialog(
      this.scene,
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2 - 30,
      500,
      500 + (Utils.GlobalSettings.gameState.numPlayers - 1) * 100,
      'dialog9patch2',
      15
    );
    this.rewardPanelBg.alpha = 0;
    this.rewardPanelBg.visible = false;

    this.rewardPanelIcon = this.scene.add.image(
      this.rewardPanelBg.x - this.rewardPanelBg.width / 2 + 20,
      this.rewardPanelBg.y - this.rewardPanelBg.height / 2 + 20,
      'enemyAtlas1',
      'wolf'
    );
    this.rewardPanelIcon.setDisplaySize(100, 100);
    this.rewardPanelIcon.alpha = 0;
    this.rewardPanelIcon.visible = false;

    this.rewardPanelTitle = this.scene.add.bitmapText(
      this.rewardPanelBg.x,
      this.rewardPanelBg.y - this.rewardPanelBg.height / 2 + 40,
      'sakkalbold',
      '',
      60
    );
    this.rewardPanelTitle.setOrigin(0.5);
    this.rewardPanelTitle.alpha = 0;
    this.rewardPanelTitle.visible = false;

    this.rewardPanelGains = this.scene.add.bitmapText(
      this.rewardPanelTitle.x,
      this.rewardPanelTitle.y + 30,
      'sakkalbold',
      '',
      40
    );
    this.rewardPanelGains.setOrigin(0.5, 0);
    this.rewardPanelGains.alpha = 0;
    this.rewardPanelGains.setCenterAlign();
    this.rewardPanelGains.visible = false;

    this.rewardPanelInfo = this.scene.add.bitmapText(
      this.rewardPanelGains.x,
      this.rewardPanelGains.y + 80,
      'sakkalicon',
      '',
      35
    );
    this.rewardPanelInfo.alpha = 0;
    this.rewardPanelInfo.setOrigin(0.5, 0);
    this.rewardPanelInfo.setMaxWidth(450);
    this.rewardPanelInfo.setCenterAlign();
    this.rewardPanelInfo.visible = false;

    this.rewardCount = 0;
    this.rewardItemID = -1;

    this.rewardPanelPlayerBG = [];
    this.rewardPanelPlayerIcons = [];
    this.rewardPanelPlayerNames = [];
    this.rewardPanelPlayerButtons = [];
    this.rewardPanelPlayerRewardTxt = [];
    this.rewardPanelPlayerLevelUpTxt = [];

    for (var i = 0; i < Utils.GlobalSettings.gameState.numPlayers; i++) {
      var bg = this.scene.rexUI.add
        .sizer({
          orientation: 'y',
          x: this.rewardPanelBg.x,
          y: this.rewardPanelInfo.y + 200 + i * 115,
          width: this.rewardPanelBg.width - 20,
          height: 80,
        })
        .addBackground(
          this.scene.rexUI.add
            .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
            .setStrokeStyle(1, UIHelper.COLOR_LIGHT)
        )
        .layout();
      bg.visible = false;
      this.rewardPanelPlayerBG.push(bg);

      var spr = this.scene.add.image(
        bg.x - this.rewardPanelBg.width / 2 + 80,
        bg.y,
        'atlas1',
        Utils.GlobalSettings.heroList[
          Utils.GlobalSettings.gameState.players[i].classType
        ].image
      );
      spr.visible = false;
      spr.setDisplaySize(100, 100);
      this.rewardPanelPlayerIcons.push(spr);

      var name = this.scene.add.bitmapText(
        spr.x,
        spr.y + 50,
        'sakkalbold',
        Utils.GlobalSettings.gameState.players[i].name,
        30
      );
      name.visible = false;
      name.setOrigin(0.5);
      this.rewardPanelPlayerNames.push(name);

      var levelup = this.scene.add.bitmapText(
        spr.x,
        spr.y + 20,
        'sakkalbold',
        'Level Up!',
        30
      );
      levelup.visible = false;
      levelup.setOrigin(0.5);
      this.rewardPanelPlayerLevelUpTxt.push(levelup);

      var btns = [];
      for (var j = 0; j < 4; j++) {
        var btn = this.scene.add.image(
          spr.x + 100 + j * 90,
          spr.y,
          'atlas1',
          'number' + (j + 1)
        );

        btn.visible = false;
        btn._buttonIndex = j;
        btn._playerIndex = i;
        btn.setInteractive();
        btn.on('pointerover', this.onHoverOver.bind(this, btn));
        btn.on('pointerout', this.onHoverOut.bind(this, btn));
        btn.on('pointerup', this.onNumberPress.bind(this, btn));

        btns.push(btn);
      }
      this.rewardPanelPlayerButtons.push(btns);

      var reward = this.scene.add.bitmapText(
        spr.x + 70,
        spr.y,
        'sakkalbold',
        'Reward',
        35
      );
      reward.visible = false;
      reward.setOrigin(0, 0.5);
      this.rewardPanelPlayerRewardTxt.push(reward);
    }

    this.rewardPanelCancelButton = UIHelper.createMenuButton(
      this.scene,
      'menubutton',
      this.rewardPanelBg.y + this.rewardPanelBg.height / 2 + 20,
      Utils.GlobalSettings.text.battle.exit,
      false
    );
    this.rewardPanelCancelButton.visible = false;
    this.rewardPanelCancelButton.on('pointerup', () => {
      this.hideRewardPanel();
      this.scene.exitRoom();
    });
  }

  onHoverOver(button, event) {
    if (button) {
      button.setTint(0x00ff00);
    }
  }

  onHoverOut(button, event) {
    if (button) {
      button.setTint(0xffffff);
    }
  }

  onNumberPress(button, event) {
    if (button) {
      this.rewardCount++;
      if (this.rewardCount >= Utils.GlobalSettings.gameState.numPlayers) {
        this.showExitButton();
      }

      this.hideRewardButtons(button._playerIndex);

      var reward = this.calculateReward(
        button._buttonIndex,
        button._playerIndex
      );
      this.rewardPanelPlayerRewardTxt[button._playerIndex].setText(reward);
      this.showRewardText(button._playerIndex);
    }
  }

  showExitButton() {
    this.rewardPanelCancelButton.visible = true;
    this.rewardPanelCancelButton.alpha = 0;
    this.rewardPanelCancelButton.buttonTxt.visible = true;
    this.rewardPanelCancelButton.buttonTxt.alpha = 0;

    this.scene.tweens.add({
      targets: [
        this.rewardPanelCancelButton,
        this.rewardPanelCancelButton.buttonTxt,
      ],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 1000,
      delay: 500,
    });
  }

  showRewardText(index) {
    this.rewardPanelPlayerRewardTxt[index].visible = true;
    this.rewardPanelPlayerRewardTxt[index].alpha = 0;
    this.rewardPanelPlayerRewardTxt[index].setScale(0);

    this.scene.tweens.add({
      targets: [this.rewardPanelPlayerRewardTxt[index]],
      alpha: { from: 0, to: 1 },
      scale: { from: 0, to: 1 },
      ease: 'Back.easeOut',
      duration: 500,
    });
  }

  toggleRewardPanelVisibility(visible) {
    this.rewardPanelBg.visible = visible;
    this.rewardPanelIcon.visible = visible;
    this.rewardPanelTitle.visible = visible;
    this.rewardPanelGains.visible = visible;
    this.rewardPanelInfo.visible = visible;

    for (var i = 0; i < this.rewardPanelPlayerBG.length; i++) {
      this.rewardPanelPlayerBG[i].visible = visible;
      this.rewardPanelPlayerIcons[i].visible = visible;
      this.rewardPanelPlayerNames[i].visible = visible;

      for (var j = 0; j < this.rewardPanelPlayerButtons[i].length; j++) {
        this.rewardPanelPlayerButtons[i][j].visible = visible;
      }
    }
  }

  toggleRewardPanelAlpha(alpha) {
    this.rewardPanelBg.alpha = alpha;
    this.rewardPanelIcon.alpha = alpha;
    this.rewardPanelTitle.alpha = alpha;
    this.rewardPanelGains.alpha = alpha;
    this.rewardPanelInfo.alpha = alpha;

    for (var i = 0; i < this.rewardPanelPlayerBG.length; i++) {
      this.rewardPanelPlayerBG[i].alpha = alpha;
      this.rewardPanelPlayerIcons[i].alpha = alpha;
      this.rewardPanelPlayerNames[i].alpha = alpha;

      for (var j = 0; j < this.rewardPanelPlayerButtons[i].length; j++) {
        this.rewardPanelPlayerButtons[i][j].alpha = alpha;
      }
    }
  }

  hideRewardButtons(playerIndex) {
    for (
      var j = 0;
      j < this.rewardPanelPlayerButtons[playerIndex].length;
      j++
    ) {
      this.rewardPanelPlayerButtons[playerIndex][j].visible = true;
      this.rewardPanelPlayerButtons[playerIndex][j].alpha = 1;
      this.rewardPanelPlayerButtons[playerIndex][j].disableInteractive();
    }

    var playerBtnArr = this.rewardPanelPlayerButtons[playerIndex];

    this.scene.tweens.add({
      targets: [...playerBtnArr],
      alpha: { from: 1, to: 0 },
      ease: 'Linear.easeOut',
      duration: 100,
      onComplete: () => {
        for (
          var j = 0;
          j < this.rewardPanelPlayerButtons[playerIndex].length;
          j++
        ) {
          this.rewardPanelPlayerButtons[playerIndex][j].visible = false;
        }
      },
    });
  }

  hideRewardPanel() {
    this.scene.bgOverlay.alpha = 0.8;
    this.scene.bgOverlay.visible = true;

    this.toggleRewardPanelAlpha(1);
    this.toggleRewardPanelVisibility(true);

    for (var i = 0; i < Utils.GlobalSettings.gameState.numPlayers; i++) {
      this.rewardPanelPlayerBG[i].visible = false;
      this.rewardPanelPlayerIcons[i].visible = false;
      this.rewardPanelPlayerNames[i].visible = false;
      this.rewardPanelPlayerLevelUpTxt[i].visible = false;
      for (var j = 0; j < this.rewardPanelPlayerButtons[i].length; j++) {
        this.rewardPanelPlayerButtons[i][j].visible = false;
      }
      this.rewardPanelPlayerRewardTxt[i].visible = false;
    }

    this.rewardPanelCancelButton.visible = false;
    this.rewardPanelCancelButton.buttonTxt.visible = false;

    this.scene.tweens.add({
      targets: [this.scene.bgOverlay],
      alpha: { from: 0.8, to: 0 },
      ease: 'Linear.easeOut',
      duration: 500,
      onComplete: () => {
        this.scene.bgOverlay.visible = false;
      },
    });

    var playerBtnArr = this.rewardPanelPlayerButtons.flat();

    this.scene.tweens.add({
      targets: [
        this.rewardPanelBg,
        this.rewardPanelIcon,
        this.rewardPanelTitle,
        this.rewardPanelGains,
        this.rewardPanelInfo,
        ...this.rewardPanelPlayerBG,
        ...this.rewardPanelPlayerIcons,
        ...this.rewardPanelPlayerNames,
        ...playerBtnArr,
      ],
      alpha: { from: 1, to: 0 },
      ease: 'Linear.easeOut',
      duration: 500,
      onComplete: () => {
        this.toggleRewardPanelVisibility(false);
      },
    });
  }

  calculateReward(rewardIndex, playerIndex) {
    var player = Utils.GlobalSettings.gameState.players[playerIndex];
    switch (rewardIndex) {
      case 0:
        var goldGain = this.calculateGoldGain();
        player.gold += goldGain;
        return (
          Utils.GlobalSettings.text.battle.reward_gold +
          '\n+' +
          goldGain +
          ' Extra Gold Obtained!'
        );
        break;

      case 1:
        var expGain = this.calculateExperienceGain();
        var alreadyLeveled = player.levelUp;
        this.increaseExperience(player, expGain);

        if (!alreadyLeveled && player.levelUp) {
          this.rewardPanelPlayerLevelUpTxt[playerIndex].visible = true;
          this.rewardPanelPlayerLevelUpTxt[playerIndex].alpha = 0;
          this.scene.tweens.add({
            targets: [this.rewardPanelPlayerLevelUpTxt[playerIndex]],
            y: {
              from: this.rewardPanelPlayerLevelUpTxt[playerIndex].y + 10,
              to: this.rewardPanelPlayerLevelUpTxt[playerIndex].y,
            },
            alpha: { from: 0, to: 1 },
            ease: 'Back.easeOut',
            duration: 500,
          });
        }

        return (
          Utils.GlobalSettings.text.battle.reward_exp +
          '\n+' +
          expGain +
          ' Extra Experience Obtained!'
        );
        break;

      case 2:
        var hpGain = Math.ceil(player.maxHealth / 2);
        var mpGain = Math.ceil(player.maxMagic / 2);
        player.health += hpGain;

        var maxHP = GameCalculations.getMaxHealth(player);
        var maxMP = GameCalculations.getMaxMagic(player);

        if (player.health > maxHP) {
          player.health = maxHP;
        }
        player.magic += mpGain;
        if (player.magic > maxMP) {
          player.magic = maxMP;
        }
        return (
          Utils.GlobalSettings.text.battle.reward_recovery +
          '\n' +
          hpGain +
          ' HP & ' +
          mpGain +
          ' MP Restored!'
        );
        break;

      case 3:
        this.rewardItemID = GameCalculations.getItemReward(1);
        var rewardItem = Utils.GlobalSettings.itemsList[this.rewardItemID];

        if (player.items.length < 4) {
          player.items.push(this.rewardItemID);
        } else {
          this.scene.battleUI.dropItemPanel.setPlayer(player);
          this.scene.battleUI.dropItemPanel.showDropItemPanel(
            0,
            this.rewardItemID
          );
        }

        return (
          Utils.GlobalSettings.text.battle.reward_item + '\n' + rewardItem.name
        );
        break;
    }
  }

  calculateExperienceGain() {
    return GameCalculations.calculateExperienceGain(
      this.scene.enemyManager.currentEnemy.level
    );
  }

  calculateGoldGain() {
    return GameCalculations.calculateGoldGain(
      this.scene.enemyManager.currentEnemy.level
    );
  }

  increaseExperience(player, amount) {
    GameCalculations.increaseExperience(
      player,
      amount,
      Utils.GlobalSettings.heroList[player.classType].exp_rate
    );
  }

  showRewardPanel(delay = 0) {
    this.toggleRewardPanelAlpha(0);
    this.toggleRewardPanelVisibility(true);

    this.rewardPanelTitle.setText(
      this.scene.enemyManager.currentEnemy.name +
        '  ' +
        Utils.GlobalSettings.text.battle.defeated
    );

    var expGain = this.calculateExperienceGain();
    var goldGain = this.calculateGoldGain();

    for (var i = 0; i < Utils.GlobalSettings.gameState.numPlayers; i++) {
      Utils.GlobalSettings.gameState.players[i].gold += goldGain;
      this.increaseExperience(
        Utils.GlobalSettings.gameState.players[i],
        expGain
      );

      if (Utils.GlobalSettings.gameState.players[i].levelUp) {
        this.rewardPanelPlayerLevelUpTxt[i].visible = true;
        this.rewardPanelPlayerLevelUpTxt[i].alpha = 0;
        this.scene.tweens.add({
          targets: [this.rewardPanelPlayerLevelUpTxt[i]],
          y: {
            from: this.rewardPanelPlayerLevelUpTxt[i].y + 10,
            to: this.rewardPanelPlayerLevelUpTxt[i].y,
          },
          alpha: { from: 0, to: 1 },
          ease: 'Back.easeOut',
          duration: 500,
          delay: delay + 500,
        });
      }
    }

    this.rewardPanelGains.setText(
      Utils.GlobalSettings.text.battle.experience +
        ':  +' +
        expGain +
        '\n' +
        Utils.GlobalSettings.text.battle.gold +
        ':  +' +
        goldGain
    );

    this.rewardPanelInfo.setText(
      Utils.GlobalSettings.text.battle.instructions_reward
    );

    this.rewardPanelIcon.setFrame(this.scene.enemyManager.enemyIcon.frame.name);

    this.scene.bgOverlay.alpha = 0;
    this.scene.bgOverlay.visible = true;

    this.scene.tweens.add({
      targets: [this.scene.bgOverlay],
      alpha: { from: 0, to: 0.8 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: delay,
    });

    var playerBtnArr = this.rewardPanelPlayerButtons.flat();

    this.scene.tweens.add({
      targets: [
        this.rewardPanelBg,
        this.rewardPanelIcon,
        this.rewardPanelTitle,
        this.rewardPanelGains,
        this.rewardPanelInfo,
        ...this.rewardPanelPlayerBG,
        ...this.rewardPanelPlayerIcons,
        ...this.rewardPanelPlayerNames,
        ...playerBtnArr,
      ],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: delay,
    });
  }

  destroy() {
    if (this.rewardPanelBg) {
      this.rewardPanelBg.destroy();
      this.rewardPanelBg = null;
    }

    if (this.rewardPanelIcon) {
      this.rewardPanelIcon.destroy();
      this.rewardPanelIcon = null;
    }

    if (this.rewardPanelTitle) {
      this.rewardPanelTitle.destroy();
      this.rewardPanelTitle = null;
    }

    if (this.rewardPanelGains) {
      this.rewardPanelGains.destroy();
      this.rewardPanelGains = null;
    }

    if (this.rewardPanelInfo) {
      this.rewardPanelInfo.destroy();
      this.rewardPanelInfo = null;
    }

    if (this.rewardPanelPlayerBG) {
      for (var i = 0; i < this.rewardPanelPlayerBG.length; i++) {
        this.rewardPanelPlayerBG[i].destroy();
        this.rewardPanelPlayerIcons[i].destroy();
        this.rewardPanelPlayerNames[i].destroy();
        this.rewardPanelPlayerRewardTxt[i].destroy();
        this.rewardPanelPlayerLevelUpTxt[i].destroy();

        this.rewardPanelPlayerBG[i] = null;
        this.rewardPanelPlayerIcons[i] = null;
        this.rewardPanelPlayerNames[i] = null;
        this.rewardPanelPlayerRewardTxt[i] = null;
        this.rewardPanelPlayerLevelUpTxt[i] = null;

        for (var j = 0; j < this.rewardPanelPlayerButtons[i].length; j++) {
          this.rewardPanelPlayerButtons[i][j].destroy();
          this.rewardPanelPlayerButtons[i][j] = null;
        }
      }
    }

    this.rewardPanelPlayerBG = null;
    this.rewardPanelPlayerIcons = null;
    this.rewardPanelPlayerNames = null;
    this.rewardPanelPlayerButtons = null;
    this.rewardPanelPlayerRewardTxt = null;
    this.rewardPanelPlayerLevelUpTxt = null;

    if (
      this.rewardPanelCancelButton &&
      this.rewardPanelCancelButton.buttonTxt
    ) {
      this.rewardPanelCancelButton.buttonTxt.destroy();
      this.rewardPanelCancelButton.buttonTxt = null;
      this.rewardPanelCancelButton.destroy();
      this.rewardPanelCancelButton = null;
    }

    console.log('kill reward panel');
  }
}
