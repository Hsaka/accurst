import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import ActionPanel from 'utils/battle/actionPanel';
import PlayerPanel from 'utils/battle/playerPanel';
import SkillPanel from 'utils/battle/skillPanel';
import TimelinePanel from 'utils/battle/timelinePanel';
import StatsPanel from 'utils/battle/statsPanel';
import RewardPanel from 'utils/battle/rewardPanel';
import DeathPanel from 'utils/battle/deathPanel';
import DropItemPanel from 'utils/shared/dropItemPanel';

export default class BattleUI {
  constructor(scene) {
    this.scene = scene;
  }

  setupPanels() {
    this.actionPanel = new ActionPanel(this.scene);

    this.statsPanel = new StatsPanel(this.scene);
    this.rewardPanel = new RewardPanel(this.scene);
    this.dropItemPanel = new DropItemPanel(this.scene);
    this.deathPanel = new DeathPanel(this.scene);

    this.playerPanel = new PlayerPanel(this.scene);
    this.playerPanel.setupPlayer();

    this.skillPanel = new SkillPanel(this.scene);
    this.skillPanel.setupSkillPanel();

    this.timelinePanel = new TimelinePanel(this.scene);

    this.setupMenuIcons();

    this.setupInstructions();

    this.turnTxt = this.scene.add.bitmapText(
      -300,
      Utils.GlobalSettings.height / 2,
      'sakkalbold',
      Utils.GlobalSettings.text.battle.turn +
        '  ' +
        Utils.GlobalSettings.gameState.encounter.combatRound,
      80
    );
    this.turnTxt.setOrigin(0.5);
    this.scene.tweens.add({
      targets: [this.turnTxt],
      x: { from: -300, to: Utils.GlobalSettings.width / 2 },
      ease: 'Quad.easeOut',
      duration: 500,
    });
    this.scene.tweens.add({
      targets: [this.turnTxt],
      x: {
        from: Utils.GlobalSettings.width / 2,
        to: Utils.GlobalSettings.width + 300,
      },
      ease: 'Quad.easeIn',
      duration: 500,
      delay: 1000,
      onComplete: () => {
        this.turnTxt.visible = false;
      },
    });
  }

  setupInstructions() {
    this.instructionBackground = this.scene.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.instructionDialog = this.scene.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width - 110,
        y: Utils.GlobalSettings.height / 2 - 70,
        width: 200,
        height: 300,
      })
      .addBackground(this.instructionBackground)
      .layout();
    this.instructionDialog.alpha = 0;
    this.instructionDialog.visible = false;

    this.instructionTxt = this.scene.add.bitmapText(
      this.instructionDialog.x - this.instructionDialog.width / 2 + 10,
      this.instructionDialog.y - this.instructionDialog.height / 2 + 10,
      'sakkalbold',
      '',
      28
    );
    this.instructionTxt.alpha = 0;
    this.instructionTxt.setOrigin(0);
    this.instructionTxt.setMaxWidth(this.instructionDialog.width - 30);
    this.instructionTxt.visible = false;
  }

  showInstructions(text, delay = 0) {
    this.instructionDialog.visible = true;
    this.instructionTxt.visible = true;
    this.instructionDialog.alpha = 0;
    this.instructionTxt.alpha = 0;

    this.instructionTxt.setText(text);

    this.scene.tweens.add({
      targets: [this.instructionDialog, this.instructionTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeOut',
      duration: 1000,
      delay: delay,
    });
  }

  hideInstructions() {
    if (this.instructionDialog.visible) {
      this.instructionDialog.alpha = 1;
      this.instructionTxt.alpha = 1;

      this.scene.tweens.add({
        targets: [this.instructionDialog, this.instructionTxt],
        alpha: { from: 1, to: 0 },
        ease: 'Sine.easeOut',
        duration: 500,
        onComplete: () => {
          this.instructionDialog.visible = false;
          this.instructionTxt.visible = false;
        },
      });
    }
  }

  setupMenuIcons() {
    this.currentMenuSelection = Utils.GlobalSettings.text.battle.skills;

    this.attackIcon = this.createMenuIcon(
      50,
      550,
      'rune_attack',
      Utils.GlobalSettings.text.battle.skills
    );
    this.defendIcon = this.createMenuIcon(
      this.attackIcon.x + 100,
      this.attackIcon.y,
      'rune_defend',
      Utils.GlobalSettings.text.battle.defense
    );
    this.itemIcon = this.createMenuIcon(
      this.attackIcon.x,
      this.attackIcon.y + 100,
      'rune_item',
      Utils.GlobalSettings.text.battle.items
    );
    this.endturnIcon = this.createMenuIcon(
      this.defendIcon.x,
      this.defendIcon.y + 100,
      'rune_endturn',
      Utils.GlobalSettings.text.battle.end_turn
    );

    this.showMenuIcons(1500);

    this.attackIcon.on('pointerup', () => {
      this.currentMenuSelection = Utils.GlobalSettings.text.battle.skills;
      this.skillPanel.showAttackPanel();
    });

    this.defendIcon.on('pointerup', () => {
      this.currentMenuSelection = Utils.GlobalSettings.text.battle.defense;
      this.skillPanel.showDefendPanel();
    });

    this.itemIcon.on('pointerup', () => {
      this.currentMenuSelection = Utils.GlobalSettings.text.battle.items;
      this.skillPanel.showItemPanel();
    });

    this.endturnIcon.on('pointerup', () => {
      this.endturnIcon.disableInteractive();
      this.hideMenuIcons();
      this.hideInstructions();
      this.skillPanel.hideSkillPanel();
      this.timelinePanel.startTimeline();
    });

    this.scene.time.delayedCall(
      2000,
      () => {
        this.skillPanel.showAttackPanel();
      },
      [],
      this
    );
  }

  showMenuIcons(delay = 0) {
    var icons = [
      this.attackIcon,
      this.defendIcon,
      this.itemIcon,
      this.endturnIcon,
    ];
    for (var i = 0; i < icons.length; i++) {
      icons[i].visible = true;
      icons[i].setScale(0);

      this.scene.tweens.add({
        targets: icons[i],
        scale: { from: 0, to: 0.5 },
        ease: 'Back.easeOut',
        duration: 500,
        delay: delay + i * 100,
      });
    }

    var txts = [
      this.attackIcon.iconTxt,
      this.defendIcon.iconTxt,
      this.itemIcon.iconTxt,
      this.endturnIcon.iconTxt,
    ];
    for (var i = 0; i < txts.length; i++) {
      txts[i].visible = true;
      txts[i].alpha = 0;

      this.scene.tweens.add({
        targets: txts[i],
        alpha: { from: 0, to: 1 },
        ease: 'Linear.easeOut',
        duration: 500,
        delay: delay + i * 100,
      });
    }
  }

  hideMenuIcons(delay = 0) {
    var icons = [
      this.attackIcon,
      this.defendIcon,
      this.itemIcon,
      this.endturnIcon,
    ];
    for (var i = 0; i < icons.length; i++) {
      icons[i].visible = true;
      icons[i].setScale(0.5);

      this.scene.tweens.add({
        targets: icons[i],
        scale: { from: 0.5, to: 0 },
        ease: 'Back.easeIn',
        duration: 500,
        delay: delay + i * 100,
        onComplete: (tween) => {
          tween.targets[0].visible = false;
        },
      });
    }

    var txts = [
      this.attackIcon.iconTxt,
      this.defendIcon.iconTxt,
      this.itemIcon.iconTxt,
      this.endturnIcon.iconTxt,
    ];
    for (var i = 0; i < txts.length; i++) {
      txts[i].visible = true;
      txts[i].alpha = 1;

      this.scene.tweens.add({
        targets: txts[i],
        alpha: { from: 1, to: 0 },
        ease: 'Linear.easeOut',
        duration: 500,
        delay: delay + i * 100,
        onComplete: (tween) => {
          tween.targets[0].visible = false;
        },
      });
    }
  }

  createMenuIcon(x, y, frame, text) {
    var icon = this.scene.add.image(x, y, 'atlas1', frame);
    icon.setInteractive();
    icon.setScale(0);

    var txt = this.scene.add.bitmapText(
      icon.x,
      icon.y + 35,
      'sakkalbold',
      text,
      35
    );
    txt.alpha = 0;
    txt.setOrigin(0.5);

    icon.on('pointerover', (event) => {
      icon.setTint(0x00ff00);
      txt.setTint(0x009dfd);
    });

    icon.on('pointerout', (event) => {
      icon.setTint(0xffffff);
      txt.setTint(0xffffff);
    });

    icon.iconTxt = txt;
    return icon;
  }

  destroy() {
    if (this.actionPanel) {
      this.actionPanel.destroy();
      this.actionPanel = null;
    }

    if (this.playerPanel) {
      this.playerPanel.destroy();
      this.playerPanel = null;
    }

    if (this.skillPanel) {
      this.skillPanel.destroy();
      this.skillPanel = null;
    }

    if (this.timelinePanel) {
      this.timelinePanel.destroy();
      this.timelinePanel = null;
    }

    if (this.statsPanel) {
      this.statsPanel.destroy();
      this.statsPanel = null;
    }

    if (this.rewardPanel) {
      this.rewardPanel.destroy();
      this.rewardPanel = null;
    }

    if (this.dropItemPanel) {
      this.dropItemPanel.destroy();
      this.dropItemPanel = null;
    }

    if (this.instructionBackground) {
      this.instructionBackground.destroy();
      this.instructionBackground = null;
    }

    if (this.instructionDialog) {
      this.instructionDialog.destroy();
      this.instructionDialog = null;
    }

    if (this.instructionTxt) {
      this.instructionTxt.destroy();
      this.instructionTxt = null;
    }

    if (this.attackIcon.iconTxt) {
      this.attackIcon.iconTxt.destroy();
      this.attackIcon.iconTxt = null;
    }

    if (this.defendIcon.iconTxt) {
      this.defendIcon.iconTxt.destroy();
      this.defendIcon.iconTxt = null;
    }

    if (this.itemIcon.iconTxt) {
      this.itemIcon.iconTxt.destroy();
      this.itemIcon.iconTxt = null;
    }

    if (this.endturnIcon.iconTxt) {
      this.endturnIcon.iconTxt.destroy();
      this.endturnIcon.iconTxt = null;
    }

    if (this.attackIcon) {
      this.attackIcon.destroy();
      this.attackIcon = null;
    }

    if (this.defendIcon) {
      this.defendIcon.destroy();
      this.defendIcon = null;
    }

    if (this.itemIcon) {
      this.itemIcon.destroy();
      this.itemIcon = null;
    }

    if (this.endturnIcon) {
      this.endturnIcon.destroy();
      this.endturnIcon = null;
    }

    if (this.turnTxt) {
      this.turnTxt.destroy();
      this.turnTxt = null;
    }

    console.log('kill battle ui');
  }
}
