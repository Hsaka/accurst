import Utils from 'utils/utils';

export default class EffectManager {
  constructor(scene) {
    this.scene = scene;

    this.actionTargetIcon = this.scene.add.image(
      100,
      300,
      'atlas1',
      'hero_knight'
    );
    this.actionTargetIcon.setScale(0.35);
    this.actionTargetIcon.visible = false;

    this.particles = this.scene.add.particles('effectAtlas1');

    this.effectImage = this.scene.add.sprite(
      0,
      0,
      'effectAtlas1',
      'item_health_m'
    );
    this.effectImage.visible = false;
    this.effectImage.alpha = 0;

    this.effects = this.scene.cache.json.get('particles');

    this.emitters = {};

    this.effectTxt = this.scene.add.bitmapText(0, 0, 'sakkalbold', 'Test', 35);
    this.effectTxt.setOrigin(0.5);
    this.effectTxt.setCenterAlign();
    this.effectTxt.visible = false;
    this.effectTxt.alpha = 0;
  }

  showTargetIconImage(frame, duration = 1000, delay = 0) {
    this.actionTargetIcon.visible = true;
    this.actionTargetIcon.setScale(0);
    this.actionTargetIcon.alpha = 0;
    this.actionTargetIcon.setFrame(frame);

    this.scene.tweens.add({
      targets: [this.actionTargetIcon],
      alpha: { from: 0, to: 1 },
      scale: { from: 0, to: 0.35 },
      ease: 'Back.easeOut',
      duration: duration,
      delay: delay,
      yoyo: true,
    });
  }

  showEffectImage(frame, config, duration = 1000, delay = 0, callback) {
    this.effectImage.visible = true;
    this.effectImage.alpha = 0;
    this.effectImage.x = config.x;
    this.effectImage.y = config.y;
    this.effectImage.setAngle(0);
    this.effectImage.setFrame(frame);
    this.effectImage.setScale(0);

    this.scene.tweens.add({
      targets: [this.effectImage],
      scale: { from: 0, to: 2 },
      alpha: { from: 0, to: 1 },
      angle: { from: 0, to: 720 },
      ease: 'Sine.easeOut',
      duration: duration,
      delay: delay,
      yoyo: true,
      onComplete: () => {
        this.effectImage.visible = false;
        this.effectTxt.visible = false;
        this.effectTxt.alpha = 0;
        if (callback && typeof callback === 'function') {
          callback();
        }
      },
    });
  }

  showEffectText(x, y, text, color = 0xffffff, duration = 800) {
    this.effectTxt.visible = true;
    this.effectTxt.alpha = 0;
    this.effectTxt.x = x;
    this.effectTxt.y = y;
    this.effectTxt.setTint(color);
    this.effectTxt.setText(text);

    this.scene.tweens.add({
      targets: [this.effectTxt],
      y: {
        from: y + 20,
        to: y,
      },
      ease: 'Sine.easeOut',
      duration: duration,
    });

    this.scene.tweens.add({
      targets: [this.effectTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: duration - 200,
      yoyo: true,
    });
  }

  triggerEmitter(index, config, duration, callback) {
    var emitter = this.emitters[index];
    if (emitter) {
      if (config) {
        this.modifyEffect(config, emitter);
      }

      emitter.start();

      this.scene.time.delayedCall(
        duration,
        () => {
          this.stopAllEmitters();
          this.effectTxt.visible = false;
          this.effectTxt.alpha = 0;
          if (callback && typeof callback === 'function') {
            callback();
          }
        },
        [],
        this
      );
    }
    return emitter;
  }

  doEffect(index, config, duration = 1000, callback) {
    switch (index) {
      case 0: //summon
        var emitter = this.triggerEmitter(index, config, duration, callback);
        if (emitter) {
          this.scene.cameras.main.shake(1000, 0.01);
        }
        break;

      case 5: //death
        var emitter = this.triggerEmitter(index, config, duration, callback);
        if (emitter) {
          this.scene.cameras.main.shake(1000, 0.01);
          this.scene.cameras.main.flash(250, 255, 0, 0);
        }
        break;

      default:
        var emitter = this.triggerEmitter(index, config, duration, callback);
        break;
    }
  }

  modifyEffect(config, emitter) {
    if (config.x && config.y) {
      emitter.setPosition(config.x, config.y);
    }

    if (config.moveToX) {
      emitter.moveToX.propertyValue = config.moveToX;
    }

    if (config.moveToY) {
      emitter.moveToY.propertyValue = config.moveToY;
    }
  }

  loadEffects(effectsToLoad) {
    for (var i = 0; i < effectsToLoad.length; i++) {
      var eff = effectsToLoad[i];

      if (eff < this.effects.length && !this.emitters[eff]) {
        var effect = this.effects[eff];
        effect.on = false;
        this.emitters[eff] = this.particles.createEmitter(effect);
      }
    }
  }

  stopAllEmitters() {
    var emitters = this.particles.emitters.list;
    if (emitters) {
      for (var i = 0; i < emitters.length; i++) {
        if (emitters[i]) {
          emitters[i].stop();
        }
      }
    }
  }

  destroy() {
    if (this.actionTargetIcon) {
      this.actionTargetIcon.destroy();
      this.actionTargetIcon = null;
    }

    if (this.particles) {
      this.particles.destroy();
      this.particles = null;
    }

    if (this.effectImage) {
      this.effectImage.destroy();
      this.effectImage = null;
    }

    this.effects = null;

    this.emitters = null;

    console.log('kill effect manager');
  }
}
