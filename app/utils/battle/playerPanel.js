import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import GameCalculations from 'utils/gameCalculations';

export default class PlayerPanel {
  constructor(scene) {
    this.scene = scene;
    this.playerDead = false;
  }

  setupPlayer() {
    var endsWithS = UIHelper.endsWithS(this.scene.currentPlayer.name);

    this.infoDialog = UIHelper.createTextDialog(
      this.scene,
      Utils.GlobalSettings.width / 2,
      50,
      500,
      75,
      endsWithS
        ? this.scene.currentPlayer.name +
            "' " +
            Utils.GlobalSettings.text.battle.turn
        : this.scene.currentPlayer.name +
            "'s " +
            Utils.GlobalSettings.text.battle.turn
    );
    this.infoDialog.textContent.alpha = 0;
    this.infoDialog.setScale(0);

    this.scene.tweens.add({
      targets: [this.infoDialog],
      scale: { from: 0, to: 1 },
      ease: 'Back.easeOut',
      duration: 500,
      delay: 1000,
    });

    this.scene.tweens.add({
      targets: [this.infoDialog.textContent],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 1000,
      delay: 1000,
    });

    this.playerIcon = this.scene.add.image(
      100,
      Utils.GlobalSettings.height - 120,
      'atlas1',
      Utils.GlobalSettings.heroList[this.scene.currentPlayer.classType].image
    );
    this.playerIcon.setInteractive();
    this.playerIcon.setScale(0);
    this.playerIcon.on('pointerover', (event) => {
      this.playerIcon.setTint(0x00ff00);
    });

    this.playerIcon.on('pointerout', (event) => {
      this.playerIcon.setTint(0xffffff);
    });
    this.playerIcon.on('pointerup', () => {
      this.scene.battleUI.statsPanel.showStatsPanel();
    });

    this.hpIcon = this.createStatIcon(
      this.playerIcon.x + 70,
      this.playerIcon.y + 60,
      'rune_health',
      '' + this.scene.currentPlayer.health
    );
    this.hpTxt = this.hpIcon.iconTxt;

    this.mpIcon = this.createStatIcon(
      this.playerIcon.x - 70,
      this.playerIcon.y + 60,
      'rune_magic',
      '' + this.scene.currentPlayer.magic
    );
    this.mpTxt = this.mpIcon.iconTxt;

    this.powIcon = this.createStatIcon(
      this.playerIcon.x,
      this.playerIcon.y + 80,
      'rune_power',
      '' + this.scene.currentPlayer.power
    );
    this.powTxt = this.powIcon.iconTxt;

    this.lvlIcon = this.createStatIcon(
      this.playerIcon.x - 70,
      this.playerIcon.y - 60,
      'rune_level',
      '' + this.scene.currentPlayer.level
    );
    this.lvlTxt = this.lvlIcon.iconTxt;

    this.wepIcon = this.createStatIcon2(
      this.playerIcon.x,
      this.playerIcon.y - 80,
      'rune_weapon',
      Utils.GlobalSettings.weaponsList[this.scene.currentPlayer.weapon].image
    );
    this.wepIconImg = this.wepIcon.iconImg;

    this.expIcon = this.createStatIcon(
      this.playerIcon.x + 70,
      this.playerIcon.y - 60,
      'rune_exp',
      '' + this.scene.currentPlayer.experience
    );
    this.expTxt = this.expIcon.iconTxt;

    var currentStatus = GameCalculations.getStatus(
      this.scene.currentPlayer
    ).toLowerCase();

    this.statusIcon = this.createStatIcon(
      this.playerIcon.x - 75,
      this.playerIcon.y,
      'status_' + currentStatus,
      ''
    );
    this.statusTxt = this.statusIcon.iconTxt;

    this.showPlayerPanel(1500);
  }

  showPlayerPanel(delay = 0) {
    this.playerIcon.visible = true;
    this.playerIcon.setScale(0);
    this.scene.tweens.add({
      targets: [this.playerIcon],
      scale: { from: 0, to: 0.5 },
      ease: 'Back.easeOut',
      duration: 500,
      delay: delay,
    });

    var currentStatus = GameCalculations.getStatus(this.scene.currentPlayer);

    var icons = [
      this.lvlIcon,
      this.wepIcon,
      this.expIcon,
      this.hpIcon,
      this.powIcon,
      this.mpIcon,
      this.statusIcon,
    ];
    for (var i = 0; i < icons.length; i++) {
      icons[i].visible = true;
      icons[i].setScale(0);
      if (
        icons[i] === this.statusIcon &&
        currentStatus === Utils.GlobalSettings.text.battle.status_normal
      ) {
        icons[i].visible = false;
      }

      this.scene.tweens.add({
        targets: icons[i],
        scale: { from: 0, to: 0.45 },
        ease: 'Back.easeOut',
        duration: 500,
        delay: delay + i * 100,
      });
    }

    var txts = [
      this.lvlTxt,
      this.wepIconImg,
      this.expTxt,
      this.hpTxt,
      this.powTxt,
      this.mpTxt,
    ];

    for (var i = 0; i < txts.length; i++) {
      txts[i].visible = true;
      txts[i].alpha = 0;

      this.scene.tweens.add({
        targets: txts[i],
        alpha: { from: 0, to: 1 },
        ease: 'Linear.easeOut',
        duration: 500,
        delay: delay + i * 100,
      });
    }
  }

  hidePlayerPanel(delay = 0) {
    this.playerIcon.visible = true;
    this.playerIcon.setScale(0.5);
    this.scene.tweens.add({
      targets: [this.playerIcon],
      scale: { from: 0.5, to: 0 },
      ease: 'Back.easeIn',
      duration: 900,
      delay: delay,
      onComplete: () => {
        this.playerIcon.visible = false;
      },
    });

    var currentStatus = GameCalculations.getStatus(this.scene.currentPlayer);

    var icons = [
      this.lvlIcon,
      this.wepIcon,
      this.expIcon,
      this.hpIcon,
      this.powIcon,
      this.mpIcon,
      this.statusIcon,
    ];
    for (var i = 0; i < icons.length; i++) {
      icons[i].visible = true;
      icons[i].setScale(0.45);
      if (
        icons[i] === this.statusIcon &&
        currentStatus === Utils.GlobalSettings.text.battle.status_normal
      ) {
        icons[i].visible = false;
      }

      this.scene.tweens.add({
        targets: icons[i],
        scale: { from: 0.45, to: 0 },
        ease: 'Back.easeIn',
        duration: 500,
        delay: delay + i * 100,
        onComplete: (tween) => {
          tween.targets[0].visible = false;
        },
      });
    }

    var txts = [
      this.lvlTxt,
      this.wepIconImg,
      this.expTxt,
      this.hpTxt,
      this.powTxt,
      this.mpTxt,
      this.infoDialog.textContent,
      this.infoDialog,
    ];
    for (var i = 0; i < txts.length; i++) {
      txts[i].visible = true;
      txts[i].alpha = 1;

      this.scene.tweens.add({
        targets: txts[i],
        alpha: { from: 1, to: 0 },
        ease: 'Linear.easeOut',
        duration: 500,
        delay: delay + i * 100,
        onComplete: (tween) => {
          tween.targets[0].visible = false;
        },
      });
    }
  }

  createStatIcon(x, y, frame, text) {
    var icon = this.scene.add.image(x, y, 'atlas1', frame);
    icon.setInteractive();
    icon.setScale(0);

    icon.on('pointerover', (event) => {
      icon.setTint(0x00ff00);
    });

    icon.on('pointerout', (event) => {
      icon.setTint(0xffffff);
    });

    icon.on('pointerup', () => {
      var toastMsg = '';
      switch (icon.frame.name) {
        case 'rune_health':
          toastMsg =
            Utils.GlobalSettings.text.battle.health +
            ': ' +
            this.scene.currentPlayer.health +
            '/' +
            GameCalculations.getMaxHealth(this.scene.currentPlayer);
          break;

        case 'rune_magic':
          toastMsg =
            Utils.GlobalSettings.text.battle.magic +
            ': ' +
            this.scene.currentPlayer.magic +
            '/' +
            GameCalculations.getMaxMagic(this.scene.currentPlayer);
          break;

        case 'rune_power':
          toastMsg =
            Utils.GlobalSettings.text.battle.power +
            ': ' +
            this.scene.currentPlayer.power +
            '/' +
            GameCalculations.getMaxPower(this.scene.currentPlayer);
          break;

        case 'rune_exp':
          toastMsg =
            Utils.GlobalSettings.text.battle.experience +
            ': ' +
            this.scene.currentPlayer.experience +
            '/' +
            this.scene.currentPlayer.requiredExperience;
          break;

        case 'rune_level':
          toastMsg =
            Utils.GlobalSettings.text.battle.level +
            ': ' +
            this.scene.currentPlayer.level;
          break;
      }

      if (icon.frame.name.startsWith('status_')) {
        toastMsg =
          Utils.GlobalSettings.text.battle.status +
          ': ' +
          Utils.GlobalSettings.text.battle[icon.frame.name];
      }

      UIHelper.showToast(
        this.scene,
        this.scene.toast,
        icon.x,
        icon.y - 60,
        toastMsg
      );
    });

    var txt = this.scene.add.bitmapText(icon.x, icon.y, 'sakkalbold', text, 38);
    txt.alpha = 0;
    txt.setOrigin(0.5);

    icon.iconTxt = txt;
    return icon;
  }

  createStatIcon2(x, y, frame, imgFrame) {
    var icon = this.scene.add.image(x, y, 'atlas1', frame);
    icon.setInteractive();
    icon.setScale(0);

    icon.on('pointerover', (event) => {
      icon.setTint(0x00ff00);
    });

    icon.on('pointerout', (event) => {
      icon.setTint(0xffffff);
    });

    icon.on('pointerup', () => {
      var toastMsg = '';
      switch (icon.frame.name) {
        case 'rune_weapon':
          toastMsg =
            Utils.GlobalSettings.text.battle.weapon +
            ': ' +
            Utils.GlobalSettings.weaponsList[this.scene.currentPlayer.weapon]
              .name;
          break;
      }
      UIHelper.showToast(
        this.scene,
        this.scene.toast,
        icon.x,
        icon.y - 60,
        toastMsg
      );
    });

    var iconImg = this.scene.add.image(x, y, 'atlas1', imgFrame);
    iconImg.alpha = 0;
    iconImg.setScale(0.5);

    icon.iconImg = iconImg;
    return icon;
  }

  killPlayer() {
    this.scene.battleUI.timelinePanel.canAdvance = false;
    this.playerDead = true;
    this.scene.effectManager.doEffect(5, {
      x: this.playerIcon.x,
      y: this.playerIcon.y,
    });
    this.hidePlayerPanel();
  }

  doAction(action, onComplete, isStatusEffectAction = false) {
    if (!this.playerDead) {
      this.scene.actionManager.handleAction(action, true, onComplete);
    } else {
      if (onComplete && typeof onComplete === 'function') {
        this.scene.time.delayedCall(
          500,
          () => {
            onComplete();
          },
          [],
          this
        );
      }
    }
  }

  removeStatusEffect() {
    this.scene.currentPlayer.status =
      Utils.GlobalSettings.text.battle.status_normal;
    this.statusIcon.visible = true;
    this.statusIcon.setScale(0.45);
    this.statusIcon.alpha = 1;

    this.scene.tweens.add({
      targets: this.statusIcon,
      scale: { from: 0.45, to: 1 },
      alpha: { from: 1, to: 0 },
      ease: 'Back.easeIn',
      duration: 500,
      onComplete: () => {
        this.statusIcon.visible = false;
        this.statusIcon.setScale(0.45);
        this.statusIcon.alpha = 1;
      },
    });
  }

  applyStatusEffect(status, duration) {
    this.scene.currentPlayer.status = status;
    var currentStatus = GameCalculations.getStatus(this.scene.currentPlayer);
    if (currentStatus === status) {
      this.statusIcon.visible = true;
      this.statusIcon.alpha = 1;
      this.statusIcon.setScale(0);
      this.statusIcon.setFrame('status_' + currentStatus.toLowerCase());

      this.scene.tweens.add({
        targets: this.statusIcon,
        scale: { from: 0, to: 0.45 },
        ease: 'Back.easeOut',
        duration: 500,
      });
    }
  }

  reduceSpeed(amount) {
    var newSpeed = this.scene.currentPlayer.speed - amount;
    if (newSpeed < 0) {
      newSpeed = 0;
    }
    this.scene.currentPlayer.speed = newSpeed;
  }

  reducePower(amount) {
    this.powIcon.visible = true;
    this.powIcon.setScale(0);

    this.scene.tweens.add({
      targets: this.powIcon,
      scale: { from: 0, to: 0.45 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    var newPow = this.scene.currentPlayer.power - amount;
    if (newPow < 0) {
      newPow = 0;
    }

    this.scene.tweens.addCounter({
      from: this.scene.currentPlayer.power,
      to: newPow,
      duration: 500,
      onUpdate: (tween) => {
        this.powTxt.setText('' + Math.ceil(tween.getValue()));
      },
      onComplete: () => {
        this.scene.currentPlayer.power = newPow;
        this.powTxt.setText('' + this.scene.currentPlayer.power);
      },
    });
  }

  reduceMagic(amount) {
    this.mpIcon.visible = true;
    this.mpIcon.setScale(0);

    this.scene.tweens.add({
      targets: this.mpIcon,
      scale: { from: 0, to: 0.45 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    var newMP = this.scene.currentPlayer.magic - amount;
    if (newMP < 0) {
      newMP = 0;
    }

    this.scene.tweens.addCounter({
      from: this.scene.currentPlayer.magic,
      to: newMP,
      duration: 500,
      onUpdate: (tween) => {
        this.mpTxt.setText('' + Math.ceil(tween.getValue()));
      },
      onComplete: () => {
        this.scene.currentPlayer.magic = newMP;
        this.mpTxt.setText('' + this.scene.currentPlayer.magic);
      },
    });
  }

  reduceHealth(amount) {
    this.hpIcon.visible = true;
    this.hpIcon.setScale(0);

    this.scene.tweens.add({
      targets: this.hpIcon,
      scale: { from: 0, to: 0.45 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    var newHP = this.scene.currentPlayer.health - amount;
    if (newHP <= 0) {
      newHP = 0;
      this.killPlayer();
    }

    this.scene.tweens.addCounter({
      from: this.scene.currentPlayer.health,
      to: newHP,
      duration: 500,
      onUpdate: (tween) => {
        this.hpTxt.setText('' + Math.ceil(tween.getValue()));
      },
      onComplete: () => {
        this.scene.currentPlayer.health = newHP;
        this.hpTxt.setText('' + this.scene.currentPlayer.health);
      },
    });
  }

  increaseHealth(amount) {
    this.hpIcon.visible = true;
    this.hpIcon.setScale(0);

    this.scene.tweens.add({
      targets: this.hpIcon,
      scale: { from: 0, to: 0.45 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    var newHP = this.scene.currentPlayer.health + amount;
    var maxHP = GameCalculations.getMaxHealth(this.scene.currentPlayer);
    if (newHP > maxHP) {
      newHP = maxHP;
    }

    this.scene.tweens.addCounter({
      from: this.scene.currentPlayer.health,
      to: newHP,
      duration: 500,
      onUpdate: (tween) => {
        this.hpTxt.setText('' + Math.ceil(tween.getValue()));
      },
      onComplete: () => {
        this.scene.currentPlayer.health = newHP;
        this.hpTxt.setText('' + this.scene.currentPlayer.health);
      },
    });
  }

  increaseMagic(amount) {
    this.mpIcon.visible = true;
    this.mpIcon.setScale(0);

    this.scene.tweens.add({
      targets: this.mpIcon,
      scale: { from: 0, to: 0.45 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    var newMP = this.scene.currentPlayer.magic + amount;
    var maxMP = GameCalculations.getMaxMagic(this.scene.currentPlayer);
    if (newMP > maxMP) {
      newMP = maxMP;
    }

    this.scene.tweens.addCounter({
      from: this.scene.currentPlayer.magic,
      to: newMP,
      duration: 500,
      onUpdate: (tween) => {
        this.mpTxt.setText('' + Math.ceil(tween.getValue()));
      },
      onComplete: () => {
        this.scene.currentPlayer.magic = newMP;
        this.mpTxt.setText('' + this.scene.currentPlayer.magic);
      },
    });
  }

  increasePower(amount) {
    this.powIcon.visible = true;
    this.powIcon.setScale(0);

    this.scene.tweens.add({
      targets: this.powIcon,
      scale: { from: 0, to: 0.45 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    var newPow = this.scene.currentPlayer.power + amount;
    var maxPow = GameCalculations.getMaxPower(this.scene.currentPlayer);
    if (newPow > maxPow) {
      newPow = maxPow;
    }

    this.scene.tweens.addCounter({
      from: this.scene.currentPlayer.power,
      to: newPow,
      duration: 500,
      onUpdate: (tween) => {
        this.powTxt.setText('' + Math.ceil(tween.getValue()));
      },
      onComplete: () => {
        this.scene.currentPlayer.power = newPow;
        this.powTxt.setText('' + this.scene.currentPlayer.power);
      },
    });
  }

  destroy() {
    if (this.infoDialog.textContent) {
      this.infoDialog.textContent.destroy();
      this.infoDialog.textContent = null;
    }

    if (this.infoDialog) {
      this.infoDialog.destroy();
      this.infoDialog = null;
    }

    if (this.playerIcon) {
      this.playerIcon.destroy();
      this.playerIcon = null;
    }

    if (this.hpTxt) {
      this.hpTxt.destroy();
      this.hpTxt = null;
    }

    if (this.hpIcon) {
      this.hpIcon.destroy();
      this.hpIcon = null;
    }

    if (this.mpTxt) {
      this.mpTxt.destroy();
      this.mpTxt = null;
    }

    if (this.mpIcon) {
      this.mpIcon.destroy();
      this.mpIcon = null;
    }

    if (this.powTxt) {
      this.powTxt.destroy();
      this.powTxt = null;
    }

    if (this.powIcon) {
      this.powIcon.destroy();
      this.powIcon = null;
    }

    if (this.lvlTxt) {
      this.lvlTxt.destroy();
      this.lvlTxt = null;
    }

    if (this.lvlIcon) {
      this.lvlIcon.destroy();
      this.lvlIcon = null;
    }

    if (this.expTxt) {
      this.expTxt.destroy();
      this.expTxt = null;
    }

    if (this.expIcon) {
      this.expIcon.destroy();
      this.expIcon = null;
    }

    if (this.wepIconImg) {
      this.wepIconImg.destroy();
      this.wepIconImg = null;
    }

    if (this.wepIcon) {
      this.wepIcon.destroy();
      this.wepIcon = null;
    }

    if (this.statusTxt) {
      this.statusTxt.destroy();
      this.statusTxt = null;
    }

    if (this.statusIcon) {
      this.statusIcon.destroy();
      this.statusIcon = null;
    }

    console.log('kill player panel');
  }
}
