import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import GameCalculations from 'utils/gameCalculations';

export default class EnemyManager {
  constructor(scene) {
    //  The current Scene
    this.scene = scene;

    this.enemyList = this.scene.cache.json.get('enemies');
    this.enemyDead = false;

    this.setupEnemy();
  }

  setupEnemy() {
    this.enemyIcon = this.scene.add.sprite(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2 - 80,
      'enemyAtlas1',
      'wolf'
    );
    this.enemyIcon.alpha = 0;
    this.enemyIcon.setScale(0);

    this.currentEnemy = Utils.GlobalSettings.gameState.encounter.enemy;

    if (!this.currentEnemy) {
      if (Utils.GlobalSettings.gameState.encounter.type === 'green') {
        var amount = Object.keys(this.enemyList.green).length;
        this.currentEnemy = JSON.parse(
          JSON.stringify(
            this.enemyList.green[
              Utils.GlobalSettings.rnd.integerInRange(0, amount - 1)
            ]
          )
        );

        //TODO: Set enemy level based on floor level and difficulty
        this.currentEnemy.level = 1;
        this.currentEnemy.numActions = 3;
        this.currentEnemy.maxHealth = this.currentEnemy.health;
        this.currentEnemy.maxPower = this.currentEnemy.power;
        this.currentEnemy.defenseBonus = 0;
        this.currentEnemy.powerBonus = 0;
        this.currentEnemy.status =
          Utils.GlobalSettings.text.battle.status_normal;

        Utils.GlobalSettings.gameState.encounter.enemy = this.currentEnemy;
        Utils.saveGameState();
      }
    }

    if (this.currentEnemy) {
      this.setupEnemyAI();

      this.enemyIcon.setFrame(this.currentEnemy.image);

      this.enemyNameTxt = this.scene.add.bitmapText(
        this.enemyIcon.x,
        this.enemyIcon.y - this.enemyIcon.height / 2 + 10,
        'sakkalbold',
        this.currentEnemy.name,
        35
      );
      this.enemyNameTxt.setTint(0x00ff00);
      this.enemyNameTxt.setOrigin(0.5);
      this.enemyNameTxt.alpha = 0;

      this.hpIcon = this.createStatIcon(
        this.enemyIcon.x + 70,
        this.enemyIcon.y + 60,
        'rune_health',
        '' + this.currentEnemy.health
      );
      this.hpTxt = this.hpIcon.iconTxt;

      this.lvlIcon = this.createStatIcon(
        this.enemyIcon.x - 70,
        this.enemyIcon.y - 60,
        'rune_level',
        '' + this.currentEnemy.level
      );
      this.lvlTxt = this.lvlIcon.iconTxt;

      var currentStatus = this.currentEnemy.status.toLowerCase();

      this.statusIcon = this.createStatIcon(
        this.enemyIcon.x + 50,
        this.enemyIcon.y - 40,
        'status_' + currentStatus,
        ''
      );
      this.statusTxt = this.statusIcon.iconTxt;

      this.showEnemyPanel(2500);
    }
  }

  setupEnemyAI() {
    //TODO: improve enemy AI
    this.currentEnemy.selectedActions = [];
    var actionFrames = ['rune_attack', 'rune_defend', 'rune_item'];
    var actionTypes = ['skill', 'defense', 'item'];

    var prevActionType = '';
    for (var i = 0; i < this.currentEnemy.numActions; i++) {
      var rndActionType = Phaser.Math.RND.integerInRange(
        0,
        actionTypes.length - 1
      );

      var actionType = actionTypes[rndActionType];

      if (actionType === 'defense' && actionType === prevActionType) {
        rndActionType = Phaser.Math.RND.pick([0, 2]);
        actionType = actionTypes[rndActionType];
      }

      if (actionType === 'item' && this.currentEnemy.items.length === 0) {
        rndActionType = Phaser.Math.RND.integerInRange(0, 1);
        actionType = actionTypes[rndActionType];

        if (actionType === 'defense' && actionType === prevActionType) {
          rndActionType = 0;
          actionType = actionTypes[rndActionType];
        }
      }

      var rndAction = 0;

      switch (actionType) {
        case 'skill':
          rndAction = Phaser.Math.RND.pick(this.currentEnemy.skills);
          break;

        case 'defense':
          rndAction = Phaser.Math.RND.pick(this.currentEnemy.defense);
          break;

        case 'item':
          rndAction = Phaser.Math.RND.pick(this.currentEnemy.items);
          break;
      }

      prevActionType = actionType;

      this.currentEnemy.selectedActions.push({
        type: actionType,
        frame: actionFrames[rndActionType],
        action: rndAction,
      });
    }
  }

  doAction(action, onComplete, isStatusEffectAction = false) {
    if (!this.enemyDead) {
      this.scene.actionManager.handleAction(action, false, onComplete);
    } else {
      if (onComplete && typeof onComplete === 'function') {
        this.scene.time.delayedCall(
          500,
          () => {
            onComplete();
          },
          [],
          this
        );
      }
    }
  }

  killEnemy() {
    this.scene.battleUI.timelinePanel.canAdvance = false;
    this.enemyDead = true;
    this.scene.effectManager.doEffect(5, {
      x: this.scene.enemyManager.enemyIcon.x,
      y: this.scene.enemyManager.enemyIcon.y,
    });
    this.hideEnemyPanel();
  }

  removeStatusEffect() {
    this.currentEnemy.status = Utils.GlobalSettings.text.battle.status_normal;
    this.statusIcon.visible = true;
    this.statusIcon.setScale(0.45);
    this.statusIcon.alpha = 1;

    this.scene.tweens.add({
      targets: this.statusIcon,
      scale: { from: 0.45, to: 1 },
      alpha: { from: 1, to: 0 },
      ease: 'Back.easeIn',
      duration: 500,
      onComplete: () => {
        this.statusIcon.visible = false;
        this.statusIcon.setScale(0.45);
        this.statusIcon.alpha = 1;
      },
    });
  }

  applyStatusEffect(status, duration) {
    this.currentEnemy.status = status;
    this.statusIcon.visible = true;
    this.statusIcon.alpha = 1;
    this.statusIcon.setScale(0);
    this.statusIcon.setFrame('status_' + status.toLowerCase());

    this.scene.tweens.add({
      targets: this.statusIcon,
      scale: { from: 0, to: 0.45 },
      ease: 'Back.easeOut',
      duration: 500,
    });
  }

  reduceSpeed(amount) {
    var newSpeed = this.currentEnemy.speed - amount;
    if (newSpeed < 0) {
      newSpeed = 0;
    }
    this.currentEnemy.speed = newSpeed;
  }

  reducePower(amount) {
    var newPow = this.currentEnemy.power - amount;
    if (newPow < 0) {
      newPow = 0;
    }
    this.currentEnemy.power = newPow;
  }

  reduceMagic(amount) {}

  reduceHealth(amount) {
    this.hpIcon.visible = true;
    this.hpIcon.setScale(0);

    this.scene.tweens.add({
      targets: this.hpIcon,
      scale: { from: 0, to: 0.45 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    var newHP = this.currentEnemy.health - amount;
    if (newHP <= 0) {
      newHP = 0;
      this.killEnemy();
    }

    this.scene.tweens.addCounter({
      from: this.currentEnemy.health,
      to: newHP,
      duration: 500,
      onUpdate: (tween) => {
        this.hpTxt.setText('' + Math.ceil(tween.getValue()));
      },
      onComplete: () => {
        this.currentEnemy.health = newHP;
        this.hpTxt.setText('' + this.currentEnemy.health);
      },
    });
  }

  increaseHealth(amount) {
    this.hpIcon.visible = true;
    this.hpIcon.setScale(0);

    this.scene.tweens.add({
      targets: this.hpIcon,
      scale: { from: 0, to: 0.45 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    var newHP = this.currentEnemy.health + amount;
    var maxHP = this.currentEnemy.maxHealth;
    if (newHP > maxHP) {
      newHP = maxHP;
    }

    this.scene.tweens.addCounter({
      from: this.currentEnemy.health,
      to: newHP,
      duration: 500,
      onUpdate: (tween) => {
        this.hpTxt.setText('' + Math.ceil(tween.getValue()));
      },
      onComplete: () => {
        this.currentEnemy.health = newHP;
        this.hpTxt.setText('' + this.currentEnemy.health);
      },
    });
  }

  increaseMagic(amount) {}

  increasePower(amount) {
    var newPow = this.currentEnemy.power + amount;
    var maxPow = this.currentEnemy.maxPower;
    if (newPow > maxPow) {
      newPow = maxPow;
    }
    this.currentEnemy.power = newPow;
  }

  showEnemyPanel(delay = 0) {
    this.enemyIcon.visible = true;
    this.enemyIcon.setScale(0);
    this.enemyIcon.alpha = 0;

    this.scene.tweens.add({
      targets: [this.enemyIcon],
      scale: { from: 0, to: 1 },
      alpha: { from: 0, to: 1 },
      ease: 'Back.easeOut',
      duration: 1000,
      delay: delay,
    });

    this.enemyNameTxt.visible = true;
    this.enemyNameTxt.alpha = 0;
    this.enemyNameTxt.y = this.enemyIcon.y - this.enemyIcon.height / 2 + 10;

    this.scene.tweens.add({
      targets: [this.enemyNameTxt],
      alpha: { from: 0, to: 1 },
      y: {
        from: this.enemyIcon.y - this.enemyIcon.height / 2 + 10,
        to: this.enemyIcon.y - this.enemyIcon.height / 2 - 40,
      },
      ease: 'Sine.easeOut',
      duration: 1000,
      delay: delay,
    });

    var currentStatus = this.currentEnemy.status;

    var icons = [this.lvlIcon, this.hpIcon, this.statusIcon];
    for (var i = 0; i < icons.length; i++) {
      icons[i].visible = true;
      icons[i].setScale(0);

      if (
        icons[i] === this.statusIcon &&
        currentStatus === Utils.GlobalSettings.text.battle.status_normal
      ) {
        icons[i].visible = false;
      }

      this.scene.tweens.add({
        targets: icons[i],
        scale: { from: 0, to: 0.45 },
        ease: 'Back.easeOut',
        duration: 500,
        delay: delay + 500 + i * 100,
      });
    }

    var txts = [this.lvlTxt, this.hpTxt];
    for (var i = 0; i < txts.length; i++) {
      txts[i].visible = true;
      txts[i].alpha = 0;

      this.scene.tweens.add({
        targets: txts[i],
        alpha: { from: 0, to: 1 },
        ease: 'Linear.easeOut',
        duration: 500,
        delay: delay + 500 + i * 100,
      });
    }
  }

  hideEnemyPanel(delay = 0) {
    this.enemyIcon.visible = true;
    this.enemyIcon.setScale(1);
    this.enemyIcon.alpha = 1;

    this.scene.tweens.add({
      targets: [this.enemyIcon],
      scale: { from: 1, to: 0 },
      alpha: { from: 1, to: 0 },
      ease: 'Back.easeIn',
      duration: 1000,
      delay: delay,
    });

    this.enemyNameTxt.visible = true;
    this.enemyNameTxt.alpha = 1;
    this.enemyNameTxt.y = this.enemyIcon.y - this.enemyIcon.height / 2 - 40;

    this.scene.tweens.add({
      targets: [this.enemyNameTxt],
      alpha: { from: 1, to: 0 },
      y: {
        from: this.enemyIcon.y - this.enemyIcon.height / 2 - 40,
        to: this.enemyIcon.y - this.enemyIcon.height / 2 + 10,
      },
      ease: 'Sine.easeIn',
      duration: 1000,
      delay: delay,
    });

    var currentStatus = this.currentEnemy.status;

    var icons = [this.lvlIcon, this.hpIcon, this.statusIcon];
    for (var i = 0; i < icons.length; i++) {
      icons[i].visible = true;
      icons[i].setScale(0.45);

      if (
        icons[i] === this.statusIcon &&
        currentStatus === Utils.GlobalSettings.text.battle.status_normal
      ) {
        icons[i].visible = false;
      }

      this.scene.tweens.add({
        targets: icons[i],
        scale: { from: 0.45, to: 0 },
        ease: 'Back.easeIn',
        duration: 500,
        delay: delay + i * 100,
        onComplete: (tween) => {
          tween.targets[0].visible = false;
        },
      });
    }

    var txts = [this.lvlTxt, this.hpTxt];
    for (var i = 0; i < txts.length; i++) {
      txts[i].visible = true;
      txts[i].alpha = 1;

      this.scene.tweens.add({
        targets: txts[i],
        alpha: { from: 1, to: 0 },
        ease: 'Linear.easeOut',
        duration: 500,
        delay: delay + i * 100,
        onComplete: (tween) => {
          tween.targets[0].visible = false;
        },
      });
    }
  }

  createStatIcon(x, y, frame, text) {
    var icon = this.scene.add.image(x, y, 'atlas1', frame);
    icon.setInteractive();
    icon.setScale(0);

    icon.on('pointerover', (event) => {
      icon.setTint(0x00ff00);
    });

    icon.on('pointerout', (event) => {
      icon.setTint(0xffffff);
    });

    icon.on('pointerup', () => {
      var toastMsg = '';
      switch (icon.frame.name) {
        case 'rune_health':
          toastMsg =
            Utils.GlobalSettings.text.battle.health +
            ': ' +
            this.currentEnemy.health +
            '/' +
            this.currentEnemy.maxHealth;
          break;

        case 'rune_level':
          toastMsg =
            Utils.GlobalSettings.text.battle.level +
            ': ' +
            this.currentEnemy.level;
          break;
      }

      if (icon.frame.name.startsWith('status_')) {
        toastMsg =
          Utils.GlobalSettings.text.battle.status +
          ': ' +
          Utils.GlobalSettings.text.battle[icon.frame.name];
      }

      UIHelper.showToast(
        this.scene,
        this.scene.toast,
        icon.x,
        icon.y - 60,
        toastMsg
      );
    });

    var txt = this.scene.add.bitmapText(icon.x, icon.y, 'sakkalbold', text, 38);
    txt.alpha = 0;
    txt.setOrigin(0.5);

    icon.iconTxt = txt;
    return icon;
  }

  destroy() {
    this.enemyList = null;

    if (this.enemyIcon) {
      this.enemyIcon.destroy();
      this.enemyIcon = null;
    }

    this.currentEnemy = null;

    if (this.enemyNameTxt) {
      this.enemyNameTxt.destroy();
      this.enemyNameTxt = null;
    }

    if (this.hpTxt) {
      this.hpTxt.destroy();
      this.hpTxt = null;
    }

    if (this.hpIcon) {
      this.hpIcon.destroy();
      this.hpIcon = null;
    }

    if (this.lvlTxt) {
      this.lvlTxt.destroy();
      this.lvlTxt = null;
    }

    if (this.lvlIcon) {
      this.lvlIcon.destroy();
      this.lvlIcon = null;
    }

    if (this.statusTxt) {
      this.statusTxt.destroy();
      this.statusTxt = null;
    }

    if (this.statusIcon) {
      this.statusIcon.destroy();
      this.statusIcon = null;
    }

    console.log('kill enemy manager');
  }
}
