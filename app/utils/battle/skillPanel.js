import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';

export default class SkillPanel {
  constructor(scene) {
    this.scene = scene;
  }

  showAttackPanel() {
    this.skillPanel.dialogTxt.setText(Utils.GlobalSettings.text.battle.skills);
    this.skillPanel.dialogTxt.x =
      this.skillPanel.x - (this.skillPanel.width / 2 - 30);
    this.resizeSkillPanel(this.scene.currentPlayer.skills.length);

    this.skillPanel.visible = true;
    this.skillPanel.dialogTxt.visible = true;
    this.skillPanel.alpha = 0;
    this.skillPanel.dialogTxt.alpha = 0;

    this.scene.tweens.add({
      targets: [this.skillPanel, this.skillPanel.dialogTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500
    });

    for (var i = 0; i < this.scene.currentPlayer.skills.length; i++) {
      var skill =
        Utils.GlobalSettings.skillsList[this.scene.currentPlayer.skills[i]];
      var name =
        skill.name +
        (skill.magic_cost > 0 ? '  (' + skill.magic_cost + ' MP)' : '');
      var desc = skill.requirements + ' (' + skill.example + ')';
      if (i === 0) {
        this.showSkillButton(this.skillButton1, name, desc);
      } else if (i === 1) {
        this.showSkillButton(this.skillButton2, name, desc);
      } else if (i === 2) {
        this.showSkillButton(this.skillButton3, name, desc);
      } else if (i === 3) {
        this.showSkillButton(this.skillButton4, name, desc);
      }
    }
  }

  showDefendPanel() {
    this.skillPanel.dialogTxt.setText(Utils.GlobalSettings.text.battle.defense);
    this.skillPanel.dialogTxt.x =
      this.skillPanel.x - (this.skillPanel.width / 2 - 40);
    this.resizeSkillPanel(3);

    this.skillPanel.visible = true;
    this.skillPanel.dialogTxt.visible = true;
    this.skillPanel.alpha = 0;
    this.skillPanel.dialogTxt.alpha = 0;

    this.scene.tweens.add({
      targets: [this.skillPanel, this.skillPanel.dialogTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500
    });

    for (var i = 0; i < 3; i++) {
      var skill = Utils.GlobalSettings.defenseList[i];
      var name = skill.name;
      var desc = skill.requirements + ' (' + skill.example + ')';
      if (i === 0) {
        this.showSkillButton(this.skillButton1, name, desc);
      } else if (i === 1) {
        this.showSkillButton(this.skillButton2, name, desc);
      } else if (i === 2) {
        this.showSkillButton(this.skillButton3, name, desc);
      }
    }
  }

  showItemPanel() {
    this.skillPanel.dialogTxt.setText(Utils.GlobalSettings.text.battle.items);
    this.skillPanel.dialogTxt.x =
      this.skillPanel.x - (this.skillPanel.width / 2 - 30);
    this.resizeSkillPanel(this.scene.currentPlayer.items.length);

    this.skillPanel.visible = true;
    this.skillPanel.dialogTxt.visible = true;
    this.skillPanel.alpha = 0;
    this.skillPanel.dialogTxt.alpha = 0;

    this.scene.tweens.add({
      targets: [this.skillPanel, this.skillPanel.dialogTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500
    });

    for (var i = 0; i < this.scene.currentPlayer.items.length; i++) {
      var item =
        Utils.GlobalSettings.itemsList[this.scene.currentPlayer.items[i]];
      var name = item.name;
      var desc = item.requirements + ' (' + item.example + ')';
      if (i === 0) {
        this.showSkillButton(this.skillButton1, name, desc);
      } else if (i === 1) {
        this.showSkillButton(this.skillButton2, name, desc);
      } else if (i === 2) {
        this.showSkillButton(this.skillButton3, name, desc);
      } else if (i === 3) {
        this.showSkillButton(this.skillButton4, name, desc);
      }
    }
  }

  resizeSkillPanel(numButtons) {
    this.toggleSkillButtonVisibility(this.skillButton1, false);
    this.toggleSkillButtonVisibility(this.skillButton2, false);
    this.toggleSkillButtonVisibility(this.skillButton3, false);
    this.toggleSkillButtonVisibility(this.skillButton4, false);

    switch (numButtons) {
      case 0:
        this.skillPanel.y = Utils.GlobalSettings.height - 200;
        this.skillPanel.setDisplaySize(this.skillPanel.width, 105);
        this.skillPanel.height = 105;

        this.skillPanel.dialogTxt.y =
          this.skillPanel.y - this.skillPanel.height / 2 + 135;
        break;
      case 1:
        this.skillPanel.y = Utils.GlobalSettings.height - 200;
        this.skillPanel.setDisplaySize(this.skillPanel.width, 105);
        this.skillPanel.height = 105;

        this.skillPanel.dialogTxt.y =
          this.skillPanel.y - this.skillPanel.height / 2 + 135;

        this.setSkillButtonYPosition(
          this.skillButton1,
          this.skillPanel.dialogTxt.y + 15
        );
        this.toggleSkillButtonVisibility(this.skillButton1, true);
        break;
      case 2:
        this.skillPanel.y = Utils.GlobalSettings.height - 200;
        this.skillPanel.setDisplaySize(this.skillPanel.width, 190);
        this.skillPanel.height = 190;

        this.skillPanel.dialogTxt.y =
          this.skillPanel.y - this.skillPanel.height / 2 + 90;

        this.setSkillButtonYPosition(
          this.skillButton1,
          this.skillPanel.dialogTxt.y + 15
        );
        this.setSkillButtonYPosition(
          this.skillButton2,
          this.skillButton1.y + this.skillButton1.height + 10
        );
        this.toggleSkillButtonVisibility(this.skillButton1, true);
        this.toggleSkillButtonVisibility(this.skillButton2, true);
        break;
      case 3:
        this.skillPanel.y = Utils.GlobalSettings.height - 200;
        this.skillPanel.setDisplaySize(this.skillPanel.width, 275);
        this.skillPanel.height = 275;

        this.skillPanel.dialogTxt.y =
          this.skillPanel.y - this.skillPanel.height / 2 + 50;

        this.setSkillButtonYPosition(
          this.skillButton1,
          this.skillPanel.dialogTxt.y + 15
        );
        this.setSkillButtonYPosition(
          this.skillButton2,
          this.skillButton1.y + this.skillButton1.height + 10
        );
        this.setSkillButtonYPosition(
          this.skillButton3,
          this.skillButton2.y + this.skillButton2.height + 10
        );
        this.toggleSkillButtonVisibility(this.skillButton1, true);
        this.toggleSkillButtonVisibility(this.skillButton2, true);
        this.toggleSkillButtonVisibility(this.skillButton3, true);
        break;
      case 4:
        this.skillPanel.y = Utils.GlobalSettings.height - 200;
        this.skillPanel.setDisplaySize(this.skillPanel.width, 370);
        this.skillPanel.height = 370;

        this.skillPanel.dialogTxt.y =
          this.skillPanel.y - this.skillPanel.height / 2;

        this.setSkillButtonYPosition(
          this.skillButton1,
          this.skillPanel.dialogTxt.y + 15
        );
        this.setSkillButtonYPosition(
          this.skillButton2,
          this.skillButton1.y + this.skillButton1.height + 10
        );
        this.setSkillButtonYPosition(
          this.skillButton3,
          this.skillButton2.y + this.skillButton2.height + 10
        );
        this.setSkillButtonYPosition(
          this.skillButton4,
          this.skillButton3.y + this.skillButton3.height + 10
        );
        this.toggleSkillButtonVisibility(this.skillButton1, true);
        this.toggleSkillButtonVisibility(this.skillButton2, true);
        this.toggleSkillButtonVisibility(this.skillButton3, true);
        this.toggleSkillButtonVisibility(this.skillButton4, true);
        break;
    }
    this.skillPanel.layout();
  }

  setupSkillPanel() {
    this.skillPanel = this.createDialogPanel(
      Utils.GlobalSettings.width / 2 + 110,
      Utils.GlobalSettings.height - 200,
      400,
      370,
      Utils.GlobalSettings.text.battle.skills
    );

    this.skillButton1 = UIHelper.createSkillButton(
      this.scene,
      this.skillPanel.x - 190,
      this.skillPanel.y - this.skillPanel.height / 2 + 20,
      'Basic  Attack  1',
      'Any Pair ½¼¾%®©@'
    );

    this.skillButton2 = UIHelper.createSkillButton(
      this.scene,
      this.skillButton1.x,
      this.skillButton1.y + this.skillButton1.height + 10,
      'Basic  Attack  2',
      'Any Pair ½¼¾%®©@'
    );

    this.skillButton3 = UIHelper.createSkillButton(
      this.scene,
      this.skillButton2.x,
      this.skillButton2.y + this.skillButton2.height + 10,
      'Basic  Attack  3',
      'Any Pair ½¼¾%®©@'
    );

    this.skillButton4 = UIHelper.createSkillButton(
      this.scene,
      this.skillButton3.x,
      this.skillButton3.y + this.skillButton3.height + 10,
      'Basic  Attack  4',
      'Any Pair ½¼¾%®©@'
    );

    this.hideSkillPanel();

    this.skillButton1.on('pointerup', () => {
      this.skillButtonPressed(0, this.skillButton1);
    });

    this.skillButton2.on('pointerup', () => {
      this.skillButtonPressed(1, this.skillButton2);
    });

    this.skillButton3.on('pointerup', () => {
      this.skillButtonPressed(2, this.skillButton3);
    });

    this.skillButton4.on('pointerup', () => {
      this.skillButtonPressed(3, this.skillButton4);
    });
  }

  hideSkillPanel() {
    this.skillPanel.visible = false;
    this.skillPanel.dialogTxt.visible = false;
    this.toggleSkillButtonVisibility(this.skillButton1, false);
    this.toggleSkillButtonVisibility(this.skillButton2, false);
    this.toggleSkillButtonVisibility(this.skillButton3, false);
    this.toggleSkillButtonVisibility(this.skillButton4, false);
  }

  skillButtonPressed(buttonNum, button) {
    this.scene.battleUI.actionPanel.showActionConfirmation(buttonNum, button);
  }

  toggleSkillButtonVisibility(button, visible) {
    button.visible = visible;
    button.skillTxt.visible = visible;
    button.skillDescriptionTxt.visible = visible;
  }

  toggleSkillButtonAlpha(button, alpha) {
    button.alpha = alpha;
    button.skillTxt.alpha = alpha;
    button.skillDescriptionTxt.alpha = alpha;
  }

  showSkillButton(button, skillName, skillDescription) {
    this.toggleSkillButtonVisibility(button, true);
    this.toggleSkillButtonAlpha(button, 0);
    button.skillTxt.setText(skillName);
    button.skillDescriptionTxt.setText(skillDescription);

    this.scene.tweens.add({
      targets: [button, button.skillTxt, button.skillDescriptionTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 250,
      delay: 100
    });
  }

  setSkillButtonYPosition(button, y) {
    button.y = y;
    button.skillTxt.y = button.y + 5;
    button.skillDescriptionTxt.y = button.skillTxt.y + 30;
  }

  createDialogPanel(x, y, width, height, text) {
    var dialog = this.scene.rexUI.add
      .sizer({
        orientation: 'y',
        x: x,
        y: y,
        width: width,
        height: height
      })
      .addBackground(
        this.scene.rexUI.add
          .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
          .setStrokeStyle(1, UIHelper.COLOR_LIGHT)
      )
      .layout();

    var dialogTxt = this.scene.add.bitmapText(
      dialog.x - (dialog.width / 2 - 30),
      dialog.y - dialog.height / 2,
      'sakkalbold',
      text,
      30
    );
    dialogTxt.setOrigin(0.5);

    dialog.dialogTxt = dialogTxt;
    return dialog;
  }

  destroy() {
    if (this.skillPanel.dialogTxt) {
      this.skillPanel.dialogTxt.destroy();
      this.skillPanel.dialogTxt = null;
    }

    if (this.skillPanel) {
      this.skillPanel.destroy();
      this.skillPanel = null;
    }

    if (this.skillButton1.skillDescriptionTxt) {
      this.skillButton1.skillDescriptionTxt.destroy();
      this.skillButton1.skillDescriptionTxt = null;
    }

    if (this.skillButton1.skillTxt) {
      this.skillButton1.skillTxt.destroy();
      this.skillButton1.skillTxt = null;
    }

    if (this.skillButton1) {
      this.skillButton1.destroy();
      this.skillButton1 = null;
    }

    if (this.skillButton2.skillDescriptionTxt) {
      this.skillButton2.skillDescriptionTxt.destroy();
      this.skillButton2.skillDescriptionTxt = null;
    }

    if (this.skillButton2.skillTxt) {
      this.skillButton2.skillTxt.destroy();
      this.skillButton2.skillTxt = null;
    }

    if (this.skillButton2) {
      this.skillButton2.destroy();
      this.skillButton2 = null;
    }

    if (this.skillButton3.skillDescriptionTxt) {
      this.skillButton3.skillDescriptionTxt.destroy();
      this.skillButton3.skillDescriptionTxt = null;
    }

    if (this.skillButton3.skillTxt) {
      this.skillButton3.skillTxt.destroy();
      this.skillButton3.skillTxt = null;
    }

    if (this.skillButton3) {
      this.skillButton3.destroy();
      this.skillButton3 = null;
    }

    if (this.skillButton4.skillDescriptionTxt) {
      this.skillButton4.skillDescriptionTxt.destroy();
      this.skillButton4.skillDescriptionTxt = null;
    }

    if (this.skillButton4.skillTxt) {
      this.skillButton4.skillTxt.destroy();
      this.skillButton4.skillTxt = null;
    }

    if (this.skillButton4) {
      this.skillButton4.destroy();
      this.skillButton4 = null;
    }

    console.log('kill skill panel');
  }
}
