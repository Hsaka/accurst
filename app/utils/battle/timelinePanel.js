import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import GameCalculations from 'utils/gameCalculations';

export default class TimelinePanel {
  constructor(scene) {
    this.scene = scene;
    this.canAdvance = true;
  }

  setupTimeline() {
    this.enemyTimelineIcon = this.scene.add.image(
      65,
      130,
      'enemyAtlas1',
      this.scene.enemyManager.currentEnemy.image
    );
    this.enemyTimelineIcon.setDisplaySize(55, 55);
    this.enemyTimelineIcon.alpha = 0;

    this.enemyTimeline = this.scene.add.image(
      this.enemyTimelineIcon.x + 50,
      this.enemyTimelineIcon.y,
      'atlas1',
      'timeline'
    );
    this.enemyTimeline.setOrigin(0, 0.5);
    this.enemyTimeline.alpha = 0;

    this.playerTimelineIcon = this.scene.add.image(
      this.enemyTimelineIcon.x,
      this.enemyTimelineIcon.y + 70,
      'atlas1',
      Utils.GlobalSettings.heroList[this.scene.currentPlayer.classType].image
    );
    this.playerTimelineIcon.setDisplaySize(55, 55);
    this.playerTimelineIcon.alpha = 0;

    this.playerTimeline = this.scene.add.image(
      this.enemyTimeline.x,
      this.playerTimelineIcon.y,
      'atlas1',
      'timeline'
    );
    this.playerTimeline.setOrigin(0, 0.5);
    this.playerTimeline.alpha = 0;

    this.playerActions = [];
    this.enemyActions = [];
    for (var i = 0; i < 8; i++) {
      var spr1 = this.scene.add.image(
        this.playerTimeline.x + 25 + 62 * i,
        this.playerTimeline.y,
        'atlas1',
        'rune_attack'
      );
      spr1.setScale(0.35);
      spr1.visible = false;
      spr1.alpha = 0;
      this.playerActions.push(spr1);

      var spr2 = this.scene.add.image(
        this.enemyTimeline.x + 25 + 62 * i,
        this.enemyTimeline.y,
        'atlas1',
        'rune_random'
      );
      spr2.setScale(0.35);
      spr2.visible = true;
      spr2.alpha = 0;

      if (this.scene.enemyManager.currentEnemy.numActions <= i) {
        spr2.visible = false;
      }
      this.enemyActions.push(spr2);
    }

    this.timelineArrow = this.scene.add.image(
      this.enemyTimeline.x + 25,
      this.enemyTimeline.y + 35,
      'atlas1',
      'arrow'
    );
    this.timelineArrow.alpha = 0.4;
    this.timelineArrow.visible = false;

    this.playerActionCount = 0;
    this.enemyActionCount = this.scene.enemyManager.currentEnemy.numActions;
    this.timelineIndex = 0;
    this.totalCardsUsed = 0;

    this.scene.tweens.add({
      targets: [
        this.playerTimelineIcon,
        this.playerTimeline,
        this.enemyTimelineIcon,
        this.enemyTimeline
      ],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: 3000
    });

    this.scene.tweens.add({
      targets: [...this.enemyActions],
      alpha: { from: 0, to: 1 },
      scale: { from: 0, to: 0.35 },
      ease: 'Back.easeOut',
      duration: 500,
      delay: 3500
    });
  }

  hideTimeline(delay = 0) {
    this.playerTimelineIcon.visible = true;
    this.playerTimeline.visible = true;
    this.enemyTimelineIcon.visible = true;
    this.enemyTimeline.visible = true;
    this.timelineArrow.visible = true;

    this.playerTimelineIcon.alpha = 1;
    this.playerTimeline.alpha = 1;
    this.enemyTimelineIcon.alpha = 1;
    this.enemyTimeline.alpha = 1;
    this.timelineArrow.alpha = 0.4;

    this.scene.tweens.add({
      targets: [...this.enemyActions],
      alpha: { from: 1, to: 0 },
      scale: { from: 0.35, to: 0 },
      ease: 'Back.easeIn',
      duration: 500,
      delay: delay
    });

    this.scene.tweens.add({
      targets: [...this.playerActions],
      alpha: { from: 1, to: 0 },
      scale: { from: 0.35, to: 0 },
      ease: 'Back.easeIn',
      duration: 500,
      delay: delay
    });

    this.scene.tweens.add({
      targets: [
        this.playerTimelineIcon,
        this.playerTimeline,
        this.enemyTimelineIcon,
        this.enemyTimeline
      ],
      alpha: { from: 1, to: 0 },
      ease: 'Linear.easeIn',
      duration: 500,
      delay: delay + 500
    });

    this.scene.tweens.add({
      targets: [this.timelineArrow],
      alpha: { from: 0.4, to: 0 },
      ease: 'Linear.easeIn',
      duration: 500,
      delay: delay + 500
    });
  }

  showTimelineArrow(delay = 0) {
    this.timelineArrow.alpha = 0;
    this.timelineArrow.visible = true;
    this.timelineArrow.setScale(0);

    this.scene.tweens.add({
      targets: [this.timelineArrow],
      alpha: { from: 0, to: 0.4 },
      scale: { from: 0, to: 1 },
      ease: 'Back.easeOut',
      duration: 500,
      delay: delay,
      onComplete: () => {
        this.executeStatusEffects();
      }
    });
  }

  revealEnemyActions(delay = 0) {
    for (var i = 0; i < this.enemyActionCount; i++) {
      var aiAction = this.scene.enemyManager.currentEnemy.selectedActions[i];

      var enemyAction = this.enemyActions[i];
      enemyAction.action = aiAction;
      enemyAction.visible = true;
      enemyAction.alpha = 0;
      enemyAction.setScale(0);
      enemyAction.setFrame(aiAction.frame);

      this.scene.tweens.add({
        targets: [enemyAction],
        alpha: { from: 0, to: 1 },
        scale: { from: 0, to: 0.35 },
        ease: 'Back.easeOut',
        duration: 500,
        delay: delay + i * 100
      });
    }
  }

  startTimeline() {
    this.timelineStarted = true;
    this.timelineAtBeginning = true;

    this.timelineArrow.x = this.enemyTimeline.x - 50;
    this.timelineArrow.y = this.enemyTimeline.y + 35;

    this.scene.battleUI.playerPanel.playerIcon.disableInteractive();

    this.revealEnemyActions();
    this.showTimelineArrow(1000);
  }

  timelineCompleted() {
    this.hideTimeline();

    if (
      !this.scene.enemyManager.enemyDead &&
      !this.scene.battleUI.playerPanel.playerDead
    ) {
      //go to next turn
      Utils.GlobalSettings.gameState.encounter.combatTurn++;
      if (
        Utils.GlobalSettings.gameState.encounter.combatTurn >=
        Utils.GlobalSettings.gameState.numPlayers
      ) {
        Utils.GlobalSettings.gameState.encounter.combatTurn = 0;
      }

      Utils.GlobalSettings.gameState.encounter.combatRound++;
      Utils.saveGameState();

      if (!this.scene.battleUI.playerPanel.playerDead) {
        this.scene.battleUI.playerPanel.hidePlayerPanel(1000);
      }

      if (!this.scene.enemyManager.enemyDead) {
        this.scene.enemyManager.hideEnemyPanel(1000);
      }

      this.scene.time.delayedCall(
        2100,
        () => {
          this.scene.scene.start('battle', { from: 'battle' });
        },
        [],
        this
      );
    } else {
      //go to game over or reward screen
      if (!this.scene.battleUI.playerPanel.playerDead) {
        this.scene.battleUI.playerPanel.hidePlayerPanel(1000);
        this.scene.battleUI.rewardPanel.showRewardPanel(2000);
      } else if (!this.scene.enemyManager.enemyDead) {
        this.scene.enemyManager.hideEnemyPanel(1000);
        if (Utils.GlobalSettings.gameState.numPlayers === 1) {
          //TODO: game over scene
          this.scene.time.delayedCall(
            2000,
            () => {
              this.scene.scene.start('main', { from: 'battle' });
            },
            [],
            this
          );
        } else {
          this.scene.battleUI.deathPanel.showDeathPanel(2000, false);
        }
      }
    }
  }

  advanceTimeline(delay = 0) {
    if (this.timelineAtBeginning) {
      if (this.canAdvance) {
        this.scene.tweens.add({
          targets: [this.timelineArrow],
          x: { from: this.enemyTimeline.x - 50, to: this.enemyTimeline.x + 25 },
          ease: 'Back.easeOut',
          duration: 1000,
          delay: delay,
          onComplete: () => {
            this.timelineAtBeginning = false;
            this.executeTimelineActions();
          }
        });
      } else {
        this.timelineCompleted();
      }
    } else {
      var highest = 7;
      if (this.playerActionCount > this.enemyActionCount) {
        highest = this.playerActionCount - 1;
      } else {
        highest = this.enemyActionCount - 1;
      }
      if (this.timelineIndex < highest && this.canAdvance) {
        this.scene.tweens.add({
          targets: [this.timelineArrow],
          x: { from: this.timelineArrow.x, to: this.timelineArrow.x + 62 },
          ease: 'Back.easeOut',
          duration: 1000,
          delay: delay,
          onComplete: () => {
            this.timelineIndex++;
            this.executeTimelineActions();
          }
        });
      } else {
        this.timelineCompleted();
      }
    }
  }

  executeStatusEffects() {
    var enemySpeed = this.scene.enemyManager.currentEnemy.speed;
    var playerSpeed = GameCalculations.getSpeed(this.scene.currentPlayer);
    var enemyFirstToAct = enemySpeed > playerSpeed;
    var enemyStatus = this.scene.enemyManager.currentEnemy.status;
    var playerStatus = GameCalculations.getStatus(this.scene.currentPlayer);

    var enemyAction = {
      type: 'skill',
      frame: 'rune_attack',
      action: 0,
      num_cards: 0
    };

    switch (enemyStatus) {
      case Utils.GlobalSettings.text.battle.status_stunned:
        enemyAction.action = 0;
        break;

      case Utils.GlobalSettings.text.battle.status_poisoned:
        enemyAction.action = 1;
        break;

      case Utils.GlobalSettings.text.battle.status_bleeding:
        enemyAction.action = 2;
        break;

      case Utils.GlobalSettings.text.battle.status_slowed:
        enemyAction.action = 3;
        break;

      case Utils.GlobalSettings.text.battle.status_weakened:
        enemyAction.action = 4;
        break;
    }

    var playerAction = {
      type: 'skill',
      frame: 'rune_attack',
      action: 0,
      num_cards: 0
    };

    switch (playerStatus) {
      case Utils.GlobalSettings.text.battle.status_stunned:
        playerAction.action = 0;
        break;

      case Utils.GlobalSettings.text.battle.status_poisoned:
        playerAction.action = 1;
        break;

      case Utils.GlobalSettings.text.battle.status_bleeding:
        playerAction.action = 2;
        break;

      case Utils.GlobalSettings.text.battle.status_slowed:
        playerAction.action = 3;
        break;

      case Utils.GlobalSettings.text.battle.status_weakened:
        playerAction.action = 4;
        break;
    }

    if (enemyStatus !== Utils.GlobalSettings.text.battle.status_normal) {
      this.scene.enemyManager.removeStatusEffect();
      this.scene.battleUI.playerPanel.doAction(
        enemyAction,
        () => {
          if (playerStatus !== Utils.GlobalSettings.text.battle.status_normal) {
            this.scene.battleUI.playerPanel.removeStatusEffect();
            this.scene.enemyManager.doAction(
              playerAction,
              () => {
                this.advanceTimeline(500);
              },
              true
            );
          } else {
            this.advanceTimeline(500);
          }
        },
        true
      );
    } else if (
      playerStatus !== Utils.GlobalSettings.text.battle.status_normal
    ) {
      this.scene.battleUI.playerPanel.removeStatusEffect();
      this.scene.enemyManager.doAction(
        playerAction,
        () => {
          if (enemyStatus !== Utils.GlobalSettings.text.battle.status_normal) {
            this.scene.enemyManager.removeStatusEffect();
            this.scene.battleUI.playerPanel.doAction(
              enemyAction,
              () => {
                this.advanceTimeline(500);
              },
              true
            );
          } else {
            this.advanceTimeline(500);
          }
        },
        true
      );
    } else {
      this.advanceTimeline(500);
    }
  }

  executeTimelineActions() {
    var firstAction = undefined;
    var enemySpeed = this.scene.enemyManager.currentEnemy.speed;
    var playerSpeed = GameCalculations.getSpeed(this.scene.currentPlayer);
    var enemyFirstToAct = enemySpeed > playerSpeed;

    var enemyAction = this.enemyActions[this.timelineIndex];
    var playerAction = this.playerActions[this.timelineIndex];

    var timelineDelay = 500;

    if (playerAction && playerAction.action) {
      if (playerAction.action.type === 'defense') {
        this.scene.battleUI.playerPanel.doAction(playerAction.action, () => {
          if (enemyAction && enemyAction.action) {
            this.scene.enemyManager.doAction(enemyAction.action, () => {
              this.advanceTimeline(timelineDelay);
            });
          } else {
            this.advanceTimeline(timelineDelay);
          }
        });
      } else if (!enemyFirstToAct) {
        this.scene.battleUI.playerPanel.doAction(playerAction.action, () => {
          if (enemyAction && enemyAction.action) {
            this.scene.enemyManager.doAction(enemyAction.action, () => {
              this.advanceTimeline(timelineDelay);
            });
          } else {
            this.advanceTimeline(timelineDelay);
          }
        });
      } else if (enemyAction && enemyAction.action) {
        this.scene.enemyManager.doAction(enemyAction.action, () => {
          if (playerAction && playerAction.action) {
            this.scene.battleUI.playerPanel.doAction(
              playerAction.action,
              () => {
                this.advanceTimeline(timelineDelay);
              }
            );
          } else {
            this.advanceTimeline(timelineDelay);
          }
        });
      } else {
        this.scene.battleUI.playerPanel.doAction(playerAction.action, () => {
          if (enemyAction && enemyAction.action) {
            this.scene.enemyManager.doAction(enemyAction.action, () => {
              this.advanceTimeline(timelineDelay);
            });
          } else {
            this.advanceTimeline(timelineDelay);
          }
        });
      }
    } else if (enemyAction && enemyAction.action) {
      if (enemyAction.action.type === 'defense') {
        this.scene.enemyManager.doAction(enemyAction.action, () => {
          if (playerAction && playerAction.action) {
            this.scene.battleUI.playerPanel.doAction(
              playerAction.action,
              () => {
                this.advanceTimeline(timelineDelay);
              }
            );
          } else {
            this.advanceTimeline(timelineDelay);
          }
        });
      } else if (enemyFirstToAct) {
        this.scene.enemyManager.doAction(enemyAction.action, () => {
          if (playerAction && playerAction.action) {
            this.scene.battleUI.playerPanel.doAction(
              playerAction.action,
              () => {
                this.advanceTimeline(timelineDelay);
              }
            );
          } else {
            this.advanceTimeline(timelineDelay);
          }
        });
      } else if (playerAction && playerAction.action) {
        this.scene.battleUI.playerPanel.doAction(playerAction.action, () => {
          if (enemyAction && enemyAction.action) {
            this.scene.enemyManager.doAction(enemyAction.action, () => {
              this.advanceTimeline(timelineDelay);
            });
          } else {
            this.advanceTimeline(timelineDelay);
          }
        });
      } else {
        this.scene.enemyManager.doAction(enemyAction.action, () => {
          if (playerAction && playerAction.action) {
            this.scene.battleUI.playerPanel.doAction(
              playerAction.action,
              () => {
                this.advanceTimeline(timelineDelay);
              }
            );
          } else {
            this.advanceTimeline(timelineDelay);
          }
        });
      }
    }
  }

  addToPlayerTimeline(x, y, action) {
    if (this.playerActionCount < 8) {
      var node = this.playerActions[this.playerActionCount];
      node.action = action;
      node.setScale(0.7);
      node.visible = true;
      node.alpha = 1;
      var nodeX = node.x;
      var nodeY = node.y;
      node.x = x;
      node.y = y;
      node.setFrame(action.frame);

      this.totalCardsUsed += action.num_cards;

      this.scene.tweens.add({
        targets: [node],
        scale: { from: 0.7, to: 0.35 },
        x: nodeX,
        y: nodeY,
        ease: 'Sine.easeOut',
        duration: 500
      });

      this.playerActionCount++;
    }
  }

  destroy() {
    if (this.enemyTimelineIcon) {
      this.enemyTimelineIcon.destroy();
      this.enemyTimelineIcon = null;
    }

    if (this.enemyTimeline) {
      this.enemyTimeline.destroy();
      this.enemyTimeline = null;
    }

    if (this.playerTimelineIcon) {
      this.playerTimelineIcon.destroy();
      this.playerTimelineIcon = null;
    }

    if (this.playerTimeline) {
      this.playerTimeline.destroy();
      this.playerTimeline = null;
    }

    for (var i = 0; i < this.playerActions.length; i++) {
      if (this.playerActions[i]) {
        this.playerActions[i].destroy();
        this.playerActions[i] = null;
      }

      if (this.enemyActions[i]) {
        this.enemyActions[i].destroy();
        this.enemyActions[i] = null;
      }
    }

    this.playerActions = null;
    this.enemyActions = null;

    if (this.timelineArrow) {
      this.timelineArrow.destroy();
      this.timelineArrow = null;
    }

    console.log('kill timeline panel');
  }
}
