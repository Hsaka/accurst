import Utils from 'utils/utils';
import GameCalculations from 'utils/gameCalculations';
import ItemHandler from 'utils/itemHandler';
import SkillHandler from 'utils/skillHandler';

export default class ActionManager {
  constructor(scene) {
    this.scene = scene;
  }

  applyStatusEffect(status, duration, player) {
    player.status = status;
  }

  reduceSpeed(amount, player) {
    var newSpeed = player.speed - amount;
    if (newSpeed < 0) {
      newSpeed = 0;
    }
    player.speed = newSpeed;
  }

  reducePower(amount, player) {
    var newPow = player.power - amount;
    if (newPow < 0) {
      newPow = 0;
    }
    player.power = newPow;
  }

  reduceMagic(amount, player) {
    var newMag = player.magic - amount;
    if (newMag < 0) {
      newMag = 0;
    }
    player.magic = newMag;
  }

  reduceHealth(amount, player) {
    var newHP = player.health - amount;
    if (newHP <= 0) {
      newHP = 0;
    }
    player.health = newHP;
  }

  increaseHealth(amount, player) {
    var newHP = player.health + amount;
    var maxHP = player.maxHealth;
    if (newHP > maxHP) {
      newHP = maxHP;
    }
    player.health = newHP;
  }

  getActionResult(
    action,
    instigator,
    target,
    apply,
    instigatorManager,
    targetManager
  ) {
    var result = '';
    var damage = 0;
    if (action.type === 'skill') {
      var skillOutput = SkillHandler.getSkillAction(
        action.action,
        apply,
        targetManager,
        target,
        instigatorManager,
        instigator
      );
      damage = skillOutput.damage;
      result = skillOutput.result;
    } else if (action.type === 'defense') {
      var defense = Utils.GlobalSettings.defenseList[action.action];
      result = 'Defense +' + defense.added_defense;
      if (apply) {
        target.defenseBonus += defense.added_defense;
      }
    } else if (action.type === 'item') {
      var itemOutput = ItemHandler.getItemAction(
        action.action,
        apply,
        targetManager,
        target
      );
      damage = itemOutput.damage;
      result = itemOutput.result;
    }

    return result;
  }

  getActionConfig(action, target) {
    var config = {
      x: target.x,
      y: target.y,
    };

    if (action.type === 'skill') {
      switch (action.action) {
        case 0:
          config.x = {
            ease: 'Sine.easeIn',
            min: target.x - 50,
            max: target.x + 50,
          };
          config.y = {
            ease: 'Sine.easeIn',
            min: target.y - 50,
            max: target.y + 50,
          };
          config.moveToX = [target.x - 50, target.x + 50];
          config.moveToY = [target.y - 50, target.y + 50];
          break;

        case 1:
          config.x = {
            ease: 'Linear',
            min: target.x - 60,
            max: target.x + 60,
          };
          config.y = target.y + 20;
          config.moveToX = target.x;
          config.moveToY = target.y - 180;
          break;

        case 3:
          config.x = {
            ease: 'Linear',
            min: target.x - 60,
            max: target.x + 60,
          };
          config.y = target.y - 120;
          config.moveToX = target.x;
          config.moveToY = target.y + 120;
          break;

        case 4:
          config.x = {
            ease: 'Sine.easeIn',
            min: target.x - 50,
            max: target.x + 50,
          };
          config.y = {
            ease: 'Sine.easeIn',
            min: target.y - 50,
            max: target.y + 50,
          };
          config.moveToX = [target.x - 50, target.x + 50];
          config.moveToY = [target.y - 50, target.y + 50];
          break;

        case 5:
          config.x = target.x + 100;
          config.y = target.y - 100;
          config.moveToX = [target.x - 100, target.x - 110];
          config.moveToY = [target.y + 100, target.y + 110];
          break;
      }
    } else if (action.type === 'defense') {
      switch (action.action) {
        default:
          config.x = target.x;
          config.y = target.y;
          config.moveToX = target.x;
          config.moveToY = target.y;
          break;
      }
    } else if (action.type === 'item') {
      switch (action.action) {
        case 0:
          break;
      }
    }

    return config;
  }

  handleAction(action, isPlayer, onComplete, isStatusEffectAction = false) {
    var actionToTrigger = undefined;

    switch (action.type) {
      case 'skill':
        actionToTrigger = Utils.GlobalSettings.skillsList[action.action];
        break;

      case 'defense':
        actionToTrigger = Utils.GlobalSettings.defenseList[action.action];
        break;

      case 'item':
        actionToTrigger = Utils.GlobalSettings.itemsList[action.action];
        break;
    }

    if (actionToTrigger) {
      var config = undefined;
      var actionResult = '';
      var targetIcon = undefined;
      var targetCharacter = undefined;
      var instigatingCharacter = undefined;
      var targetManager = undefined;
      var instigatingManager = undefined;
      var currentStatus = undefined;

      if (isPlayer) {
        currentStatus = GameCalculations.getStatus(this.scene.currentPlayer);
        if (actionToTrigger.use_on_any) {
          if (action.itemTarget === -1) {
            //target is enemy
            targetIcon = this.scene.enemyManager.enemyIcon;
            targetCharacter = this.scene.enemyManager.currentEnemy;
            instigatingCharacter = this.scene.currentPlayer;
            targetManager = this.scene.enemyManager;
            instigatingManager = this.scene.battleUI.playerPanel;
          } else if (
            action.itemTarget ===
            Utils.GlobalSettings.gameState.encounter.combatTurn
          ) {
            //target is current player
            targetIcon = this.scene.battleUI.playerPanel.playerIcon;
            targetCharacter = this.scene.currentPlayer;
            instigatingCharacter = this.scene.currentPlayer;
            targetManager = this.scene.battleUI.playerPanel;
            instigatingManager = this.scene.battleUI.playerPanel;
          } else {
            //target is other player
            targetIcon = this.scene.effectManager.actionTargetIcon;
            targetCharacter =
              Utils.GlobalSettings.gameState.players[action.itemTarget];
            instigatingCharacter = this.scene.currentPlayer;
            targetManager = this;
            instigatingManager = this.scene.battleUI.playerPanel;

            this.scene.effectManager.showTargetIconImage(
              Utils.GlobalSettings.heroList[
                Utils.GlobalSettings.gameState.players[action.itemTarget]
                  .classType
              ].image
            );
          }
        } else {
          if (actionToTrigger.target === 'self') {
            targetIcon = this.scene.battleUI.playerPanel.playerIcon;
            targetCharacter = this.scene.currentPlayer;
            instigatingCharacter = this.scene.currentPlayer;
            targetManager = this.scene.battleUI.playerPanel;
            instigatingManager = this.scene.battleUI.playerPanel;
          } else {
            targetIcon = this.scene.enemyManager.enemyIcon;
            targetCharacter = this.scene.enemyManager.currentEnemy;
            instigatingCharacter = this.scene.currentPlayer;
            targetManager = this.scene.enemyManager;
            instigatingManager = this.scene.battleUI.playerPanel;
          }
        }
      } else {
        currentStatus = this.scene.enemyManager.currentEnemy.status;
        if (actionToTrigger.target === 'self') {
          targetIcon = this.scene.enemyManager.enemyIcon;
          targetCharacter = this.scene.enemyManager.currentEnemy;
          instigatingCharacter = this.scene.enemyManager.currentEnemy;
          targetManager = this.scene.enemyManager;
          instigatingManager = this.scene.enemyManager;
        } else {
          targetIcon = this.scene.battleUI.playerPanel.playerIcon;
          targetCharacter = this.scene.currentPlayer;
          instigatingCharacter = this.scene.enemyManager.currentEnemy;
          targetManager = this.scene.battleUI.playerPanel;
          instigatingManager = this.scene.enemyManager;
        }
      }

      if (
        (!isStatusEffectAction &&
          currentStatus !== Utils.GlobalSettings.text.battle.status_stunned) ||
        isStatusEffectAction
      ) {
        config = this.getActionConfig(action, targetIcon);
        actionResult = this.getActionResult(
          action,
          instigatingCharacter,
          targetCharacter
        );

        if (action.type === 'item') {
          this.scene.effectManager.showEffectImage(
            actionToTrigger.image,
            config,
            1000,
            0,
            () => {
              this.getActionResult(
                action,
                instigatingCharacter,
                targetCharacter,
                true,
                instigatingManager,
                targetManager
              );

              if (onComplete && typeof onComplete === 'function') {
                this.scene.time.delayedCall(
                  500,
                  () => {
                    onComplete();
                  },
                  [],
                  this
                );
              }
            }
          );
        } else {
          this.scene.effectManager.doEffect(
            actionToTrigger.effect,
            config,
            1000,
            () => {
              this.getActionResult(
                action,
                instigatingCharacter,
                targetCharacter,
                true,
                instigatingManager,
                targetManager
              );

              if (onComplete && typeof onComplete === 'function') {
                this.scene.time.delayedCall(
                  500,
                  () => {
                    onComplete();
                  },
                  [],
                  this
                );
              }
            }
          );
        }

        this.scene.effectManager.showEffectText(
          targetIcon.x,
          targetIcon.y,
          actionToTrigger.name +
            (actionResult.length > 0 ? '\n' + actionResult : '')
        );
      } else {
        if (onComplete && typeof onComplete === 'function') {
          this.scene.effectManager.showEffectText(
            isPlayer
              ? this.scene.battleUI.playerPanel.playerIcon.x
              : this.scene.enemyManager.enemyIcon.x,
            isPlayer
              ? this.scene.battleUI.playerPanel.playerIcon.y
              : this.scene.enemyManager.enemyIcon.y,
            currentStatus
          );

          this.scene.time.delayedCall(
            500,
            () => {
              onComplete();
            },
            [],
            this
          );
        }
      }
    }
  }

  destroy() {
    console.log('kill action manager');
  }
}
