import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import GameCalculations from 'utils/gameCalculations';

export default class DeathPanel {
  constructor(scene) {
    this.scene = scene;
  }

  setupDeathPanel() {
    this.maxDeathSaves = 2;
    this.atTurnStart = false;

    this.deathPanelBg = UIHelper.createDialog(
      this.scene,
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      500,
      480,
      'dialog9patch2',
      15
    );
    this.deathPanelBg.alpha = 0;
    this.deathPanelBg.visible = false;

    this.deathPanelIcon = this.scene.add.image(
      this.deathPanelBg.x - this.deathPanelBg.width / 2 + 20,
      this.deathPanelBg.y - this.deathPanelBg.height / 2 + 20,
      'atlas1',
      'hero_knight'
    );
    this.deathPanelIcon.setScale(0.5);
    this.deathPanelIcon.alpha = 0;
    this.deathPanelIcon.setTint(0xff0000);
    this.deathPanelIcon.visible = false;

    this.deathPanelTitle = this.scene.add.bitmapText(
      this.deathPanelBg.x,
      this.deathPanelBg.y - this.deathPanelBg.height / 2 + 40,
      'sakkalbold',
      '',
      60
    );
    this.deathPanelTitle.setOrigin(0.5);
    this.deathPanelTitle.alpha = 0;
    this.deathPanelTitle.visible = false;

    this.deathPanelInfo = this.scene.add.bitmapText(
      this.deathPanelTitle.x,
      this.deathPanelTitle.y + 40,
      'sakkalicon',
      '',
      40
    );
    this.deathPanelInfo.alpha = 0;
    this.deathPanelInfo.setOrigin(0.5, 0);
    this.deathPanelInfo.setMaxWidth(this.deathPanelBg.width - 50);
    this.deathPanelInfo.setCenterAlign();
    this.deathPanelInfo.visible = false;

    this.deathPanelHeartButton = UIHelper.createMenuButton(
      this.scene,
      'menubutton',
      this.deathPanelBg.y + this.deathPanelBg.height / 2 + 20,
      Utils.GlobalSettings.text.battle.is_a_heart,
      false
    );
    this.deathPanelHeartButton.visible = false;
    this.deathPanelHeartButton.on('pointerup', () => {
      this.hideDeathPanel();

      this.scene.cameras.main.flash(1000, 255, 255, 255);
      var quarterHP = Math.ceil(
        GameCalculations.getMaxHealth(this.scene.currentPlayer) / 4
      );

      //TODO: reset player death save count?

      if (this.atTurnStart) {
        this.scene.battleUI.playerPanel.increaseHealth(quarterHP);
      } else {
        var newHP = this.scene.currentPlayer.health + quarterHP;
        var maxHP = GameCalculations.getMaxHealth(this.scene.currentPlayer);
        if (newHP > maxHP) {
          newHP = maxHP;
        }
        this.scene.currentPlayer.health = newHP;
        this.goToNextTurn(1000);
      }
    });

    this.deathPanelNotHeartButton = UIHelper.createMenuButton(
      this.scene,
      'menubutton',
      this.deathPanelHeartButton.y + 100,
      Utils.GlobalSettings.text.battle.is_not_a_heart,
      false
    );
    this.deathPanelNotHeartButton.visible = false;
    this.deathPanelNotHeartButton.on('pointerup', () => {
      this.hideDeathPanel();

      this.scene.cameras.main.shake(1000, 0.03);
      this.scene.cameras.main.flash(250, 255, 0, 0);

      this.scene.currentPlayer.deathSaveCount++;

      if (this.scene.currentPlayer.deathSaveCount >= this.maxDeathSaves) {
        //TODO: game over scene
        this.scene.time.delayedCall(
          1000,
          () => {
            this.scene.scene.start('main', { from: 'battle' });
          },
          [],
          this
        );
      } else {
        //go to next turn
        this.goToNextTurn();
      }
    });
  }

  goToNextTurn(delay = 500) {
    Utils.GlobalSettings.gameState.encounter.combatTurn++;
    if (
      Utils.GlobalSettings.gameState.encounter.combatTurn >=
      Utils.GlobalSettings.gameState.numPlayers
    ) {
      Utils.GlobalSettings.gameState.encounter.combatTurn = 0;
    }

    Utils.GlobalSettings.gameState.encounter.combatRound++;
    Utils.saveGameState();

    this.scene.time.delayedCall(
      delay,
      () => {
        this.scene.scene.start('battle', { from: 'battle' });
      },
      [],
      this
    );
  }

  toggleDeathPanelVisibility(visible) {
    this.deathPanelBg.visible = visible;
    this.deathPanelIcon.visible = visible;
    this.deathPanelTitle.visible = visible;
    this.deathPanelInfo.visible = visible;
    this.deathPanelHeartButton.visible = visible;
    this.deathPanelHeartButton.buttonTxt.visible = visible;
    this.deathPanelNotHeartButton.visible = visible;
    this.deathPanelNotHeartButton.buttonTxt.visible = visible;
  }

  toggleDeathPanelAlpha(alpha) {
    this.deathPanelBg.alpha = alpha;
    this.deathPanelIcon.alpha = alpha;
    this.deathPanelTitle.alpha = alpha;
    this.deathPanelInfo.alpha = alpha;
    this.deathPanelHeartButton.alpha = alpha;
    this.deathPanelHeartButton.buttonTxt.alpha = alpha;
    this.deathPanelNotHeartButton.alpha = alpha;
    this.deathPanelNotHeartButton.buttonTxt.alpha = alpha;
  }

  hideDeathPanel() {
    this.scene.bgOverlay.alpha = 0.8;
    this.scene.bgOverlay.visible = true;

    this.toggleDeathPanelAlpha(1);
    this.toggleDeathPanelVisibility(true);

    this.scene.tweens.add({
      targets: [this.scene.bgOverlay],
      alpha: { from: 0.8, to: 0 },
      ease: 'Linear.easeOut',
      duration: 500,
      onComplete: () => {
        this.scene.bgOverlay.visible = false;
      }
    });

    this.scene.tweens.add({
      targets: [
        this.deathPanelBg,
        this.deathPanelIcon,
        this.deathPanelTitle,
        this.deathPanelInfo,
        this.deathPanelHeartButton,
        this.deathPanelHeartButton.buttonTxt,
        this.deathPanelNotHeartButton,
        this.deathPanelNotHeartButton.buttonTxt
      ],
      alpha: { from: 1, to: 0 },
      ease: 'Linear.easeOut',
      duration: 500,
      onComplete: () => {
        this.toggleDeathPanelVisibility(false);
      }
    });
  }

  showDeathPanel(delay = 0, atTurnStart = false) {
    this.atTurnStart = atTurnStart;
    this.toggleDeathPanelAlpha(0);
    this.toggleDeathPanelVisibility(true);

    this.deathPanelTitle.setText(
      this.scene.currentPlayer.name +
        '  ' +
        Utils.GlobalSettings.text.battle.downed
    );

    this.deathPanelIcon.setFrame(
      this.scene.battleUI.playerPanel.playerIcon.frame.name
    );

    var savesLeft =
      this.maxDeathSaves - this.scene.currentPlayer.deathSaveCount;

    var deathInfo = `${
      this.scene.currentPlayer.name
    } is at Death's door!\n\nTo recover, ${
      this.scene.currentPlayer.name
    } has ${savesLeft} ${
      savesLeft === 1 ? 'chance' : 'chances'
    } to pull a Heart (®,© or Wild) from the Combat Deck or another player can heal them.\n\nShuffle all 8 cards in the Combat Deck and draw 1 card at random.`;

    this.deathPanelInfo.setText(deathInfo);

    this.scene.bgOverlay.alpha = 0;
    this.scene.bgOverlay.visible = true;

    this.scene.tweens.add({
      targets: [this.scene.bgOverlay],
      alpha: { from: 0, to: 0.8 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: delay
    });

    this.scene.tweens.add({
      targets: [
        this.deathPanelBg,
        this.deathPanelIcon,
        this.deathPanelTitle,
        this.deathPanelInfo,
        this.deathPanelHeartButton,
        this.deathPanelHeartButton.buttonTxt,
        this.deathPanelNotHeartButton,
        this.deathPanelNotHeartButton.buttonTxt
      ],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: delay
    });
  }

  destroy() {
    if (this.deathPanelBg) {
      this.deathPanelBg.destroy();
      this.deathPanelBg = null;
    }

    if (this.deathPanelIcon) {
      this.deathPanelIcon.destroy();
      this.deathPanelIcon = null;
    }

    if (this.deathPanelTitle) {
      this.deathPanelTitle.destroy();
      this.deathPanelTitle = null;
    }

    if (this.deathPanelInfo) {
      this.deathPanelInfo.destroy();
      this.deathPanelInfo = null;
    }

    if (this.deathPanelHeartButton && this.deathPanelHeartButton.buttonTxt) {
      this.deathPanelHeartButton.buttonTxt.destroy();
      this.deathPanelHeartButton.buttonTxt = null;
      this.deathPanelHeartButton.destroy();
      this.deathPanelHeartButton = null;
    }

    if (
      this.deathPanelNotHeartButton &&
      this.deathPanelNotHeartButton.buttonTxt
    ) {
      this.deathPanelNotHeartButton.buttonTxt.destroy();
      this.deathPanelNotHeartButton.buttonTxt = null;
      this.deathPanelNotHeartButton.destroy();
      this.deathPanelNotHeartButton = null;
    }

    console.log('kill death panel');
  }
}
