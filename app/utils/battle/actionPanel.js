import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';

export default class ActionPanel {
  constructor(scene) {
    this.scene = scene;
  }

  setupActionConfirmation() {
    this.actionConfirmationBg = UIHelper.createDialog(
      this.scene,
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      500,
      400,
      'dialog9patch2',
      15
    );
    this.actionConfirmationBg.alpha = 0;
    this.actionConfirmationBg.visible = false;

    this.actionConfirmationIcon = this.scene.add.image(
      this.actionConfirmationBg.x - this.actionConfirmationBg.width / 2 + 20,
      this.actionConfirmationBg.y - this.actionConfirmationBg.height / 2 + 20,
      'atlas1',
      'rune_attack'
    );
    this.actionConfirmationIcon.setScale(0.7);
    this.actionConfirmationIcon.alpha = 0;
    this.actionConfirmationIcon.visible = false;

    this.actionConfirmationTitle = this.scene.add.bitmapText(
      this.actionConfirmationBg.x,
      this.actionConfirmationBg.y - this.actionConfirmationBg.height / 2 + 40,
      'sakkalbold',
      '',
      60
    );
    this.actionConfirmationTitle.setOrigin(0.5);
    this.actionConfirmationTitle.alpha = 0;
    this.actionConfirmationTitle.visible = false;

    this.actionConfirmationCost = this.scene.add.bitmapText(
      this.actionConfirmationTitle.x,
      this.actionConfirmationTitle.y + 35,
      'sakkalbold',
      Utils.GlobalSettings.text.battle.magic_cost,
      30
    );
    this.actionConfirmationCost.setOrigin(0.5);
    this.actionConfirmationCost.alpha = 0;
    this.actionConfirmationCost.visible = false;

    this.actionConfirmationHandReqd = this.scene.add.bitmapText(
      this.actionConfirmationBg.x - this.actionConfirmationBg.width / 2 + 20,
      this.actionConfirmationCost.y + 35,
      'sakkalbold',
      Utils.GlobalSettings.text.battle.cards_required,
      35
    );
    this.actionConfirmationHandReqd.alpha = 0;
    this.actionConfirmationHandReqd.visible = false;

    this.actionConfirmationRequirements = this.scene.add
      .bitmapText(
        this.actionConfirmationHandReqd.x,
        this.actionConfirmationHandReqd.y + 35,
        'sakkalicon',
        '',
        44
      )
      .setMaxWidth(450);
    this.actionConfirmationRequirements.alpha = 0;
    this.actionConfirmationRequirements.visible = false;

    this.actionConfirmationDescription = this.scene.add
      .bitmapText(
        this.actionConfirmationHandReqd.x,
        this.actionConfirmationHandReqd.y + 160,
        'sakkalicon',
        '',
        35
      )
      .setMaxWidth(450);
    this.actionConfirmationDescription.alpha = 0;
    this.actionConfirmationDescription.visible = false;

    this.actionConfirmationActivateButton = UIHelper.createMenuButton(
      this.scene,
      'menubutton',
      this.actionConfirmationBg.y + this.actionConfirmationBg.height / 2 + 20,
      Utils.GlobalSettings.text.battle.activate,
      false
    );
    this.actionConfirmationActivateButton.visible = false;
    this.actionConfirmationActivateButton.on('pointerup', () => {
      this.activateClicked();
    });

    this.actionConfirmationCancelButton = UIHelper.createMenuButton(
      this.scene,
      'menubutton',
      this.actionConfirmationActivateButton.y +
        this.actionConfirmationActivateButton.height / 2 +
        50,
      Utils.GlobalSettings.text.battle.cancel,
      false
    );
    this.actionConfirmationCancelButton.visible = false;
    this.actionConfirmationCancelButton.on('pointerup', () => {
      this.hideActionConfirmation();
    });

    this.targetIcons = [];
    this.targetNames = [];
    var spr;
    var name;
    for (var i = 0; i < Utils.GlobalSettings.gameState.numPlayers; i++) {
      spr = this.scene.add.image(
        this.actionConfirmationBg.x +
          this.actionConfirmationBg.width / 2 -
          50 -
          i * 100,
        this.actionConfirmationActivateButton.y - 25,
        'atlas1',
        Utils.GlobalSettings.heroList[
          Utils.GlobalSettings.gameState.players[i].classType
        ].image
      );
      spr._targetIndex = i;
      spr.visible = false;
      spr.setDisplaySize(80, 80);
      spr.setInteractive();
      spr.on('pointerover', this.onTargetHoverOver.bind(this, spr));
      spr.on('pointerout', this.onTargetHoverOut.bind(this, spr));
      spr.on('pointerup', this.onTargetPress.bind(this, spr));
      this.targetIcons.push(spr);

      name = this.scene.add.bitmapText(
        spr.x,
        spr.y + 50,
        'sakkalbold',
        Utils.GlobalSettings.gameState.players[i].name,
        27
      );
      name.visible = false;
      name.setOrigin(0.5);
      this.targetNames.push(name);
    }

    spr = this.scene.add.image(
      this.actionConfirmationBg.x +
        this.actionConfirmationBg.width / 2 -
        50 -
        Utils.GlobalSettings.gameState.players.length * 100,
      this.actionConfirmationActivateButton.y - 25,
      'enemyAtlas1',
      this.scene.enemyManager.enemyIcon.frame.name
    );
    spr._targetIndex = -1;
    spr.visible = false;
    spr.setDisplaySize(80, 80);
    spr.setInteractive();
    spr.on('pointerover', this.onTargetHoverOver.bind(this, spr));
    spr.on('pointerout', this.onTargetHoverOut.bind(this, spr));
    spr.on('pointerup', this.onTargetPress.bind(this, spr));
    this.targetIcons.push(spr);

    name = this.scene.add.bitmapText(
      spr.x,
      spr.y + 50,
      'sakkalbold',
      this.scene.enemyManager.currentEnemy.name,
      27
    );
    name.visible = false;
    name.setOrigin(0.5);
    this.targetNames.push(name);
  }

  onTargetHoverOver(button, event) {
    if (button) {
      button.setTint(0x00ff00);
    }
  }

  onTargetHoverOut(button, event) {
    if (button) {
      button.setTint(0xffffff);
    }
  }

  onTargetPress(button, event) {
    if (button) {
      this.selectedAction.itemTarget = button._targetIndex;
      this.addActionToTimeline();

      if (this.scene.currentPlayer.items.length > 0) {
        this.scene.currentPlayer.items.splice(this.actionIndex, 1);

        this.scene.battleUI.skillPanel.showItemPanel();
      }

      this.hideActionConfirmation();
    }
  }

  addActionToTimeline() {
    this.scene.battleUI.timelinePanel.addToPlayerTimeline(
      this.actionConfirmationIcon.x,
      this.actionConfirmationIcon.y,
      this.selectedAction
    );

    this.scene.battleUI.showInstructions(
      Utils.GlobalSettings.text.battle.instructions_combo,
      1000
    );
  }

  activateClicked() {
    switch (this.scene.battleUI.currentMenuSelection) {
      case Utils.GlobalSettings.text.battle.skills:
        var skill = Utils.GlobalSettings.skillsList[this.selectedAction.action];

        if (skill.use_on_any) {
          this.scene.tweens.add({
            targets: [
              this.actionConfirmationActivateButton,
              this.actionConfirmationActivateButton.buttonTxt
            ],
            alpha: { from: 1, to: 0 },
            ease: 'Linear.easeOut',
            duration: 100,
            onComplete: () => {
              this.actionConfirmationActivateButton.visible = false;
              this.actionConfirmationActivateButton.buttonTxt.visible = false;

              this.actionConfirmationDescription.setText(
                Utils.GlobalSettings.text.battle.choose_skill_target
              );
              this.showTargets();
            }
          });
        } else {
          if (skill && skill.magic_cost > 0) {
            this.scene.battleUI.playerPanel.reduceMagic(skill.magic_cost);
          }

          this.addActionToTimeline();
          this.hideActionConfirmation();
        }

        break;

      case Utils.GlobalSettings.text.battle.defense:
        this.addActionToTimeline();
        this.hideActionConfirmation();

        break;

      case Utils.GlobalSettings.text.battle.items:
        var item = Utils.GlobalSettings.itemsList[this.selectedAction.action];
        if (item.use_on_any) {
          this.scene.tweens.add({
            targets: [
              this.actionConfirmationActivateButton,
              this.actionConfirmationActivateButton.buttonTxt
            ],
            alpha: { from: 1, to: 0 },
            ease: 'Linear.easeOut',
            duration: 100,
            onComplete: () => {
              this.actionConfirmationActivateButton.visible = false;
              this.actionConfirmationActivateButton.buttonTxt.visible = false;

              this.actionConfirmationDescription.setText(
                Utils.GlobalSettings.text.battle.choose_item_target
              );
              this.showTargets();
            }
          });
        } else {
          this.addActionToTimeline();
          if (this.scene.currentPlayer.items.length > 0) {
            this.scene.currentPlayer.items.splice(this.actionIndex, 1);

            this.scene.battleUI.skillPanel.showItemPanel();
          }
          this.hideActionConfirmation();
        }

        break;
    }
  }

  toggleTargetVisibility(visible) {
    for (var i = 0; i < this.targetIcons.length; i++) {
      this.targetIcons[i].visible = visible;
      this.targetNames[i].visible = visible;
    }
  }

  toggleTargetAlpha(alpha) {
    for (var i = 0; i < this.targetIcons.length; i++) {
      this.targetIcons[i].alpha = alpha;
      this.targetNames[i].alpha = alpha;
    }
  }

  showTargets(delay = 0) {
    this.toggleTargetAlpha(0);
    this.toggleTargetVisibility(true);

    this.scene.tweens.add({
      targets: [...this.targetIcons, ...this.targetNames],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: delay
    });
  }

  toggleActionConfirmationVisibility(visible) {
    this.actionConfirmationBg.visible = visible;
    this.actionConfirmationIcon.visible = visible;
    this.actionConfirmationTitle.visible = visible;
    this.actionConfirmationCost.visible = visible;
    this.actionConfirmationHandReqd.visible = visible;
    this.actionConfirmationRequirements.visible = visible;
    this.actionConfirmationDescription.visible = visible;
    this.actionConfirmationActivateButton.visible = visible;
    this.actionConfirmationActivateButton.buttonTxt.visible = visible;
    this.actionConfirmationCancelButton.visible = visible;
    this.actionConfirmationCancelButton.buttonTxt.visible = visible;
  }

  toggleActionConfirmationAlpha(alpha) {
    this.actionConfirmationBg.alpha = alpha;
    this.actionConfirmationIcon.alpha = alpha;
    this.actionConfirmationTitle.alpha = alpha;
    this.actionConfirmationCost.alpha = alpha;
    this.actionConfirmationHandReqd.alpha = alpha;
    this.actionConfirmationRequirements.alpha = alpha;
    this.actionConfirmationDescription.alpha = alpha;
    this.actionConfirmationActivateButton.alpha = alpha;
    this.actionConfirmationActivateButton.buttonTxt.alpha = alpha;
    this.actionConfirmationCancelButton.alpha = alpha;
    this.actionConfirmationCancelButton.buttonTxt.alpha = alpha;
  }

  hideActionConfirmation() {
    this.scene.bgOverlay.alpha = 0.8;
    this.scene.bgOverlay.visible = true;

    this.toggleActionConfirmationAlpha(1);
    this.toggleActionConfirmationVisibility(true);

    this.scene.tweens.add({
      targets: [this.scene.bgOverlay],
      alpha: { from: 0.8, to: 0 },
      ease: 'Linear.easeOut',
      duration: 100,
      onComplete: () => {
        this.scene.bgOverlay.visible = false;
      }
    });

    if (this.targetIcons[0].visible) {
      this.scene.tweens.add({
        targets: [...this.targetIcons, ...this.targetNames],
        alpha: { from: 1, to: 0 },
        ease: 'Linear.easeOut',
        duration: 100,
        onComplete: () => {
          this.toggleTargetAlpha(0);
          this.toggleTargetVisibility(false);
        }
      });
    }

    this.scene.tweens.add({
      targets: [
        this.actionConfirmationBg,
        this.actionConfirmationIcon,
        this.actionConfirmationTitle,
        this.actionConfirmationCost,
        this.actionConfirmationHandReqd,
        this.actionConfirmationRequirements,
        this.actionConfirmationDescription,
        this.actionConfirmationActivateButton,
        this.actionConfirmationActivateButton.buttonTxt,
        this.actionConfirmationCancelButton,
        this.actionConfirmationCancelButton.buttonTxt
      ],
      alpha: { from: 1, to: 0 },
      ease: 'Linear.easeOut',
      duration: 100,
      onComplete: () => {
        this.toggleActionConfirmationVisibility(false);
      }
    });
  }

  showActionConfirmation(number, button) {
    var isValid = true;

    this.actionIndex = number;
    this.selectedAction = undefined;
    var selectedActionIndex = 0;
    var selectedActionType = '';
    var selectedActionNumCards = 0;

    this.toggleActionConfirmationAlpha(0);
    this.toggleActionConfirmationVisibility(true);

    switch (this.scene.battleUI.currentMenuSelection) {
      case Utils.GlobalSettings.text.battle.skills:
        var skill =
          Utils.GlobalSettings.skillsList[
            this.scene.currentPlayer.skills[number]
          ];

        if (
          this.scene.battleUI.timelinePanel.totalCardsUsed + skill.num_cards >
          8
        ) {
          isValid = false;
          UIHelper.showToast(
            this.scene,
            this.scene.toast,
            button.x,
            button.y + 10,
            Utils.GlobalSettings.text.battle.not_enough_cards
          );
        }
        if (
          skill.magic_cost > 0 &&
          this.scene.currentPlayer.magic < skill.magic_cost
        ) {
          isValid = false;
          UIHelper.showToast(
            this.scene,
            this.scene.toast,
            button.x,
            button.y + 10,
            Utils.GlobalSettings.text.battle.not_enough_magic
          );
        } else {
          this.actionConfirmationIcon.setFrame('rune_attack');
          selectedActionIndex = this.scene.currentPlayer.skills[number];
          selectedActionType = 'skill';
          selectedActionNumCards = skill.num_cards;

          this.actionConfirmationTitle.setText(skill.name);
          this.actionConfirmationCost.setText(
            Utils.GlobalSettings.text.battle.magic_cost +
              ': ' +
              skill.magic_cost
          );
          if (skill.magic_cost === 0) {
            this.actionConfirmationCost.visible = false;
          }
          this.actionConfirmationRequirements.setText(
            skill.long_requirements + ' (' + skill.example + ')'
          );
          this.actionConfirmationDescription.setText(skill.description);
        }

        break;

      case Utils.GlobalSettings.text.battle.defense:
        var defense = Utils.GlobalSettings.defenseList[number];

        if (
          this.scene.battleUI.timelinePanel.totalCardsUsed + defense.num_cards >
          8
        ) {
          isValid = false;
          UIHelper.showToast(
            this.scene,
            this.scene.toast,
            button.x,
            button.y + 10,
            Utils.GlobalSettings.text.battle.not_enough_cards
          );
        } else {
          this.actionConfirmationIcon.setFrame('rune_defend');
          selectedActionIndex = number;
          selectedActionType = 'defense';
          selectedActionNumCards = defense.num_cards;

          this.actionConfirmationTitle.setText(defense.name);
          this.actionConfirmationCost.visible = false;
          this.actionConfirmationRequirements.setText(
            defense.long_requirements + ' (' + defense.example + ')'
          );
          this.actionConfirmationDescription.setText(defense.description);
        }

        break;

      case Utils.GlobalSettings.text.battle.items:
        var item =
          Utils.GlobalSettings.itemsList[
            this.scene.currentPlayer.items[number]
          ];

        if (
          this.scene.battleUI.timelinePanel.totalCardsUsed + item.num_cards >
          8
        ) {
          isValid = false;
          UIHelper.showToast(
            this.scene,
            this.scene.toast,
            button.x,
            button.y + 10,
            Utils.GlobalSettings.text.battle.not_enough_cards
          );
        } else {
          this.actionConfirmationIcon.setFrame('rune_item');
          selectedActionIndex = this.scene.currentPlayer.items[number];
          selectedActionType = 'item';
          selectedActionNumCards = item.num_cards;

          this.actionConfirmationTitle.setText(item.name);
          this.actionConfirmationCost.visible = false;
          this.actionConfirmationRequirements.setText(
            item.long_requirements + ' (' + item.example + ')'
          );
          this.actionConfirmationDescription.setText(item.description);
        }

        break;
    }

    if (isValid) {
      this.selectedAction = {
        type: selectedActionType,
        frame: this.actionConfirmationIcon.frame.name,
        action: selectedActionIndex,
        num_cards: selectedActionNumCards
      };

      this.scene.bgOverlay.alpha = 0;
      this.scene.bgOverlay.visible = true;

      this.scene.tweens.add({
        targets: [this.scene.bgOverlay],
        alpha: { from: 0, to: 0.8 },
        ease: 'Linear.easeOut',
        duration: 500
      });

      this.scene.tweens.add({
        targets: [
          this.actionConfirmationBg,
          this.actionConfirmationIcon,
          this.actionConfirmationTitle,
          this.actionConfirmationCost,
          this.actionConfirmationHandReqd,
          this.actionConfirmationRequirements,
          this.actionConfirmationDescription,
          this.actionConfirmationActivateButton,
          this.actionConfirmationActivateButton.buttonTxt,
          this.actionConfirmationCancelButton,
          this.actionConfirmationCancelButton.buttonTxt
        ],
        alpha: { from: 0, to: 1 },
        ease: 'Linear.easeOut',
        duration: 500
      });
    } else {
      this.toggleActionConfirmationVisibility(false);
    }
  }

  destroy() {
    if (this.actionConfirmationBg) {
      this.actionConfirmationBg.destroy();
      this.actionConfirmationBg = null;
    }

    if (this.actionConfirmationIcon) {
      this.actionConfirmationIcon.destroy();
      this.actionConfirmationIcon = null;
    }

    if (this.actionConfirmationTitle) {
      this.actionConfirmationTitle.destroy();
      this.actionConfirmationTitle = null;
    }

    if (this.actionConfirmationCost) {
      this.actionConfirmationCost.destroy();
      this.actionConfirmationCost = null;
    }

    if (this.actionConfirmationHandReqd) {
      this.actionConfirmationHandReqd.destroy();
      this.actionConfirmationHandReqd = null;
    }

    if (this.actionConfirmationRequirements) {
      this.actionConfirmationRequirements.destroy();
      this.actionConfirmationRequirements = null;
    }

    if (this.actionConfirmationDescription) {
      this.actionConfirmationDescription.destroy();
      this.actionConfirmationDescription = null;
    }

    if (
      this.actionConfirmationActivateButton &&
      this.actionConfirmationActivateButton.buttonTxt
    ) {
      this.actionConfirmationActivateButton.buttonTxt.destroy();
      this.actionConfirmationActivateButton.buttonTxt = null;
      this.actionConfirmationActivateButton.destroy();
      this.actionConfirmationActivateButton = null;
    }

    if (
      this.actionConfirmationCancelButton &&
      this.actionConfirmationCancelButton.buttonTxt
    ) {
      this.actionConfirmationCancelButton.buttonTxt.destroy();
      this.actionConfirmationCancelButton.buttonTxt = null;
      this.actionConfirmationCancelButton.destroy();
      this.actionConfirmationCancelButton = null;
    }

    if (this.targetIcons && this.targetNames) {
      for (var i = 0; i < this.targetIcons.length; i++) {
        this.targetIcons[i].destroy();
        this.targetIcons[i] = null;
        this.targetNames[i].destroy();
        this.targetNames[i] = null;
      }
      this.targetIcons = null;
      this.targetNames = null;
    }

    console.log('kill action panel');
  }
}
