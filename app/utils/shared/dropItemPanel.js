import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import GameCalculations from 'utils/gameCalculations';

export default class DropItemPanel {
  constructor(scene) {
    this.scene = scene;
  }

  setupDropItemPanel(player) {
    this.player = player;
    this.onCancelPressed = undefined;
    this.onItemPressed = undefined;

    this.bgOverlay = this.scene.add.image(0, 0, 'atlas1', 'overlay');
    this.bgOverlay.setOrigin(0);
    this.bgOverlay.setDisplaySize(
      Utils.GlobalSettings.width,
      Utils.GlobalSettings.height
    );
    this.bgOverlay.alpha = 0;
    this.bgOverlay.visible = false;
    this.bgOverlay.setTint(0x000000);
    this.bgOverlay.setInteractive();

    this.dropItemPanelBg = UIHelper.createDialog(
      this.scene,
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2 - 30,
      500,
      500,
      'dialog9patch2',
      15
    );
    this.dropItemPanelBg.alpha = 0;
    this.dropItemPanelBg.visible = false;

    this.dropItemPanelIcon = this.scene.add.image(
      this.dropItemPanelBg.x - this.dropItemPanelBg.width / 2 + 20,
      this.dropItemPanelBg.y - this.dropItemPanelBg.height / 2 + 20,
      'effectAtlas1',
      'item_health_s'
    );
    this.dropItemPanelIcon.setDisplaySize(100, 100);
    this.dropItemPanelIcon.alpha = 0;
    this.dropItemPanelIcon.visible = false;

    this.dropItemPanelTitle = this.scene.add.bitmapText(
      this.dropItemPanelBg.x,
      this.dropItemPanelBg.y - this.dropItemPanelBg.height / 2 + 40,
      'sakkalbold',
      '',
      60
    );
    this.dropItemPanelTitle.setOrigin(0.5);
    this.dropItemPanelTitle.alpha = 0;
    this.dropItemPanelTitle.visible = false;

    this.dropItemPanelInfo = this.scene.add.bitmapText(
      this.dropItemPanelTitle.x,
      this.dropItemPanelTitle.y + 50,
      'sakkalicon',
      '',
      35
    );
    this.dropItemPanelInfo.alpha = 0;
    this.dropItemPanelInfo.setOrigin(0.5, 0);
    this.dropItemPanelInfo.setMaxWidth(450);
    this.dropItemPanelInfo.setCenterAlign();
    this.dropItemPanelInfo.visible = false;

    this.rewardCount = 0;

    this.dropItemPanelItemBG = [];
    this.dropItemPanelItemIcons = [];
    this.dropItemPanelItemNames = [];

    for (var i = 0; i < 4; i++) {
      var bg = this.scene.rexUI.add
        .sizer({
          orientation: 'y',
          x: this.dropItemPanelBg.x,
          y: this.dropItemPanelInfo.y + 120 + i * 80,
          width: this.dropItemPanelBg.width - 20,
          height: 70,
        })
        .addBackground(
          this.scene.rexUI.add
            .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
            .setStrokeStyle(1, UIHelper.COLOR_LIGHT)
        )
        .layout();
      bg.visible = false;
      bg._item = undefined;
      bg._itemID = undefined;
      bg.setInteractive();
      this.dropItemPanelItemBG.push(bg);

      var spr = this.scene.add.image(
        bg.x - this.dropItemPanelBg.width / 2 + 80,
        bg.y,
        'effectAtlas1',
        'item_health_s'
      );
      spr.visible = false;
      spr.setDisplaySize(64, 64);
      this.dropItemPanelItemIcons.push(spr);

      var name = this.scene.add.bitmapText(
        spr.x + 70,
        spr.y,
        'sakkalbold',
        '',
        35
      );
      name.visible = false;
      name.setOrigin(0, 0.5);
      this.dropItemPanelItemNames.push(name);

      bg.on(
        'pointerover',
        function (bg, spr, name) {
          spr.setTint(0x00ff00);
          name.setTint(0x00ff00);
        }.bind(this, bg, spr, name)
      );

      bg.on(
        'pointerout',
        function (bg, spr, name) {
          spr.setTint(0xffffff);
          name.setTint(0xffffff);
        }.bind(this, bg, spr, name)
      );

      bg.on(
        'pointerup',
        function (bg, spr, name) {
          var index = this.player.items.indexOf(bg._itemID);
          if (index > -1) {
            this.player.items[index] = this.itemIDToReplaceWith;
          }
          this.hideDropItemPanel();
          if (this.onItemPressed && typeof this.onItemPressed === 'function') {
            this.onItemPressed();
          }
        }.bind(this, bg, spr, name)
      );
    }

    this.dropItemPanelCancelButton = UIHelper.createMenuButton(
      this.scene,
      'menubutton',
      this.dropItemPanelBg.y + this.dropItemPanelBg.height / 2 + 20,
      Utils.GlobalSettings.text.shop.cancel,
      false
    );
    this.dropItemPanelCancelButton.visible = false;
    this.dropItemPanelCancelButton.on('pointerup', () => {
      this.hideDropItemPanel();
      if (this.onCancelPressed && typeof this.onCancelPressed === 'function') {
        this.onCancelPressed();
      }
    });
  }

  setPlayer(player) {
    this.player = player;
  }

  toggleDropItemPanelVisibility(visible) {
    this.dropItemPanelBg.visible = visible;
    this.dropItemPanelIcon.visible = visible;
    this.dropItemPanelTitle.visible = visible;
    this.dropItemPanelInfo.visible = visible;
    this.dropItemPanelCancelButton.visible = visible;
    this.dropItemPanelCancelButton.buttonTxt.visible = visible;

    for (var i = 0; i < this.dropItemPanelItemBG.length; i++) {
      this.dropItemPanelItemBG[i].visible = visible;
      this.dropItemPanelItemIcons[i].visible = visible;
      this.dropItemPanelItemNames[i].visible = visible;
    }
  }

  toggleDropItemPanelAlpha(alpha) {
    this.dropItemPanelBg.alpha = alpha;
    this.dropItemPanelIcon.alpha = alpha;
    this.dropItemPanelTitle.alpha = alpha;
    this.dropItemPanelInfo.alpha = alpha;
    this.dropItemPanelCancelButton.alpha = alpha;
    this.dropItemPanelCancelButton.buttonTxt.alpha = alpha;

    for (var i = 0; i < this.dropItemPanelItemBG.length; i++) {
      this.dropItemPanelItemBG[i].alpha = alpha;
      this.dropItemPanelItemIcons[i].alpha = alpha;
      this.dropItemPanelItemNames[i].alpha = alpha;
    }
  }

  hideDropItemPanel() {
    this.bgOverlay.alpha = 0.8;
    this.bgOverlay.visible = true;

    this.toggleDropItemPanelAlpha(1);
    this.toggleDropItemPanelVisibility(true);

    for (var i = 0; i < this.dropItemPanelItemBG.length; i++) {
      this.dropItemPanelItemBG[i].visible = false;
      this.dropItemPanelItemIcons[i].visible = false;
      this.dropItemPanelItemNames[i].visible = false;
    }

    this.scene.tweens.add({
      targets: [this.bgOverlay],
      alpha: { from: 0.8, to: 0 },
      ease: 'Linear.easeOut',
      duration: 500,
      onComplete: () => {
        this.bgOverlay.visible = false;
      },
    });

    this.scene.tweens.add({
      targets: [
        this.dropItemPanelBg,
        this.dropItemPanelIcon,
        this.dropItemPanelTitle,
        this.dropItemPanelInfo,
        this.dropItemPanelCancelButton,
        this.dropItemPanelCancelButton.buttonTxt,
        ...this.dropItemPanelItemBG,
        ...this.dropItemPanelItemIcons,
        ...this.dropItemPanelItemNames,
      ],
      alpha: { from: 1, to: 0 },
      ease: 'Linear.easeOut',
      duration: 500,
      onComplete: () => {
        this.toggleDropItemPanelVisibility(false);
      },
    });
  }

  showDropItemPanel(delay = 0, itemIDToReplaceWith) {
    this.itemIDToReplaceWith = itemIDToReplaceWith;
    var item = Utils.GlobalSettings.itemsList[itemIDToReplaceWith];

    this.toggleDropItemPanelAlpha(0);
    this.toggleDropItemPanelVisibility(true);

    this.dropItemPanelTitle.setText(
      Utils.GlobalSettings.text.dropItemPanel.title
    );

    this.dropItemPanelInfo.setText(
      Utils.GlobalSettings.text.dropItemPanel.instructions
    );

    this.dropItemPanelIcon.setFrame(item.image);

    var itemIDs = this.player.items;

    for (var i = 0; i < itemIDs.length; i++) {
      var existingItem = Utils.GlobalSettings.itemsList[itemIDs[i]];

      this.dropItemPanelItemBG[i]._item = existingItem;
      this.dropItemPanelItemBG[i]._itemID = itemIDs[i];

      this.dropItemPanelItemIcons[i].setFrame(existingItem.image);
      this.dropItemPanelItemNames[i].setText(existingItem.name);
    }

    this.bgOverlay.alpha = 0;
    this.bgOverlay.visible = true;

    this.scene.tweens.add({
      targets: [this.bgOverlay],
      alpha: { from: 0, to: 0.8 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: delay,
    });

    this.scene.tweens.add({
      targets: [
        this.dropItemPanelBg,
        this.dropItemPanelIcon,
        this.dropItemPanelTitle,
        this.dropItemPanelInfo,
        this.dropItemPanelCancelButton,
        this.dropItemPanelCancelButton.buttonTxt,
        ...this.dropItemPanelItemBG,
        ...this.dropItemPanelItemIcons,
        ...this.dropItemPanelItemNames,
      ],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: delay,
    });
  }

  destroy() {
    if (this.dropItemPanelBg) {
      this.dropItemPanelBg.destroy();
      this.dropItemPanelBg = null;
    }

    if (
      this.dropItemPanelCancelButton &&
      this.dropItemPanelCancelButton.buttonTxt
    ) {
      this.dropItemPanelCancelButton.buttonTxt.destroy();
      this.dropItemPanelCancelButton.buttonTxt = null;
      this.dropItemPanelCancelButton.destroy();
      this.dropItemPanelCancelButton = null;
    }

    if (this.bgOverlay) {
      this.bgOverlay.destroy();
      this.bgOverlay = null;
    }

    if (this.dropItemPanelIcon) {
      this.dropItemPanelIcon.destroy();
      this.dropItemPanelIcon = null;
    }

    if (this.dropItemPanelTitle) {
      this.dropItemPanelTitle.destroy();
      this.dropItemPanelTitle = null;
    }

    if (this.dropItemPanelInfo) {
      this.dropItemPanelInfo.destroy();
      this.dropItemPanelInfo = null;
    }

    if (this.dropItemPanelItemBG) {
      for (var i = 0; i < this.dropItemPanelItemBG.length; i++) {
        this.dropItemPanelItemBG[i].destroy();
        this.dropItemPanelItemIcons[i].destroy();
        this.dropItemPanelItemNames[i].destroy();

        this.dropItemPanelItemBG[i] = null;
        this.dropItemPanelItemIcons[i] = null;
        this.dropItemPanelItemNames[i] = null;
      }
    }

    this.dropItemPanelItemBG = null;
    this.dropItemPanelItemIcons = null;
    this.dropItemPanelItemNames = null;

    console.log('kill dropitem panel');
  }
}
