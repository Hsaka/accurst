import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import GameCalculations from 'utils/gameCalculations';

export default class SwapWeaponPanel {
  constructor(scene) {
    this.scene = scene;
  }

  setupSwapWeaponPanel(player) {
    this.player = player;
    this.onCancelPressed = undefined;
    this.onConfirmPressed = undefined;

    this.bgOverlay = this.scene.add.image(0, 0, 'atlas1', 'overlay');
    this.bgOverlay.setOrigin(0);
    this.bgOverlay.setDisplaySize(
      Utils.GlobalSettings.width,
      Utils.GlobalSettings.height
    );
    this.bgOverlay.alpha = 0;
    this.bgOverlay.visible = false;
    this.bgOverlay.setTint(0x000000);
    this.bgOverlay.setInteractive();

    this.swapWeaponPanelBg = UIHelper.createDialog(
      this.scene,
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2 - 50,
      500,
      600,
      'dialog9patch2',
      15
    );
    this.swapWeaponPanelBg.alpha = 0;
    this.swapWeaponPanelBg.visible = false;

    this.swapWeaponPanelIcon = this.scene.add.image(
      this.swapWeaponPanelBg.x - this.swapWeaponPanelBg.width / 2 + 20,
      this.swapWeaponPanelBg.y - this.swapWeaponPanelBg.height / 2 + 20,
      'atlas1',
      'rune_attack'
    );
    this.swapWeaponPanelIcon.setDisplaySize(100, 100);
    this.swapWeaponPanelIcon.alpha = 0;
    this.swapWeaponPanelIcon.visible = false;

    this.swapWeaponPanelTitle = this.scene.add.bitmapText(
      this.swapWeaponPanelBg.x,
      this.swapWeaponPanelBg.y - this.swapWeaponPanelBg.height / 2 + 40,
      'sakkalbold',
      '',
      60
    );
    this.swapWeaponPanelTitle.setOrigin(0.5);
    this.swapWeaponPanelTitle.alpha = 0;
    this.swapWeaponPanelTitle.visible = false;

    this.swapWeaponPanelInfo = this.scene.add.bitmapText(
      this.swapWeaponPanelTitle.x,
      this.swapWeaponPanelTitle.y + 50,
      'sakkalicon',
      '',
      35
    );
    this.swapWeaponPanelInfo.alpha = 0;
    this.swapWeaponPanelInfo.setOrigin(0.5, 0);
    this.swapWeaponPanelInfo.setMaxWidth(450);
    this.swapWeaponPanelInfo.setCenterAlign();
    this.swapWeaponPanelInfo.visible = false;

    var weapon = Utils.GlobalSettings.weaponsList[this.player.weapon];

    this.swapArrow = this.scene.add.image(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2 + 10,
      'atlas1',
      'swap_arrow'
    );
    this.swapArrow.visible = false;

    this.existingWeaponBg = this.scene.rexUI.add
      .sizer({
        orientation: 'y',
        x: this.swapWeaponPanelBg.x,
        y: this.swapWeaponPanelInfo.y + 170,
        width: this.swapWeaponPanelBg.width - 20,
        height: 175,
      })
      .addBackground(
        this.scene.rexUI.add
          .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
          .setStrokeStyle(1, UIHelper.COLOR_LIGHT)
      )
      .layout();
    this.existingWeaponBg.visible = false;

    this.existingWeaponIcon = this.scene.add.image(
      this.existingWeaponBg.x - this.swapWeaponPanelBg.width / 2 + 80,
      this.existingWeaponBg.y,
      'atlas1',
      weapon.image
    );
    this.existingWeaponIcon.visible = false;
    this.existingWeaponIcon.setDisplaySize(100, 100);

    this.existingLabel = this.scene.add.bitmapText(
      this.existingWeaponIcon.x - 50,
      this.existingWeaponIcon.y - 100,
      'sakkalbold',
      Utils.GlobalSettings.text.swapWeaponPanel.existing,
      30
    );
    this.existingLabel.visible = false;

    this.existingWeaponName = this.scene.add.bitmapText(
      this.existingWeaponIcon.x + 70,
      this.existingWeaponIcon.y,
      'sakkalbold',
      `${weapon.name}\nATK:  ${weapon.damage_boost}, HP:  ${
        weapon.health_boost
      }, POW:  ${weapon.power_boost}\nMAG:  ${weapon.magic_boost}, SPD:  ${
        weapon.speed_boost
      }, DEF:  ${weapon.defense_boost}\nCurse: ${
        weapon.status_effect.length > 0 ? weapon.status_effect : 'None'
      }\nEnchanted: ${weapon.skills.length > 0 ? 'Yes' : 'No'}`,
      35
    );
    this.existingWeaponName.visible = false;
    this.existingWeaponName.setOrigin(0, 0.5);

    this.newWeaponBg = this.scene.rexUI.add
      .sizer({
        orientation: 'y',
        x: this.swapWeaponPanelBg.x,
        y: this.existingWeaponBg.y + 230,
        width: this.swapWeaponPanelBg.width - 20,
        height: 175,
      })
      .addBackground(
        this.scene.rexUI.add
          .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
          .setStrokeStyle(1, UIHelper.COLOR_LIGHT)
      )
      .layout();
    this.newWeaponBg.visible = false;

    this.newWeaponIcon = this.scene.add.image(
      this.newWeaponBg.x - this.swapWeaponPanelBg.width / 2 + 80,
      this.newWeaponBg.y,
      'atlas1',
      weapon.image
    );
    this.newWeaponIcon.visible = false;
    this.newWeaponIcon.setDisplaySize(100, 100);

    this.newLabel = this.scene.add.bitmapText(
      this.newWeaponIcon.x - 50,
      this.newWeaponIcon.y - 100,
      'sakkalbold',
      Utils.GlobalSettings.text.swapWeaponPanel.new,
      30
    );
    this.newLabel.visible = false;

    this.newWeaponName = this.scene.add.bitmapText(
      this.newWeaponIcon.x + 70,
      this.newWeaponIcon.y,
      'sakkalbold',
      `${weapon.name}\nATK:  ${weapon.damage_boost}, HP:  ${
        weapon.health_boost
      }, POW:  ${weapon.power_boost}\nMAG:  ${weapon.magic_boost}, SPD:  ${
        weapon.speed_boost
      }, DEF:  ${weapon.defense_boost}\nCurse: ${
        weapon.status_effect.length > 0 ? weapon.status_effect : 'None'
      }\nEnchanted: ${weapon.skills.length > 0 ? 'Yes' : 'No'}`,
      35
    );
    this.newWeaponName.visible = false;
    this.newWeaponName.setOrigin(0, 0.5);

    this.swapWeaponPanelConfirmButton = UIHelper.createMenuButton(
      this.scene,
      'menubutton',
      this.swapWeaponPanelBg.y + this.swapWeaponPanelBg.height / 2 + 20,
      Utils.GlobalSettings.text.swapWeaponPanel.replace,
      false
    );
    this.swapWeaponPanelConfirmButton.visible = false;
    this.swapWeaponPanelConfirmButton.on('pointerup', () => {
      this.hideSwapWeaponPanel();
      if (
        this.onConfirmPressed &&
        typeof this.onConfirmPressed === 'function'
      ) {
        this.onConfirmPressed();
      }
    });

    this.swapWeaponPanelCancelButton = UIHelper.createMenuButton(
      this.scene,
      'menubutton',
      this.swapWeaponPanelConfirmButton.y +
        this.swapWeaponPanelConfirmButton.height / 2 +
        40,
      Utils.GlobalSettings.text.swapWeaponPanel.cancel,
      false
    );
    this.swapWeaponPanelCancelButton.visible = false;
    this.swapWeaponPanelCancelButton.on('pointerup', () => {
      this.hideSwapWeaponPanel();
      if (this.onCancelPressed && typeof this.onCancelPressed === 'function') {
        this.onCancelPressed();
      }
    });
  }

  setPlayer(player) {
    this.player = player;
  }

  toggleSwapWeaponPanelVisibility(visible) {
    this.swapWeaponPanelBg.visible = visible;
    this.swapWeaponPanelIcon.visible = visible;
    this.swapWeaponPanelTitle.visible = visible;
    this.swapWeaponPanelInfo.visible = visible;
    this.swapWeaponPanelConfirmButton.visible = visible;
    this.swapWeaponPanelConfirmButton.buttonTxt.visible = visible;
    this.swapWeaponPanelCancelButton.visible = visible;
    this.swapWeaponPanelCancelButton.buttonTxt.visible = visible;
    this.existingWeaponBg.visible = visible;
    this.existingWeaponIcon.visible = visible;
    this.existingLabel.visible = visible;
    this.existingWeaponName.visible = visible;
    this.newWeaponBg.visible = visible;
    this.newWeaponIcon.visible = visible;
    this.newWeaponName.visible = visible;
    this.newLabel.visible = visible;
    this.swapArrow.visible = visible;
  }

  toggleSwapWeaponPanelAlpha(alpha) {
    this.swapWeaponPanelBg.alpha = alpha;
    this.swapWeaponPanelIcon.alpha = alpha;
    this.swapWeaponPanelTitle.alpha = alpha;
    this.swapWeaponPanelInfo.alpha = alpha;
    this.swapWeaponPanelConfirmButton.alpha = alpha;
    this.swapWeaponPanelConfirmButton.buttonTxt.alpha = alpha;
    this.swapWeaponPanelCancelButton.alpha = alpha;
    this.swapWeaponPanelCancelButton.buttonTxt.alpha = alpha;
    this.existingWeaponBg.alpha = alpha;
    this.existingWeaponIcon.alpha = alpha;
    this.existingLabel.alpha = alpha;
    this.existingWeaponName.alpha = alpha;
    this.newWeaponBg.alpha = alpha;
    this.newWeaponIcon.alpha = alpha;
    this.newWeaponName.alpha = alpha;
    this.newLabel.alpha = alpha;
    this.swapArrow.alpha = alpha;
  }

  hideSwapWeaponPanel() {
    this.bgOverlay.alpha = 0.8;
    this.bgOverlay.visible = true;

    this.toggleSwapWeaponPanelAlpha(1);
    this.toggleSwapWeaponPanelVisibility(true);

    this.scene.tweens.add({
      targets: [this.bgOverlay],
      alpha: { from: 0.8, to: 0 },
      ease: 'Linear.easeOut',
      duration: 500,
      onComplete: () => {
        this.bgOverlay.visible = false;
      },
    });

    this.scene.tweens.add({
      targets: [
        this.swapWeaponPanelBg,
        this.swapWeaponPanelIcon,
        this.swapWeaponPanelTitle,
        this.swapWeaponPanelInfo,
        this.swapWeaponPanelConfirmButton,
        this.swapWeaponPanelConfirmButton.buttonTxt,
        this.swapWeaponPanelCancelButton,
        this.swapWeaponPanelCancelButton.buttonTxt,
        this.existingWeaponBg,
        this.existingWeaponIcon,
        this.existingLabel,
        this.existingWeaponName,
        this.newWeaponBg,
        this.newWeaponIcon,
        this.newLabel,
        this.newWeaponName,
        this.swapArrow,
      ],
      alpha: { from: 1, to: 0 },
      ease: 'Linear.easeOut',
      duration: 500,
      onComplete: () => {
        this.toggleSwapWeaponPanelVisibility(false);
      },
    });
  }

  showSwapWeaponPanel(delay = 0, newWeaponID) {
    this.weaponID = newWeaponID;

    this.toggleSwapWeaponPanelAlpha(0);
    this.toggleSwapWeaponPanelVisibility(true);

    this.swapWeaponPanelTitle.setText(
      Utils.GlobalSettings.text.swapWeaponPanel.title
    );

    this.swapWeaponPanelInfo.setText(
      Utils.GlobalSettings.text.swapWeaponPanel.instructions
    );

    var weapon = Utils.GlobalSettings.weaponsList[this.player.weapon];

    this.existingWeaponIcon.setFrame(weapon.image);

    this.existingWeaponName.setText(
      `${weapon.name}\nATK:  ${weapon.damage_boost}, HP:  ${
        weapon.health_boost
      }, POW:  ${weapon.power_boost}\nMAG:  ${weapon.magic_boost}, SPD:  ${
        weapon.speed_boost
      }, DEF:  ${weapon.defense_boost}\nCurse: ${
        weapon.status_effect.length > 0 ? weapon.status_effect : 'None'
      }\nEnchanted: ${weapon.skills.length > 0 ? 'Yes' : 'No'}`
    );

    var newWeapon = Utils.GlobalSettings.weaponsList[newWeaponID];

    this.newWeaponIcon.setFrame(newWeapon.image);

    this.newWeaponName.setText(
      `${newWeapon.name}\nATK:  ${newWeapon.damage_boost}, HP:  ${
        newWeapon.health_boost
      }, POW:  ${newWeapon.power_boost}\nMAG:  ${
        newWeapon.magic_boost
      }, SPD:  ${newWeapon.speed_boost}, DEF:  ${
        newWeapon.defense_boost
      }\nCurse: ${
        newWeapon.status_effect.length > 0 ? newWeapon.status_effect : 'None'
      }\nEnchanted: ${newWeapon.skills.length > 0 ? 'Yes' : 'No'}`
    );

    this.bgOverlay.alpha = 0;
    this.bgOverlay.visible = true;

    this.scene.tweens.add({
      targets: [this.bgOverlay],
      alpha: { from: 0, to: 0.8 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: delay,
    });

    this.scene.tweens.add({
      targets: [
        this.swapWeaponPanelBg,
        this.swapWeaponPanelIcon,
        this.swapWeaponPanelTitle,
        this.swapWeaponPanelInfo,
        this.swapWeaponPanelConfirmButton,
        this.swapWeaponPanelConfirmButton.buttonTxt,
        this.swapWeaponPanelCancelButton,
        this.swapWeaponPanelCancelButton.buttonTxt,
        this.existingWeaponBg,
        this.existingWeaponIcon,
        this.existingLabel,
        this.existingWeaponName,
        this.newWeaponBg,
        this.newWeaponIcon,
        this.newLabel,
        this.newWeaponName,
        this.swapArrow,
      ],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: delay,
    });
  }

  destroy() {
    if (this.swapWeaponPanelBg) {
      this.swapWeaponPanelBg.destroy();
      this.swapWeaponPanelBg = null;
    }

    if (
      this.swapWeaponPanelCancelButton &&
      this.swapWeaponPanelCancelButton.buttonTxt
    ) {
      this.swapWeaponPanelCancelButton.buttonTxt.destroy();
      this.swapWeaponPanelCancelButton.buttonTxt = null;
      this.swapWeaponPanelCancelButton.destroy();
      this.swapWeaponPanelCancelButton = null;
    }

    if (
      this.swapWeaponPanelConfirmButton &&
      this.swapWeaponPanelConfirmButton.buttonTxt
    ) {
      this.swapWeaponPanelConfirmButton.buttonTxt.destroy();
      this.swapWeaponPanelConfirmButton.buttonTxt = null;
      this.swapWeaponPanelConfirmButton.destroy();
      this.swapWeaponPanelConfirmButton = null;
    }

    if (this.bgOverlay) {
      this.bgOverlay.destroy();
      this.bgOverlay = null;
    }

    if (this.swapWeaponPanelIcon) {
      this.swapWeaponPanelIcon.destroy();
      this.swapWeaponPanelIcon = null;
    }

    if (this.swapWeaponPanelTitle) {
      this.swapWeaponPanelTitle.destroy();
      this.swapWeaponPanelTitle = null;
    }

    if (this.swapWeaponPanelInfo) {
      this.swapWeaponPanelInfo.destroy();
      this.swapWeaponPanelInfo = null;
    }

    if (this.swapArrow) {
      this.swapArrow.destroy();
      this.swapArrow = null;
    }

    if (this.existingWeaponBg) {
      this.existingWeaponBg.destroy();
      this.existingWeaponBg = null;
    }

    if (this.existingWeaponIcon) {
      this.existingWeaponIcon.destroy();
      this.existingWeaponIcon = null;
    }

    if (this.existingLabel) {
      this.existingLabel.destroy();
      this.existingLabel = null;
    }

    if (this.existingWeaponName) {
      this.existingWeaponName.destroy();
      this.existingWeaponName = null;
    }

    if (this.newWeaponBg) {
      this.newWeaponBg.destroy();
      this.newWeaponBg = null;
    }

    if (this.newWeaponIcon) {
      this.newWeaponIcon.destroy();
      this.newWeaponIcon = null;
    }

    if (this.newLabel) {
      this.newLabel.destroy();
      this.newLabel = null;
    }

    if (this.newWeaponName) {
      this.newWeaponName.destroy();
      this.newWeaponName = null;
    }

    console.log('kill swap weapon panel');
  }
}
