export default class NameGenerator {
  constructor() {
    this.name_set = {};
    this.chain_cache = {};
  }

  setNameSet(set, name) {
    this.name_set = {};
    this.name_set[name] = set;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // generator function

  generate_name(type) {
    var chain;
    if ((chain = this.markov_chain(type))) {
      var name = this.markov_name(chain);
      name = name.replace(/-/g, '');
      return name;
    }
    return '';
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // generate multiple

  name_list(type, n_of) {
    var list = [];

    var i;
    for (i = 0; i < n_of; i++) {
      list.push(this.generate_name(type));
    }
    return list;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // get markov chain by type

  markov_chain(type) {
    var chain;
    if ((chain = this.chain_cache[type])) {
      return chain;
    } else {
      var list;
      if ((list = this.name_set[type])) {
        var chain;
        if ((chain = this.construct_chain(list))) {
          this.chain_cache[type] = chain;
          return chain;
        }
      }
    }
    return false;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // construct markov chain from list of names

  construct_chain(list) {
    var chain = {};

    var i;
    for (i = 0; i < list.length; i++) {
      var names = list[i].split(/\s+/);
      chain = this.incr_chain(chain, 'parts', names.length);

      var j;
      for (j = 0; j < names.length; j++) {
        var name = names[j];
        chain = this.incr_chain(chain, 'name_len', name.length);

        var c = name.substr(0, 1);
        chain = this.incr_chain(chain, 'initial', c);

        var string = name.substr(1);
        var last_c = c;

        while (string.length > 0) {
          var c = string.substr(0, 1);
          chain = this.incr_chain(chain, last_c, c);

          string = string.substr(1);
          last_c = c;
        }
      }
    }
    return this.scale_chain(chain);
  }

  incr_chain(chain, key, token) {
    if (chain[key]) {
      if (chain[key][token]) {
        chain[key][token]++;
      } else {
        chain[key][token] = 1;
      }
    } else {
      chain[key] = {};
      chain[key][token] = 1;
    }
    return chain;
  }

  scale_chain(chain) {
    var table_len = {};

    var key;
    for (key in chain) {
      table_len[key] = 0;

      var token;
      for (token in chain[key]) {
        var count = chain[key][token];
        var weighted = Math.floor(Math.pow(count, 1.3));

        chain[key][token] = weighted;
        table_len[key] += weighted;
      }
    }
    chain['table_len'] = table_len;
    return chain;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // construct name from markov chain

  markov_name(chain) {
    var parts = this.select_link(chain, 'parts');
    var names = [];

    var i;
    for (i = 0; i < parts; i++) {
      var name_len = this.select_link(chain, 'name_len');
      var c = this.select_link(chain, 'initial');
      var name = c;
      var last_c = c;

      while (name.length < name_len) {
        c = this.select_link(chain, last_c);
        name += c;
        last_c = c;
      }
      names.push(name);
    }
    return names.join(' ');
  }

  select_link(chain, key) {
    var len = chain['table_len'][key];
    var idx = Math.floor(Math.random() * len);

    var t = 0;
    var token;
    for (token in chain[key]) {
      t += chain[key][token];
      if (idx < t) {
        return token;
      }
    }
    return '-';
  }
}
