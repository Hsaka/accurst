<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.2</string>
        <key>fileName</key>
        <string>E:/dev/sync/wip/dungeon-crawler/app/static/assets/atlas.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-hash</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>sprites-{n}.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">raw/cards/blueroom_small.png</key>
            <key type="filename">raw/cards/greenroom_small.png</key>
            <key type="filename">raw/cards/shop_small.png</key>
            <key type="filename">raw/cards/shrine_small.png</key>
            <key type="filename">raw/cards/treasureroom_small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>85,62,169,124</rect>
                <key>scale9Paddings</key>
                <rect>85,62,169,124</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/cards/bossroom_small.png</key>
            <key type="filename">raw/cards/roomback2.png</key>
            <key type="filename">raw/cards/roomback3.png</key>
            <key type="filename">raw/ui/location.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,31,85,62</rect>
                <key>scale9Paddings</key>
                <rect>42,31,85,62</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/cards/combatback.png</key>
            <key type="filename">raw/cards/roomback.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>103,141,207,281</rect>
                <key>scale9Paddings</key>
                <rect>103,141,207,281</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/heroes/hero_bard.png</key>
            <key type="filename">raw/heroes/hero_knight.png</key>
            <key type="filename">raw/heroes/hero_monk.png</key>
            <key type="filename">raw/heroes/hero_ranger.png</key>
            <key type="filename">raw/heroes/hero_rogue.png</key>
            <key type="filename">raw/heroes/hero_wizard.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,75,150,150</rect>
                <key>scale9Paddings</key>
                <rect>75,75,150,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/menubuttons/menubutton1.png</key>
            <key type="filename">raw/menubuttons/menubutton2.png</key>
            <key type="filename">raw/menubuttons/menubuttonspecial1.png</key>
            <key type="filename">raw/menubuttons/menubuttonspecial2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,30,200,60</rect>
                <key>scale9Paddings</key>
                <rect>100,30,200,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/runes/blue_chest.png</key>
            <key type="filename">raw/runes/green_chest.png</key>
            <key type="filename">raw/runes/red_chest.png</key>
            <key type="filename">raw/runes/rune_attack.png</key>
            <key type="filename">raw/runes/rune_defend.png</key>
            <key type="filename">raw/runes/rune_endturn.png</key>
            <key type="filename">raw/runes/rune_exp.png</key>
            <key type="filename">raw/runes/rune_health.png</key>
            <key type="filename">raw/runes/rune_item.png</key>
            <key type="filename">raw/runes/rune_level.png</key>
            <key type="filename">raw/runes/rune_magic.png</key>
            <key type="filename">raw/runes/rune_mystery.png</key>
            <key type="filename">raw/runes/rune_power.png</key>
            <key type="filename">raw/runes/rune_random.png</key>
            <key type="filename">raw/runes/rune_room.png</key>
            <key type="filename">raw/runes/rune_weapon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/status/status_bleeding.png</key>
            <key type="filename">raw/status/status_normal.png</key>
            <key type="filename">raw/status/status_poisoned.png</key>
            <key type="filename">raw/status/status_slowed.png</key>
            <key type="filename">raw/status/status_stunned.png</key>
            <key type="filename">raw/status/status_weakened.png</key>
            <key type="filename">raw/weapons/weapon0.png</key>
            <key type="filename">raw/weapons/weapon1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/systembuttons/systembuttons0.png</key>
            <key type="filename">raw/systembuttons/systembuttons1.png</key>
            <key type="filename">raw/systembuttons/systembuttons10.png</key>
            <key type="filename">raw/systembuttons/systembuttons11.png</key>
            <key type="filename">raw/systembuttons/systembuttons12.png</key>
            <key type="filename">raw/systembuttons/systembuttons13.png</key>
            <key type="filename">raw/systembuttons/systembuttons2.png</key>
            <key type="filename">raw/systembuttons/systembuttons3.png</key>
            <key type="filename">raw/systembuttons/systembuttons4.png</key>
            <key type="filename">raw/systembuttons/systembuttons5.png</key>
            <key type="filename">raw/systembuttons/systembuttons6.png</key>
            <key type="filename">raw/systembuttons/systembuttons7.png</key>
            <key type="filename">raw/systembuttons/systembuttons8.png</key>
            <key type="filename">raw/systembuttons/systembuttons9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,25,25</rect>
                <key>scale9Paddings</key>
                <rect>13,13,25,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/title_/title_0.png</key>
            <key type="filename">raw/title_/title_1.png</key>
            <key type="filename">raw/title_/title_10.png</key>
            <key type="filename">raw/title_/title_11.png</key>
            <key type="filename">raw/title_/title_12.png</key>
            <key type="filename">raw/title_/title_13.png</key>
            <key type="filename">raw/title_/title_14.png</key>
            <key type="filename">raw/title_/title_15.png</key>
            <key type="filename">raw/title_/title_16.png</key>
            <key type="filename">raw/title_/title_17.png</key>
            <key type="filename">raw/title_/title_18.png</key>
            <key type="filename">raw/title_/title_19.png</key>
            <key type="filename">raw/title_/title_2.png</key>
            <key type="filename">raw/title_/title_20.png</key>
            <key type="filename">raw/title_/title_21.png</key>
            <key type="filename">raw/title_/title_22.png</key>
            <key type="filename">raw/title_/title_23.png</key>
            <key type="filename">raw/title_/title_24.png</key>
            <key type="filename">raw/title_/title_25.png</key>
            <key type="filename">raw/title_/title_26.png</key>
            <key type="filename">raw/title_/title_27.png</key>
            <key type="filename">raw/title_/title_28.png</key>
            <key type="filename">raw/title_/title_29.png</key>
            <key type="filename">raw/title_/title_3.png</key>
            <key type="filename">raw/title_/title_4.png</key>
            <key type="filename">raw/title_/title_5.png</key>
            <key type="filename">raw/title_/title_6.png</key>
            <key type="filename">raw/title_/title_7.png</key>
            <key type="filename">raw/title_/title_8.png</key>
            <key type="filename">raw/title_/title_9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>128,32,256,64</rect>
                <key>scale9Paddings</key>
                <rect>128,32,256,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/ui/arrow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,26,20,53</rect>
                <key>scale9Paddings</key>
                <rect>10,26,20,53</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/ui/back1.png</key>
            <key type="filename">raw/ui/back2.png</key>
            <key type="filename">raw/ui/forward1.png</key>
            <key type="filename">raw/ui/forward2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,21,33,43</rect>
                <key>scale9Paddings</key>
                <rect>17,21,33,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/ui/locked.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,50,50</rect>
                <key>scale9Paddings</key>
                <rect>25,25,50,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/ui/number1.png</key>
            <key type="filename">raw/ui/number2.png</key>
            <key type="filename">raw/ui/number3.png</key>
            <key type="filename">raw/ui/number4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,20,40,40</rect>
                <key>scale9Paddings</key>
                <rect>20,20,40,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/ui/overlay.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>0,0,1,1</rect>
                <key>scale9Paddings</key>
                <rect>0,0,1,1</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/ui/swap_arrow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,20,45,40</rect>
                <key>scale9Paddings</key>
                <rect>23,20,45,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">raw/ui/timeline.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>125,4,250,8</rect>
                <key>scale9Paddings</key>
                <rect>125,4,250,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>raw/systembuttons</filename>
            <filename>raw/title_</filename>
            <filename>raw/menubuttons</filename>
            <filename>raw/ui</filename>
            <filename>raw/heroes</filename>
            <filename>raw/runes</filename>
            <filename>raw/cards</filename>
            <filename>raw/weapons</filename>
            <filename>raw/status</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
