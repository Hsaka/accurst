import Utils from 'utils/utils';
import BootScene from 'scenes/boot';
import PreloaderScene from 'scenes/preloader';
import GameUIScene from 'scenes/gameUI';
import MainScene from 'scenes/main';
import PlayerSelectScene from 'scenes/playerSelect';
import SetupPlayerScene from 'scenes/setupPlayer';
import ConfirmPlayersScene from 'scenes/confirmPlayers';
import DeckSetupScene from 'scenes/setupDecks';
import MapScene from 'scenes/map';
import MapExplorationScene from 'scenes/mapExploration';
import BattleScene from 'scenes/battle';
import LevelUpScene from 'scenes/levelUp';
import ShopScene from 'scenes/shop';
import TreasureScene from 'scenes/treasure';
import ShrineScene from 'scenes/shrine';
import UIPlugin from 'plugins/rexuiplugin.min.js';

window.onload = function () {
  // Check that service workers are supported
  //TODO: uncomment to enable service worker in prod
  if ('serviceWorker' in navigator) {
    //navigator.serviceWorker.register('sw.js');
  }

  function resize() {
    var canvas = document.querySelector('canvas');
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    var windowRatio = windowWidth / windowHeight;
    var gameRatio = Utils.GlobalSettings.width / Utils.GlobalSettings.height;
    if (windowRatio < gameRatio) {
      canvas.style.width = windowWidth + 'px';
      canvas.style.height = windowWidth / gameRatio + 'px';
    } else {
      canvas.style.width = windowHeight * gameRatio + 'px';
      canvas.style.height = windowHeight + 'px';
    }
  }

  function inIframe() {
    try {
      return window.self !== window.top;
    } catch (e) {
      return true;
    }
  }

  window.game = new Phaser.Game({
    // See <https://github.com/photonstorm/phaser/blob/master/src/boot/Config.js>

    width: Utils.GlobalSettings.width,
    height: Utils.GlobalSettings.height,
    // zoom: 1,
    // resolution: 1,
    type: Phaser.AUTO,
    parent: 'game',
    // canvas: null,
    // canvasStyle: null,
    // seed: null,
    title: 'Accurst', // 'My Phaser 3 Game'
    url: 'https://accurst.xyz',
    version: '0.0.1',
    input: {
      keyboard: true,
      mouse: true,
      touch: true,
      gamepad: false,
    },
    disableContextMenu: true,
    // banner: false
    banner: {
      // hidePhaser: false,
      // text: 'white',
      background: ['#e54661', '#ffa644', '#998a2f', '#2c594f', '#002d40'],
    },
    // fps: {
    //   min: 10,
    //   target: 60,
    //   forceSetTimeout: false,
    // },
    // pixelArt: false,
    // transparent: false,
    clearBeforeRender: false,
    // backgroundColor: 0x000000, // black
    loader: {
      // baseURL: '',
      path: '',
      maxParallelDownloads: 6,
      // crossOrigin: 'anonymous',
      // timeout: 0
    },
    dom: {
      createContainer: true,
    },
    plugins: {
      scene: [
        {
          key: 'rexUI',
          plugin: UIPlugin,
          mapping: 'rexUI',
        },
      ],
    },
    physics: {
      default: 'arcade',
      arcade: {},
    },
    scale: {
      mode: Phaser.Scale.FIT,
      autoCenter: Phaser.Scale.CENTER_BOTH,
      width: Utils.GlobalSettings.width,
      height: Utils.GlobalSettings.height,
    },
    scene: [
      BootScene,
      PreloaderScene,
      GameUIScene,
      MainScene,
      PlayerSelectScene,
      SetupPlayerScene,
      ConfirmPlayersScene,
      DeckSetupScene,
      MapExplorationScene,
      MapScene,
      BattleScene,
      LevelUpScene,
      ShopScene,
      TreasureScene,
      ShrineScene,
    ],
  });

  resize();
  window.addEventListener('resize', resize, false);

  $('body').css({ background: '#000' });
};
