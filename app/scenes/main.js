import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';

export default class MainScene extends Phaser.Scene {
  constructor() {
    super('main');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    Utils.loadGameState();

    if (Utils.GlobalSettings.gameState) {
      var clone = JSON.parse(JSON.stringify(Utils.GlobalSettings.gameState));
      Utils.SavedSettings.savedGames[clone.id] = clone;
      Utils.save();

      Utils.GlobalSettings.gameState = null;
      Utils.removeGameState();
    }

    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'bg1');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.versionTxt = this.add.bitmapText(
      0,
      0,
      'sakkal',
      'v: ' + Utils.GlobalSettings.version,
      25
    );

    this.anims.create({
      key: 'titleAnim',
      frames: this.anims.generateFrameNames('atlas1', {
        prefix: 'title_',
        start: 0,
        end: 29,
        zeroPad: 0,
      }),
    });

    this.title = this.add.sprite(
      Utils.GlobalSettings.width / 2,
      100,
      'atlas1',
      'title_0'
    );
    this.screenGroup.add(this.title);
    this.title.visible = false;

    this.time.delayedCall(
      250,
      () => {
        this.title.visible = true;
        this.title.play('titleAnim');

        this.tweens.add({
          targets: [this.bg, this.title],
          alpha: { from: 1, to: 0.5 },
          ease: 'Sine.easeInOut',
          duration: 2500,
          delay: 1000,
          repeat: -1,
          yoyo: true,
        });
      },
      [],
      this
    );

    this.setupMenuButtons();

    this.creditIndex = 0;
    this.creditText = [
      'Code: Hsaka',
      'Music: Nicolas Jeudy [Dark Fantasy Studio]',
      'Artwork: Quaternius [http://quaternius.com]',
      'Artwork: Ravenmore [http://dycha.net]',
      'Artwork: Justin Nichol',
      'Artwork: Skoll [https://game-icons.net]',
      'Artwork: Lorc [https://game-icons.net]',
    ];

    this.credits = this.add.bitmapText(
      10,
      Utils.GlobalSettings.height,
      'sakkal',
      this.creditText[0],
      50
    );
    this.credits.alpha = 0;

    this.tweens.add({
      targets: this.credits,
      alpha: 1,
      y: Utils.GlobalSettings.height - 50,
      ease: 'Sine.easeOut',
      duration: 2500,
      delay: 100,
      yoyo: true,
      loop: -1,
      onLoop: () => {
        this.creditIndex++;
        if (this.creditIndex >= this.creditText.length) {
          this.creditIndex = 0;
        }
        this.credits.setText(this.creditText[this.creditIndex]);
      },
    });

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI');
      this.scene.bringToTop('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI');
      this.scene.bringToTop('gameUI');
    }
    this.registry.set('showHome', false);
    this.registry.set('showParty', false);
  }

  setupMenuButtons() {
    var buttonOffsetY = 150;

    this.newGameButton = UIHelper.createMenuButton(
      this,
      'menubuttonspecial',
      Utils.GlobalSettings.height / 2 - buttonOffsetY,
      Utils.GlobalSettings.text.main.new_game
    );

    this.newGameButton.on('pointerup', () => {
      if (!Utils.SavedSettings.muted) {
        Utils.GlobalSettings.selectSound.play();
      }

      var id = Phaser.Utils.String.UUID();
      var seed = Date.now();
      Utils.GlobalSettings.rnd.sow([seed]);
      Utils.GlobalSettings.gameState = { id: id, seed: seed };
      Utils.saveGameState();

      this.scene.start('playerSelect', { from: 'main' });
    });

    this.continueGameButton = UIHelper.createMenuButton(
      this,
      'menubutton',
      this.newGameButton.y + 150,
      Utils.GlobalSettings.text.main.continue_game
    );
    this.continueGameButton.on('pointerup', () => {
      if (!Utils.SavedSettings.muted) {
        Utils.GlobalSettings.selectSound.play();
      }

      Utils.GlobalSettings.gameState = JSON.parse(
        '{"id":"f5ac9179-7bbb-4d43-922f-c582bebc8053","seed":1584764337002,"numPlayers":1,"players":[{"name":"Durte","classType":0,"health":20,"magic":5,"power":15,"maxHealth":20,"maxMagic":5,"maxPower":15,"defenseBonus":0,"powerBonus":0,"experience":0,"speed":5,"requiredExperience":64,"level":1,"deathSaveCount":0,"gold":10,"skills":[5,6],"items":[0],"status":"Normal","weaponProficiency":"sword","weapon":1}],"currentPlayerSetup":1,"floor":1,"timestamp":"2020-03-21T04:18:59.915Z","gameStarted":true,"turn":0,"rooms":{"51d52377-3886-479f-bd42-ef75233ab8fe":{"id":"51d52377-3886-479f-bd42-ef75233ab8fe","roomFrame":"greenroom_small","x":676,"y":744,"isMiddle":true,"isCompleted":false,"hasMystery":false}},"encounter":{"type":"green","combatTurn":0,"combatRound":1,"enemy":{"name":"Wolf","image":"wolf","health":10,"exp_gain":10,"speed":6,"power":3,"skills":[5],"defense":[0],"items":[],"weapon":0,"level":1,"numActions":3,"maxHealth":10,"defenseBonus":0,"powerBonus":0,"status":"Normal"}},"phase":"encounter","mysterySpots":[{"x":3,"y":-5},{"x":-2,"y":-2},{"x":3,"y":3}],"floorName":"Norgi","lastRoomId":"51d52377-3886-479f-bd42-ef75233ab8fe"}'
      );

      Utils.GlobalSettings.rnd.sow([Utils.GlobalSettings.gameState.seed]);

      this.scene.start('battle', { from: 'main' });
      //this.scene.start('levelUp', { from: 'main' });
    });

    this.helpButton = UIHelper.createMenuButton(
      this,
      'menubutton',
      this.continueGameButton.y + 150,
      Utils.GlobalSettings.text.main.how_to_play
    );
    this.helpButton.on('pointerup', () => {});

    this.tweens.add({
      targets: [this.newGameButton, this.newGameButton.buttonTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 1500,
      delay: 1000,
    });

    this.tweens.add({
      targets: [this.continueGameButton, this.continueGameButton.buttonTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 1500,
      delay: 1250,
    });

    this.tweens.add({
      targets: [this.helpButton, this.helpButton.buttonTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 1500,
      delay: 1500,
    });
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.title) {
      this.title.destroy();
      this.title = null;
    }

    this.creditText = null;

    if (this.credits) {
      this.credits.destroy();
      this.credits = null;
    }

    if (this.versionTxt) {
      this.versionTxt.destroy();
      this.versionTxt = null;
    }

    if (this.newGameButton && this.newGameButton.buttonTxt) {
      this.newGameButton.buttonTxt.destroy();
      this.newGameButton.buttonTxt = null;
      this.newGameButton.destroy();
      this.newGameButton = null;
    }

    if (this.continueGameButton && this.continueGameButton.buttonTxt) {
      this.continueGameButton.buttonTxt.destroy();
      this.continueGameButton.buttonTxt = null;
      this.continueGameButton.destroy();
      this.continueGameButton = null;
    }

    if (this.helpButton && this.helpButton.buttonTxt) {
      this.helpButton.buttonTxt.destroy();
      this.helpButton.buttonTxt = null;
      this.helpButton.destroy();
      this.helpButton = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill main');
  }
}
