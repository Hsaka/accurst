import Utils from 'utils/utils';

export default class Preloader extends Phaser.Scene {
  constructor() {
    super('preloader');
    this.progressBar = null;
    this.progressBarRectangle = null;
  }

  preload() {
    this.load.setCORS('Anonymous');

    this.load.image('bg1', 'assets/bg1.jpg');
    this.load.image('green', 'assets/green.jpg');
    this.load.image('blue', 'assets/blue.jpg');
    this.load.image('boss', 'assets/boss.jpg');
    this.load.image('shop', 'assets/shop.jpg');
    this.load.image('treasure', 'assets/treasure.jpg');
    this.load.image('shrine', 'assets/shrine.jpg');
    this.load.image('grid', 'assets/grid.png');
    this.load.image('dialog9patch1', 'assets/dialog9patch1.png');
    this.load.image('dialog9patch2', 'assets/dialog9patch2.png');
    this.load.image('dialog9patch3', 'assets/dialog9patch3.png');

    this.load.atlas('atlas1', 'assets/sprites-0.png', 'assets/sprites-0.json');
    this.load.multiatlas({
      key: 'enemyAtlas1',
      atlasURL: 'assets/enemies-multi.json',
      path: 'assets/',
    });
    this.load.multiatlas({
      key: 'effectAtlas1',
      atlasURL: 'assets/effects-multi.json',
      path: 'assets/',
    });

    this.load.audio('gp', ['assets/audio/gp.ogg', 'assets/audio/gp.m4a']);
    this.load.audio('sword1', [
      'assets/audio/sword1.ogg',
      'assets/audio/sword1.m4a',
    ]);
    this.load.audio('back', ['assets/audio/back.ogg', 'assets/audio/back.m4a']);
    this.load.audio('mystery', [
      'assets/audio/mystery.ogg',
      'assets/audio/mystery.m4a',
    ]);
    this.load.audio('click', [
      'assets/audio/click.ogg',
      'assets/audio/click.m4a',
    ]);
    this.load.audio('uiSelect', [
      'assets/audio/UI_Button_Select.ogg',
      'assets/audio/UI_Button_Select.m4a',
    ]);
    this.load.audio('uiError', [
      'assets/audio/UI_Error.ogg',
      'assets/audio/UI_Error.m4a',
    ]);
    this.load.audio('uiTurn', [
      'assets/audio/UI_Player_Turn.ogg',
      'assets/audio/UI_Player_Turn.m4a',
    ]);
    this.load.audio('bgm1', ['assets/audio/bgm1.ogg', 'assets/audio/bgm1.m4a']);
    this.load.audio('bgm2', ['assets/audio/bgm2.ogg', 'assets/audio/bgm2.m4a']);

    this.load.bitmapFont(
      'sakkal',
      'assets/fonts/sakkal_0.png',
      'assets/fonts/sakkal.xml'
    );

    this.load.bitmapFont(
      'sakkalicon',
      'assets/fonts/sakkalicon_0.png',
      'assets/fonts/sakkalicon.xml'
    );

    this.load.bitmapFont(
      'sakkalbold',
      'assets/fonts/sakkalbold_0.png',
      'assets/fonts/sakkalbold.xml'
    );

    this.load.json('names', 'assets/data/names.json');
    this.load.json('heroes', 'assets/data/heroes.json');
    this.load.json('skills', 'assets/data/skills.json');
    this.load.json('items', 'assets/data/items.json');
    this.load.json('enemies', 'assets/data/enemies.json');
    this.load.json('weapons', 'assets/data/weapons.json');
    this.load.json('text', 'assets/data/text.json');

    this.load.json('particles', 'assets/data/effects.json');

    this.load.plugin(
      'rexninepatchplugin',
      'plugins/rexninepatchplugin.min.js',
      true
    );

    this.load.on('progress', this.onLoadProgress, this);
    this.load.on('complete', this.onLoadComplete, this);
    this.createProgressBar();

    this.icon = this.add.image(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      'logo'
    );
    this.icon.setOrigin(0.5, 0.5);
  }

  create() {
    Utils.GlobalSettings.bgm = this.sound.add('bgm1');
    Utils.GlobalSettings.battleBgm = this.sound.add('bgm2');
    Utils.GlobalSettings.selectSound = this.sound.add('sword1');
    Utils.GlobalSettings.backSound = this.sound.add('back');
    Utils.GlobalSettings.text = this.cache.json.get('text');

    var list = this.cache.json.get('skills');
    Utils.GlobalSettings.skillsList = list.skills;
    Utils.GlobalSettings.defenseList = list.defense;
    Utils.GlobalSettings.itemsList = this.cache.json.get('items').items;
    Utils.GlobalSettings.weaponsList = this.cache.json.get('weapons').weapons;
    Utils.GlobalSettings.heroList = this.cache.json.get('heroes');

    this.message = this.add.dynamicBitmapText(
      0,
      Utils.GlobalSettings.height - 350,
      'sakkalbold',
      'Tap To Play!',
      50
    );
    this.message.setOrigin(0, 0);

    this.messageWaveIdx = 0;
    this.messageWave = 0;
    this.messageDelay = 0;
    this.messageColor = [
      0xffa8a8,
      0xffacec,
      0xffa8d3,
      0xfea9f3,
      0xefa9fe,
      0xe7a9fe,
      0xc4abfe,
    ];
    this.messageColorOffset = 0;

    this.message.setDisplayCallback((data) => {
      data.color = this.messageColor[
        (this.messageColorOffset + this.messageWaveIdx) %
          this.messageColor.length
      ];
      this.messageWaveIdx =
        (this.messageWaveIdx + 1) % this.messageColor.length;
      data.y = Math.cos(this.messageWave + this.messageWaveIdx) * 10;
      this.messageWave += 0.01;

      return data;
    });
    this.message.x = Utils.GlobalSettings.width / 2 - this.message.width / 2;

    this.input.on('pointerdown', () => {
      this.transitionOut();
      //this.scale.toggleFullscreen();
    });

    this.events.on('shutdown', this.shutdown, this);
  }

  transitionOut(target = 'main') {
    var tween = this.tweens.add({
      targets: [this.icon, this.message],
      alpha: 0,
      ease: 'Power1',
      duration: 1000,
      onComplete: () => {
        this.scene.start(target, { from: 'preloader' });
      },
    });
  }

  // extend:

  createProgressBar() {
    var main = this.cameras.main;
    this.progressBarRectangle = new Phaser.Geom.Rectangle(
      0,
      0,
      0.75 * main.width,
      20
    );
    Phaser.Geom.Rectangle.CenterOn(
      this.progressBarRectangle,
      0.5 * main.width,
      main.height - 150
    );
    this.progressBar = this.add.graphics();
  }

  onLoadComplete(loader) {
    //console.log('onLoadComplete', loader);
    this.progressBar.destroy();
  }

  onLoadProgress(progress) {
    var rect = this.progressBarRectangle;
    var color = 0xca59ff;
    this.progressBar
      .clear()
      .fillStyle(0x222222)
      .fillRect(rect.x, rect.y, rect.width, rect.height)
      .fillStyle(color)
      .fillRect(rect.x, rect.y, progress * rect.width, rect.height);
    //console.log('progress', progress);
  }

  update() {
    this.messageWaveIdx = 0;

    if (this.messageDelay++ === 6) {
      this.messageColorOffset =
        (this.messageColorOffset + 1) % this.messageColor.length;
      this.messageDelay = 0;
    }
  }

  shutdown() {
    if (this.icon) {
      this.icon.destroy();
      this.icon = null;
    }

    if (this.progressBarRectangle) {
      this.progressBarRectangle = null;
    }

    this.events.off('shutdown', this.shutdown, this);

    console.log('kill preloader');
  }
}
