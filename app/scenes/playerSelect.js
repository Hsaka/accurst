import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';

export default class PlayerSelectScene extends Phaser.Scene {
  constructor() {
    super('playerSelect');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'bg1');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.title = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      100,
      'sakkalbold',
      Utils.GlobalSettings.text.playerSelect.title,
      80
    );
    this.title.setOrigin(0.5);
    this.screenGroup.add(this.title);

    this.setupPlayerButtons();

    this.backButton = UIHelper.createNavButton(this, 50, 'back');
    this.backButton.alpha = 0;
    this.backButton.on('pointerup', () => {
      if (!Utils.SavedSettings.muted) {
        Utils.GlobalSettings.backSound.play();
      }
      this.scene.start('main', { from: 'playerSelect' });
    });

    this.tweens.add({
      targets: [this.backButton],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 500,
      delay: 0
    });

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI');
      this.scene.bringToTop('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI');
      this.scene.bringToTop('gameUI');
    }
  }

  setupPlayerButtons() {
    var buttonOffsetY = 200;

    this.player1Button = UIHelper.createMenuButton(
      this,
      'menubutton',
      Utils.GlobalSettings.height / 2 - buttonOffsetY,
      Utils.GlobalSettings.text.playerSelect.one_player
    );
    this.player1Button.on('pointerup', () => {
      this.setupPlayerStart(1);
    });

    this.player2Button = UIHelper.createMenuButton(
      this,
      'menubutton',
      this.player1Button.y + 120,
      Utils.GlobalSettings.text.playerSelect.two_players
    );
    this.player2Button.on('pointerup', () => {
      this.setupPlayerStart(2);
    });

    this.player3Button = UIHelper.createMenuButton(
      this,
      'menubutton',
      this.player2Button.y + 120,
      Utils.GlobalSettings.text.playerSelect.three_players
    );
    this.player3Button.on('pointerup', () => {
      this.setupPlayerStart(3);
    });

    this.player4Button = UIHelper.createMenuButton(
      this,
      'menubutton',
      this.player3Button.y + 120,
      Utils.GlobalSettings.text.playerSelect.four_players
    );
    this.player4Button.on('pointerup', () => {
      this.setupPlayerStart(4);
    });

    this.tweens.add({
      targets: [this.player1Button, this.player1Button.buttonTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 500,
      delay: 0
    });

    this.tweens.add({
      targets: [this.player2Button, this.player2Button.buttonTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 500,
      delay: 200
    });

    this.tweens.add({
      targets: [this.player3Button, this.player3Button.buttonTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 500,
      delay: 400
    });

    this.tweens.add({
      targets: [this.player4Button, this.player4Button.buttonTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 500,
      delay: 600
    });
  }

  setupPlayerStart(numPlayers) {
    if (!Utils.SavedSettings.muted) {
      Utils.GlobalSettings.selectSound.play();
    }

    Utils.GlobalSettings.gameState.numPlayers = numPlayers;
    Utils.GlobalSettings.gameState.players = [];
    Utils.GlobalSettings.gameState.currentPlayerSetup = 0;
    Utils.GlobalSettings.gameState.floor = 1;
    Utils.saveGameState();

    this.scene.start('setupPlayer', { from: 'playerSelect' });
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.title) {
      this.title.destroy();
      this.title = null;
    }

    if (this.player1Button && this.player1Button.buttonTxt) {
      this.player1Button.buttonTxt.destroy();
      this.player1Button.buttonTxt = null;
      this.player1Button.destroy();
      this.player1Button = null;
    }

    if (this.player2Button && this.player2Button.buttonTxt) {
      this.player2Button.buttonTxt.destroy();
      this.player2Button.buttonTxt = null;
      this.player2Button.destroy();
      this.player2Button = null;
    }

    if (this.player3Button && this.player3Button.buttonTxt) {
      this.player3Button.buttonTxt.destroy();
      this.player3Button.buttonTxt = null;
      this.player3Button.destroy();
      this.player3Button = null;
    }

    if (this.player4Button && this.player4Button.buttonTxt) {
      this.player4Button.buttonTxt.destroy();
      this.player4Button.buttonTxt = null;
      this.player4Button.destroy();
      this.player4Button = null;
    }

    if (this.backButton) {
      this.backButton.destroy();
      this.backButton = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill player select');
  }
}
