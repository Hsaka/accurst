import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import BattleUI from 'utils/battle/battleUI';
import EnemyManager from 'utils/battle/enemyManager';
import EffectManager from 'utils/battle/effectManager';
import ActionManager from 'utils/battle/actionManager';

export default class BattleScene extends Phaser.Scene {
  constructor() {
    super('battle');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    Utils.GlobalSettings.bgm.pause();

    if (!Utils.GlobalSettings.battleBgm.isPlaying) {
      Utils.GlobalSettings.battleBgm.setVolume(0.2);
      Utils.GlobalSettings.battleBgm.setLoop(true);
      Utils.GlobalSettings.battleBgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.battleBgm.pause();
    }

    this.clickSnd = this.sound.add('click');
    this.turnSnd = this.sound.add('uiTurn');
    this.mysterySnd = this.sound.add('mystery');

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      Utils.GlobalSettings.gameState.encounter.type
    );
    this.bg.setOrigin(0.5);
    this.screenGroup.add(this.bg);

    if (!Utils.SavedSettings.muted) {
      this.mysterySnd.play();
    }

    if (Utils.GlobalSettings.gameState.encounter.type === 'green') {
      this.bg.setScale(0);
      this.bg.alpha = 0;
      this.tweens.add({
        targets: [this.bg],
        alpha: { from: 0, to: 1 },
        scale: { from: 0, to: 1 },
        ease: 'Back.easeOut',
        duration: 1000,
        delay: 0,
        onComplete: () => {
          if (!Utils.SavedSettings.muted) {
            this.turnSnd.play();
          }
        },
      });
    } else if (Utils.GlobalSettings.gameState.encounter.type === 'blue') {
      this.bg.setScale(0, 1);
      this.bg.alpha = 0;
      this.tweens.add({
        targets: [this.bg],
        alpha: { from: 0, to: 1 },
        scaleX: { from: 0, to: 1 },
        ease: 'Bounce.easeOut',
        duration: 1000,
        delay: 0,
        onComplete: () => {
          if (!Utils.SavedSettings.muted) {
            this.turnSnd.play();
          }
        },
      });
    } else if (Utils.GlobalSettings.gameState.encounter.type === 'boss') {
      this.bg.setScale(0, 0);
      this.bg.alpha = 0;
      this.bg.angle = 360 * 3;
      this.tweens.add({
        targets: [this.bg],
        alpha: { from: 0, to: 1 },
        scale: { from: 0, to: 1 },
        angle: { from: 360, to: 0 },
        ease: 'Back.easeOut',
        duration: 1000,
        delay: 0,
        onComplete: () => {
          if (!Utils.SavedSettings.muted) {
            this.turnSnd.play();
          }
        },
      });
    }

    this.currentPlayer =
      Utils.GlobalSettings.gameState.players[
        Utils.GlobalSettings.gameState.encounter.combatTurn
      ];

    this.battleUI = new BattleUI(this);
    this.battleUI.setupPanels();

    this.enemyManager = new EnemyManager(this);

    this.battleUI.timelinePanel.setupTimeline();

    this.bgOverlay = this.add.image(0, 0, 'atlas1', 'overlay');
    this.bgOverlay.setOrigin(0);
    this.bgOverlay.setDisplaySize(
      Utils.GlobalSettings.width,
      Utils.GlobalSettings.height
    );
    this.bgOverlay.alpha = 0;
    this.bgOverlay.visible = false;
    this.bgOverlay.setTint(0x000000);
    this.bgOverlay.setInteractive();

    this.battleUI.actionPanel.setupActionConfirmation();
    this.battleUI.statsPanel.setupStatsPanel();
    this.battleUI.rewardPanel.setupRewardPanel();
    this.battleUI.dropItemPanel.setupDropItemPanel(this.currentPlayer);
    this.battleUI.deathPanel.setupDeathPanel();

    this.effectManager = new EffectManager(this);

    this.setupEffects();
    if (Utils.GlobalSettings.gameState.encounter.combatRound === 1) {
      this.time.delayedCall(
        2000,
        () => {
          this.effectManager.doEffect(0, {
            moveToX: this.enemyManager.enemyIcon.x,
            moveToY: this.enemyManager.enemyIcon.y,
          });
        },
        [],
        this
      );
    }

    this.actionManager = new ActionManager(this);

    this.battleUI.showInstructions(
      Utils.GlobalSettings.text.battle.instructions_start,
      5000
    );

    this.toast = UIHelper.createToast(this);

    if (this.currentPlayer.health <= 0) {
      this.battleUI.deathPanel.showDeathPanel(2000, true);
    }

    this.maxParticles = 0;
    this.debugText = this.add.text(10, 10, '-- Particles', {
      font: 'bold 16px Arial',
      fill: '#ffffff',
    });
    this.debugText.visible = false;

    this.closeButton = UIHelper.createMenuButton(
      this,
      'menubutton',
      Utils.GlobalSettings.height - 200,
      Utils.GlobalSettings.text.battle.close,
      false
    );
    this.closeButton.visible = false;
    this.closeButton.alpha = 0;
    this.closeButton.on('pointerup', () => {
      Utils.GlobalSettings.gameState.rooms[
        Utils.GlobalSettings.gameState.lastRoomId
      ].isCompleted = true;
      Utils.GlobalSettings.gameState.encounter = {};
      Utils.GlobalSettings.gameState.phase = 'exploration';

      Utils.GlobalSettings.gameState.turn++;
      if (
        Utils.GlobalSettings.gameState.turn >=
        Utils.GlobalSettings.gameState.numPlayers
      ) {
        Utils.GlobalSettings.gameState.turn = 0;
      }
      Utils.saveGameState();

      var goToLevelUpScene = false;
      var playerIndexToLevelUp = -1;
      for (var i = 0; i < Utils.GlobalSettings.gameState.numPlayers; i++) {
        if (Utils.GlobalSettings.gameState.players[i].levelUp) {
          goToLevelUpScene = true;
          playerIndexToLevelUp = i;
          break;
        }
      }

      if (goToLevelUpScene) {
        this.scene.start('levelUp', {
          from: 'battle',
          playerIndex: playerIndexToLevelUp,
        });
      } else {
        this.scene.start('mapExploration', { from: 'battle' });
      }
    });

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI');
      this.scene.bringToTop('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI');
      this.scene.bringToTop('gameUI');
    }
  }

  exitRoom() {
    this.bg.setScale(1);
    this.tweens.add({
      targets: [this.bg],
      scaleX: { from: 1, to: 0 },
      ease: 'Quad.easeOut',
      duration: 1000,
      delay: 500,
      onComplete: () => {
        this.bg.setTexture('bg1');
      },
    });

    this.tweens.add({
      targets: [this.bg],
      scaleX: { from: 0, to: 1 },
      ease: 'Quad.easeIn',
      duration: 1000,
      delay: 1500,
      onComplete: () => {
        this.tweens.add({
          targets: [this.bg],
          alpha: { from: 1, to: 0.5 },
          ease: 'Sine.easeInOut',
          duration: 2500,
          delay: 1000,
          repeat: -1,
          yoyo: true,
        });
      },
    });

    this.battleUI.instructionDialog.x = Utils.GlobalSettings.width / 2;
    this.battleUI.instructionDialog.y = Utils.GlobalSettings.height / 2;
    this.battleUI.instructionTxt.x =
      this.battleUI.instructionDialog.x -
      this.battleUI.instructionDialog.width / 2 +
      10;
    this.battleUI.instructionTxt.y =
      this.battleUI.instructionDialog.y -
      this.battleUI.instructionDialog.height / 2 +
      10;

    this.battleUI.showInstructions(
      Utils.GlobalSettings.text.battle.encounter_complete,
      2500
    );

    this.closeButton.visible = true;
    this.closeButton.alpha = 0;
    this.closeButton.buttonTxt.visible = true;
    this.closeButton.buttonTxt.alpha = 0;
    this.tweens.add({
      targets: [this.closeButton, this.closeButton.buttonTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 1000,
      delay: 2500,
    });
  }

  setupEffects() {
    var effects = [0, 5, 6, 7, 8, 9, 10];
    if (this.enemyManager.currentEnemy) {
      for (var i = 0; i < this.enemyManager.currentEnemy.skills.length; i++) {
        var skill =
          Utils.GlobalSettings.skillsList[
            this.enemyManager.currentEnemy.skills[i]
          ];

        effects.push(skill.effect);
      }
    }

    if (this.currentPlayer) {
      for (var i = 0; i < this.currentPlayer.skills.length; i++) {
        var skill =
          Utils.GlobalSettings.skillsList[this.currentPlayer.skills[i]];
        effects.push(skill.effect);
      }

      for (var i = 0; i < this.currentPlayer.items.length; i++) {
        var item = Utils.GlobalSettings.itemsList[this.currentPlayer.items[i]];
        effects.push(item.effect);
      }
    }

    for (var key in Utils.GlobalSettings.defenseList) {
      var defense = Utils.GlobalSettings.defenseList[key];
      effects.push(defense.effect);
    }

    this.effectManager.loadEffects(effects);
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.battleBgm.pause();
      } else {
        Utils.GlobalSettings.battleBgm.resume();
      }
    } else if (key === 'home' && data) {
      this.registry.set('showHome', false);
      this.scene.start('main', { from: 'deckSetup' });
    }
  }

  update(time, delta) {
    if (this.effectManager.particles.emitters.list) {
      var numParticles = 0;
      for (
        var i = 0;
        i < this.effectManager.particles.emitters.list.length;
        i++
      ) {
        numParticles += this.effectManager.particles.emitters.list[i].alive
          .length;
      }

      if (numParticles > this.maxParticles) {
        this.maxParticles = numParticles;
      }
      this.debugText.setText(
        numParticles + ' Particles\nMax: ' + this.maxParticles
      );
    }
  }

  shutdown() {
    Utils.GlobalSettings.battleBgm.pause();
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.clickSnd) {
      this.clickSnd.destroy();
      this.clickSnd = null;
    }

    if (this.turnSnd) {
      this.turnSnd.destroy();
      this.turnSnd = null;
    }

    if (this.mysterySnd) {
      this.mysterySnd.destroy();
      this.mysterySnd = null;
    }

    this.currentPlayer = null;

    if (this.battleUI) {
      this.battleUI.destroy();
      this.battleUI = null;
    }

    if (this.enemyManager) {
      this.enemyManager.destroy();
      this.enemyManager = null;
    }

    if (this.bgOverlay) {
      this.bgOverlay.destroy();
      this.bgOverlay = null;
    }

    if (this.toast) {
      this.toast.destroy();
      this.toast = null;
    }

    if (this.effectManager) {
      this.effectManager.destroy();
      this.effectManager = null;
    }

    if (this.actionManager) {
      this.actionManager.destroy();
      this.actionManager = null;
    }

    if (this.closeButton) {
      this.closeButton.destroy();
      this.closeButton = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill battle');
  }
}
