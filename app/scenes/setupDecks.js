import Utils from 'utils/utils';
import NameGenerator from 'utils/nameGenerator';
import UIHelper from 'utils/uiHelper';

export default class DeckSetupScene extends Phaser.Scene {
  constructor() {
    super('deckSetup');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    this.clickSnd = this.sound.add('click');

    var gen = new NameGenerator();
    var names = this.cache.json.get('names');
    gen.setNameSet(names, 'fantasy');
    Utils.GlobalSettings.gameState.floorName = gen.generate_name('fantasy');
    Utils.saveGameState();
    names = null;

    this.screenGroup = this.add.container(0, 0);

    var endsWithS = UIHelper.endsWithS(
      Utils.GlobalSettings.gameState.floorName
    );

    this.bg = this.add.image(0, 0, 'bg1');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.title = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      80,
      'sakkalbold',
      Utils.GlobalSettings.text.deckSetup.title +
        ' ' +
        Utils.GlobalSettings.gameState.floor +
        '...',
      80
    );
    this.title.setOrigin(0.5);
    this.screenGroup.add(this.title);

    this.subtitle = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      120,
      'sakkalbold',
      endsWithS
        ? Utils.GlobalSettings.gameState.floorName +
            "' " +
            Utils.GlobalSettings.text.deckSetup.floor
        : Utils.GlobalSettings.gameState.floorName +
            "'s " +
            Utils.GlobalSettings.text.deckSetup.floor,
      35
    );
    this.subtitle.setOrigin(0.5);
    this.screenGroup.add(this.subtitle);

    this.setupTutorial();

    this.goButton = UIHelper.createNavButton(
      this,
      Utils.GlobalSettings.width - 50,
      'forward'
    );
    this.goButton.alpha = 0;
    this.goButton.on('pointerup', () => {
      if (!Utils.SavedSettings.muted) {
        Utils.GlobalSettings.selectSound.play();
      }

      this.scene.start('mapExploration', { from: 'deckSetup' });
    });

    this.tweens.add({
      targets: [this.goButton],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 500,
      delay: 500
    });

    this.time.delayedCall(
      250,
      () => {
        this.tweens.add({
          targets: [this.bg],
          alpha: { from: 1, to: 0.7 },
          ease: 'Sine.easeInOut',
          duration: 2500,
          delay: 1000,
          repeat: -1,
          yoyo: true
        });
      },
      [],
      this
    );

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI');
      this.scene.bringToTop('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI');
      this.scene.bringToTop('gameUI');
    }
    this.registry.set('showParty', true);
  }

  setupTutorial() {
    this.deckSetupLabel = this.add.bitmapText(
      30,
      200,
      'sakkalbold',
      Utils.GlobalSettings.text.deckSetup.deck_setup,
      45
    );
    this.deckSetupLabel.setOrigin(0);

    this.setupTutorialStep1();
    this.setupTutorialStep2();
    this.setupTutorialStep3();
    this.setupTutorialStep4();
  }

  setupTutorialStep1() {
    this.tutorial1Background = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.tutorial1Dialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2,
        y: this.deckSetupLabel.y + 150,
        width: 600,
        height: 200
      })
      .addBackground(this.tutorial1Background)
      .layout();

    this.tutorial1Txt = this.add.bitmapText(
      30,
      this.tutorial1Dialog.y - 90,
      'sakkal',
      Utils.GlobalSettings.text.deckSetup.tutorial_1,
      40
    );

    this.roomback = this.add.image(
      Utils.GlobalSettings.width / 2 - 100,
      this.tutorial1Dialog.y + 10,
      'atlas1',
      'roomback'
    );
    this.roomback.setScale(0.4);

    this.roombacktxt = this.add.bitmapText(
      this.roomback.x,
      this.roomback.y + 70,
      'sakkalbold',
      Utils.GlobalSettings.text.deckSetup.room_cards,
      25
    );
    this.roombacktxt.setOrigin(0.5);

    this.combatback = this.add.image(
      Utils.GlobalSettings.width / 2 + 100,
      this.tutorial1Dialog.y + 10,
      'atlas1',
      'combatback'
    );
    this.combatback.setScale(0.4);

    this.combatbacktxt = this.add.bitmapText(
      this.combatback.x,
      this.combatback.y + 70,
      'sakkalbold',
      Utils.GlobalSettings.text.deckSetup.combat_cards,
      25
    );
    this.combatbacktxt.setOrigin(0.5);

    this.tutorial1Dialog.alpha = 0;
    this.tutorial1Txt.alpha = 0;
    this.roomback.alpha = 0;
    this.roombacktxt.alpha = 0;
    this.combatback.alpha = 0;
    this.combatbacktxt.alpha = 0;

    this.tweens.add({
      targets: [
        this.tutorial1Dialog,
        this.tutorial1Txt,
        this.roomback,
        this.roombacktxt,
        this.combatback,
        this.combatbacktxt
      ],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 1000,
      delay: 0
    });
  }

  setupTutorialStep2() {
    this.tutorial2Background = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.tutorial2Dialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2,
        y: this.tutorial1Dialog.y + this.tutorial1Dialog.height + 30,
        width: 600,
        height: 200
      })
      .addBackground(this.tutorial2Background)
      .layout();

    this.tutorial2Txt = this.add.bitmapText(
      30,
      this.tutorial2Dialog.y - 90,
      'sakkal',
      Utils.GlobalSettings.text.deckSetup.tutorial_2,
      40
    );

    this.bossroom = this.add.image(
      Utils.GlobalSettings.width / 2,
      this.tutorial2Dialog.y + 10,
      'atlas1',
      'bossroom_small'
    );
    this.bossroom.setScale(0.5);

    this.bossroomtxt = this.add.bitmapText(
      this.bossroom.x,
      this.bossroom.y + 70,
      'sakkalbold',
      Utils.GlobalSettings.text.deckSetup.boss_card,
      25
    );
    this.bossroomtxt.setOrigin(0.5);

    this.tutorial2Dialog.alpha = 0;
    this.tutorial2Txt.alpha = 0;
    this.bossroom.alpha = 0;
    this.bossroomtxt.alpha = 0;

    this.tweens.add({
      targets: [
        this.tutorial2Dialog,
        this.tutorial2Txt,
        this.bossroom,
        this.bossroomtxt
      ],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 1000,
      delay: 500
    });
  }

  setupTutorialStep3() {
    this.tutorial3Background = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.tutorial3Dialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2,
        y: this.tutorial2Dialog.y + this.tutorial2Dialog.height - 20,
        width: 600,
        height: 100
      })
      .addBackground(this.tutorial3Background)
      .layout();

    this.tutorial3Txt = this.add.bitmapText(
      30,
      this.tutorial3Dialog.y - 40,
      'sakkal',
      Utils.GlobalSettings.text.deckSetup.tutorial_3,
      40
    );

    this.tutorial3Dialog.alpha = 0;
    this.tutorial3Txt.alpha = 0;

    this.tweens.add({
      targets: [this.tutorial3Dialog, this.tutorial3Txt],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 1000,
      delay: 1000
    });
  }

  setupTutorialStep4() {
    this.tutorial4Background = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.tutorial4Dialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2 - 100,
        y: this.tutorial3Dialog.y + this.tutorial3Dialog.height + 5,
        width: 400,
        height: 50
      })
      .addBackground(this.tutorial4Background)
      .layout();

    this.tutorial4Txt = this.add.bitmapText(
      30,
      this.tutorial4Dialog.y - 20,
      'sakkal',
      Utils.GlobalSettings.text.deckSetup.tutorial_4,
      40
    );

    this.tutorial4Dialog.alpha = 0;
    this.tutorial4Txt.alpha = 0;

    this.tweens.add({
      targets: [this.tutorial4Dialog, this.tutorial4Txt],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 1000,
      delay: 1500
    });
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    } else if (key === 'home' && data) {
      this.registry.set('showHome', false);
      this.registry.set('showParty', false);
      this.scene.start('main', { from: 'deckSetup' });
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.title) {
      this.title.destroy();
      this.title = null;
    }

    if (this.subtitle) {
      this.subtitle.destroy();
      this.subtitle = null;
    }

    if (this.goButton) {
      this.goButton.destroy();
      this.goButton = null;
    }

    if (this.deckSetupLabel) {
      this.deckSetupLabel.destroy();
      this.deckSetupLabel = null;
    }

    if (this.tutorial1Background) {
      this.tutorial1Background.destroy();
      this.tutorial1Background = null;
    }

    if (this.tutorial1Dialog) {
      this.tutorial1Dialog.destroy();
      this.tutorial1Dialog = null;
    }

    if (this.tutorial1Txt) {
      this.tutorial1Txt.destroy();
      this.tutorial1Txt = null;
    }

    if (this.roomback) {
      this.roomback.destroy();
      this.roomback = null;
    }

    if (this.roombacktxt) {
      this.roombacktxt.destroy();
      this.roombacktxt = null;
    }

    if (this.combatback) {
      this.combatback.destroy();
      this.combatback = null;
    }

    if (this.combatbacktxt) {
      this.combatbacktxt.destroy();
      this.combatbacktxt = null;
    }

    if (this.tutorial2Background) {
      this.tutorial2Background.destroy();
      this.tutorial2Background = null;
    }

    if (this.tutorial2Dialog) {
      this.tutorial2Dialog.destroy();
      this.tutorial2Dialog = null;
    }

    if (this.tutorial2Txt) {
      this.tutorial2Txt.destroy();
      this.tutorial2Txt = null;
    }

    if (this.bossroom) {
      this.bossroom.destroy();
      this.bossroom = null;
    }

    if (this.bossroomtxt) {
      this.bossroomtxt.destroy();
      this.bossroomtxt = null;
    }

    if (this.tutorial3Background) {
      this.tutorial3Background.destroy();
      this.tutorial3Background = null;
    }

    if (this.tutorial3Dialog) {
      this.tutorial3Dialog.destroy();
      this.tutorial3Dialog = null;
    }

    if (this.tutorial3Txt) {
      this.tutorial3Txt.destroy();
      this.tutorial3Txt = null;
    }

    if (this.tutorial4Background) {
      this.tutorial4Background.destroy();
      this.tutorial4Background = null;
    }

    if (this.tutorial4Dialog) {
      this.tutorial4Dialog.destroy();
      this.tutorial4Dialog = null;
    }

    if (this.tutorial4Txt) {
      this.tutorial4Txt.destroy();
      this.tutorial4Txt = null;
    }

    if (this.clickSnd) {
      this.clickSnd.destroy();
      this.clickSnd = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill player select');
  }
}
