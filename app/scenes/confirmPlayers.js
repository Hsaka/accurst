import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';

export default class ConfirmPlayersScene extends Phaser.Scene {
  constructor() {
    super('confirmPlayers');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    Utils.GlobalSettings.gameState.timestamp = moment();
    Utils.GlobalSettings.gameState.gameStarted = true;
    Utils.GlobalSettings.gameState.turn = 0;
    Utils.GlobalSettings.gameState.rooms = {};
    Utils.GlobalSettings.gameState.encounter = {};
    Utils.GlobalSettings.gameState.phase = 'exploration';
    Utils.GlobalSettings.gameState.mysterySpots = [
      {
        x: Utils.GlobalSettings.rnd.pick([
          Utils.GlobalSettings.rnd.integerInRange(-3, -1),
          Utils.GlobalSettings.rnd.integerInRange(1, 3)
        ]),
        y: Utils.GlobalSettings.rnd.pick([
          Utils.GlobalSettings.rnd.integerInRange(-5, -1),
          Utils.GlobalSettings.rnd.integerInRange(1, 5)
        ])
      },
      {
        x: Utils.GlobalSettings.rnd.pick([
          Utils.GlobalSettings.rnd.integerInRange(-3, -1),
          Utils.GlobalSettings.rnd.integerInRange(1, 3)
        ]),
        y: Utils.GlobalSettings.rnd.pick([
          Utils.GlobalSettings.rnd.integerInRange(-5, -1),
          Utils.GlobalSettings.rnd.integerInRange(1, 5)
        ])
      },
      {
        x: Utils.GlobalSettings.rnd.pick([
          Utils.GlobalSettings.rnd.integerInRange(-3, -1),
          Utils.GlobalSettings.rnd.integerInRange(1, 3)
        ]),
        y: Utils.GlobalSettings.rnd.pick([
          Utils.GlobalSettings.rnd.integerInRange(-5, -1),
          Utils.GlobalSettings.rnd.integerInRange(1, 5)
        ])
      }
    ];

    Utils.saveGameState();

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'bg1');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.title = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      100,
      'sakkalbold',
      Utils.GlobalSettings.text.confirmPlayers.title,
      80
    );
    this.title.setOrigin(0.5);
    this.screenGroup.add(this.title);

    this.setupPlayerButtons();

    this.goButton = UIHelper.createNavButton(
      this,
      Utils.GlobalSettings.width - 50,
      'forward'
    );
    this.goButton.alpha = 0;
    this.goButton.on('pointerup', () => {
      if (!Utils.SavedSettings.muted) {
        Utils.GlobalSettings.selectSound.play();
      }

      this.scene.start('deckSetup', { from: 'confirmPlayers' });
    });

    this.tweens.add({
      targets: [this.goButton],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 500,
      delay: 0
    });

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI');
      this.scene.bringToTop('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI');
      this.scene.bringToTop('gameUI');
    }
    this.registry.set('showHome', true);
  }

  setupPlayerButtons() {
    var buttonOffsetY = 60 * (Utils.GlobalSettings.gameState.numPlayers - 1);

    this.player1Button = UIHelper.createMenuButton(
      this,
      'menubutton',
      Utils.GlobalSettings.height / 2 - buttonOffsetY,
      '1 Player'
    );
    this.player1Button.icon = this.add.image(
      this.player1Button.x - 170,
      this.player1Button.y,
      'atlas1',
      'hero_knight'
    );
    this.player1Button.icon.setScale(0.3);
    this.player1Button.icon.visible = false;
    this.player1Button.visible = false;
    this.player1Button.buttonTxt.visible = false;
    this.player1Button.on('pointerup', () => {
      this.editPlayer(0);
    });

    this.player2Button = UIHelper.createMenuButton(
      this,
      'menubutton',
      this.player1Button.y + 120,
      '2 Players'
    );
    this.player2Button.icon = this.add.image(
      this.player2Button.x - 170,
      this.player2Button.y,
      'atlas1',
      'hero_knight'
    );
    this.player2Button.icon.setScale(0.3);
    this.player2Button.icon.visible = false;
    this.player2Button.visible = false;
    this.player2Button.buttonTxt.visible = false;
    this.player2Button.on('pointerup', () => {
      this.editPlayer(1);
    });

    this.player3Button = UIHelper.createMenuButton(
      this,
      'menubutton',
      this.player2Button.y + 120,
      '3 Players'
    );
    this.player3Button.icon = this.add.image(
      this.player3Button.x - 170,
      this.player3Button.y,
      'atlas1',
      'hero_knight'
    );
    this.player3Button.icon.setScale(0.3);
    this.player3Button.icon.visible = false;
    this.player3Button.visible = false;
    this.player3Button.buttonTxt.visible = false;
    this.player3Button.on('pointerup', () => {
      this.editPlayer(2);
    });

    this.player4Button = UIHelper.createMenuButton(
      this,
      'menubutton',
      this.player3Button.y + 120,
      '4 Players'
    );
    this.player4Button.icon = this.add.image(
      this.player4Button.x - 170,
      this.player4Button.y,
      'atlas1',
      'hero_knight'
    );
    this.player4Button.icon.setScale(0.3);
    this.player4Button.icon.visible = false;
    this.player4Button.visible = false;
    this.player4Button.buttonTxt.visible = false;
    this.player4Button.on('pointerup', () => {
      this.editPlayer(3);
    });

    if (Utils.GlobalSettings.gameState.numPlayers >= 1) {
      this.player1Button.visible = true;
      this.player1Button.buttonTxt.visible = true;
      this.player1Button.buttonTxt.setText(
        Utils.GlobalSettings.gameState.players[0].name
      );
      this.player1Button.icon.visible = true;
      this.player1Button.icon.setFrame(
        Utils.GlobalSettings.heroList[
          Utils.GlobalSettings.gameState.players[0].classType
        ].image
      );
      this.tweens.add({
        targets: [this.player1Button, this.player1Button.buttonTxt],
        alpha: { from: 0, to: 1 },
        ease: 'Sine.easeInOut',
        duration: 500,
        delay: 0
      });
    }

    if (Utils.GlobalSettings.gameState.numPlayers >= 2) {
      this.player2Button.visible = true;
      this.player2Button.buttonTxt.visible = true;
      this.player2Button.buttonTxt.setText(
        Utils.GlobalSettings.gameState.players[1].name
      );
      this.player2Button.icon.visible = true;
      this.player2Button.icon.setFrame(
        Utils.GlobalSettings.heroList[
          Utils.GlobalSettings.gameState.players[1].classType
        ].image
      );
      this.tweens.add({
        targets: [this.player2Button, this.player2Button.buttonTxt],
        alpha: { from: 0, to: 1 },
        ease: 'Sine.easeInOut',
        duration: 500,
        delay: 0
      });
    }

    if (Utils.GlobalSettings.gameState.numPlayers >= 3) {
      this.player3Button.visible = true;
      this.player3Button.buttonTxt.visible = true;
      this.player3Button.buttonTxt.setText(
        Utils.GlobalSettings.gameState.players[2].name
      );
      this.player3Button.icon.visible = true;
      this.player3Button.icon.setFrame(
        Utils.GlobalSettings.heroList[
          Utils.GlobalSettings.gameState.players[2].classType
        ].image
      );
      this.tweens.add({
        targets: [this.player3Button, this.player3Button.buttonTxt],
        alpha: { from: 0, to: 1 },
        ease: 'Sine.easeInOut',
        duration: 500,
        delay: 0
      });
    }

    if (Utils.GlobalSettings.gameState.numPlayers >= 4) {
      this.player4Button.visible = true;
      this.player4Button.buttonTxt.visible = true;
      this.player4Button.buttonTxt.setText(
        Utils.GlobalSettings.gameState.players[3].name
      );
      this.player4Button.icon.visible = true;
      this.player4Button.icon.setFrame(
        Utils.GlobalSettings.heroList[
          Utils.GlobalSettings.gameState.players[3].classType
        ].image
      );
      this.tweens.add({
        targets: [this.player4Button, this.player4Button.buttonTxt],
        alpha: { from: 0, to: 1 },
        ease: 'Sine.easeInOut',
        duration: 500,
        delay: 0
      });
    }
  }

  editPlayer(index) {
    if (!Utils.SavedSettings.muted) {
      Utils.GlobalSettings.backSound.play();
    }

    Utils.GlobalSettings.gameState.currentPlayerSetup = index;
    Utils.saveGameState();

    this.registry.set('showHome', false);
    this.scene.start('setupPlayer', { from: 'confirmPlayers' });
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    } else if (key === 'home' && data) {
      this.registry.set('showHome', false);
      this.scene.start('main', { from: 'confirmPlayers' });
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.title) {
      this.title.destroy();
      this.title = null;
    }

    if (
      this.player1Button &&
      this.player1Button.buttonTxt &&
      this.player1Button.icon
    ) {
      this.player1Button.buttonTxt.destroy();
      this.player1Button.buttonTxt = null;
      this.player1Button.icon.destroy();
      this.player1Button.icon = null;
      this.player1Button.destroy();
      this.player1Button = null;
    }

    if (
      this.player2Button &&
      this.player2Button.buttonTxt &&
      this.player2Button.icon
    ) {
      this.player2Button.buttonTxt.destroy();
      this.player2Button.buttonTxt = null;
      this.player2Button.icon.destroy();
      this.player2Button.icon = null;
      this.player2Button.destroy();
      this.player2Button = null;
    }

    if (
      this.player3Button &&
      this.player3Button.buttonTxt &&
      this.player3Button.icon
    ) {
      this.player3Button.buttonTxt.destroy();
      this.player3Button.buttonTxt = null;
      this.player3Button.icon.destroy();
      this.player3Button.icon = null;
      this.player3Button.destroy();
      this.player3Button = null;
    }

    if (
      this.player4Button &&
      this.player4Button.buttonTxt &&
      this.player4Button.icon
    ) {
      this.player4Button.buttonTxt.destroy();
      this.player4Button.buttonTxt = null;
      this.player4Button.icon.destroy();
      this.player4Button.icon = null;
      this.player4Button.destroy();
      this.player4Button = null;
    }

    if (this.goButton) {
      this.goButton.destroy();
      this.goButton = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill confirm players');
  }
}
