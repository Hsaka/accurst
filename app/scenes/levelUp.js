import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import GameCalculations from 'utils/gameCalculations';

export default class LevelUpScene extends Phaser.Scene {
  constructor() {
    super('levelUp');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    var playerIndex = this.initData.playerIndex;
    this.player = Utils.GlobalSettings.gameState.players[playerIndex];
    this.player.levelUp = false;

    this.clickSnd = this.sound.add('click');

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'bg1');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.message = this.add.dynamicBitmapText(
      0,
      100,
      'sakkalbold',
      'LEVEL  UP!',
      120
    );
    this.message.setOrigin(0, 0);

    this.messageWaveIdx = 0;
    this.messageWave = 0;
    this.messageDelay = 0;
    this.messageColor = [
      0xffa8a8,
      0xffacec,
      0xffa8d3,
      0xfea9f3,
      0xefa9fe,
      0xe7a9fe,
      0xc4abfe,
    ];
    this.messageColorOffset = 0;

    this.message.setDisplayCallback((data) => {
      data.color = this.messageColor[
        (this.messageColorOffset + this.messageWaveIdx) %
          this.messageColor.length
      ];
      this.messageWaveIdx =
        (this.messageWaveIdx + 1) % this.messageColor.length;
      data.y = Math.cos(this.messageWave + this.messageWaveIdx) * 10;
      this.messageWave += 0.01;

      return data;
    });
    this.message.x = Utils.GlobalSettings.width / 2 - this.message.width / 2;

    this.cameras.main.flash(500, 255, 255, 255);

    this.playerIcon = this.add.image(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      'atlas1',
      Utils.GlobalSettings.heroList[this.player.classType].image
    );
    this.playerIcon.setScale(0);
    this.playerIcon.alpha = 0;

    this.tweens.add({
      targets: [this.playerIcon],
      alpha: { from: 0, to: 1 },
      scale: { from: 0, to: 1 },
      ease: 'Back.easeOut',
      duration: 1000,
      onComplete: () => {
        this.toggleStatsPanel(true);
      },
    });

    this.playerName = this.add.bitmapText(
      this.playerIcon.x,
      this.playerIcon.y - 170,
      'sakkalbold',
      this.player.name,
      38
    );
    this.playerName.alpha = 0;
    this.playerName.setOrigin(0.5);

    this.playerLevel = this.add.bitmapText(
      this.playerIcon.x,
      this.playerIcon.y + 170,
      'sakkalbold',
      'Level  ' + this.player.level,
      30
    );
    this.playerLevel.alpha = 0;
    this.playerLevel.setOrigin(0.5);

    this.tweens.add({
      targets: [this.playerName, this.playerLevel],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: 500,
    });

    this.particles = this.add.particles('effectAtlas1');
    this.emitter = this.particles.createEmitter({
      x: Utils.GlobalSettings.width / 2,
      y: Utils.GlobalSettings.height / 2,
      frame: 'circle_05',
      blendMode: 'SCREEN',
      scale: { start: 0.8, end: 0 },
      speed: { min: -50, max: 50 },
      tint: [
        16699747,
        16689147,
        16679247,
        16669347,
        16659447,
        16649547,
        16639647,
      ],
      quantity: 1,
    });

    this.emitter.setEmitZone({
      source: new Phaser.Geom.Circle(0, 0, 200),
      type: 'edge',
      quantity: 50,
    });

    this.setupStats();

    var hero = Utils.GlobalSettings.heroList[this.player.classType];
    var hp = GameCalculations.calculateHealthGain(
      this.player.level,
      hero.max_level_health
    );
    var mp = GameCalculations.calculateMagicGain(
      this.player.level,
      hero.max_level_magic
    );
    var pow = GameCalculations.calculatePowerGain(
      this.player.level,
      hero.max_level_power
    );
    var spd = GameCalculations.calculateSpeedGain(
      this.player.level,
      hero.max_level_speed
    );

    this.player.maxHealth += hp;
    this.player.maxMagic += mp;
    this.player.maxPower += pow;

    this.player.health = this.player.maxHealth;
    this.player.magic = this.player.maxMagic;
    this.player.power = this.player.maxPower;
    //TODO: should speed be increased per level?
    //this.player.speed = hero.speed + spd;

    this.setStats(hp, pow, mp);

    this.goButton = UIHelper.createNavButton(
      this,
      Utils.GlobalSettings.width - 50,
      'forward'
    );
    this.goButton.alpha = 0;
    this.goButton.on('pointerup', () => {
      if (!Utils.SavedSettings.muted) {
        Utils.GlobalSettings.selectSound.play();
      }

      Utils.saveGameState();

      var nextIndex = this.initData.playerIndex + 1;
      if (nextIndex >= Utils.GlobalSettings.gameState.numPlayers) {
        this.scene.start('mapExploration', { from: 'levelUp' });
      } else {
        var goToLevelUpScene = false;
        var playerIndexToLevelUp = -1;
        for (
          var i = nextIndex;
          i < Utils.GlobalSettings.gameState.numPlayers;
          i++
        ) {
          if (Utils.GlobalSettings.gameState.players[i].levelUp) {
            goToLevelUpScene = true;
            playerIndexToLevelUp = i;
            break;
          }
        }

        if (goToLevelUpScene) {
          this.scene.start('levelUp', {
            from: 'levelUp',
            playerIndex: playerIndexToLevelUp,
          });
        } else {
          this.scene.start('mapExploration', { from: 'levelUp' });
        }
      }
    });

    this.tweens.add({
      targets: [this.goButton],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 500,
      delay: 500,
    });

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI');
      this.scene.bringToTop('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI');
      this.scene.bringToTop('gameUI');
    }
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  setupStats() {
    this.statsBackground = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.statsDialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2,
        y: 800,
        width: 350,
        height: 100,
      })
      .addBackground(this.statsBackground)
      .layout();

    this.healthRune = this.add.image(
      this.statsDialog.x - 130,
      this.statsDialog.y - 5,
      'atlas1',
      'rune_health'
    );
    this.healthRune.setScale(0.5);
    this.healthRuneAmountTxt = this.add.bitmapText(
      this.healthRune.x,
      this.healthRune.y,
      'sakkalbold',
      '50',
      30
    );
    this.healthRuneAmountTxt.setOrigin(0.5);

    this.healthRuneTxt = this.add.bitmapText(
      this.healthRune.x,
      this.healthRune.y + 30,
      'sakkalbold',
      Utils.GlobalSettings.text.setupPlayer.health,
      35
    );
    this.healthRuneTxt.setOrigin(0.5);

    this.powerRune = this.add.image(
      this.statsDialog.x,
      this.statsDialog.y - 5,
      'atlas1',
      'rune_power'
    );
    this.powerRune.setScale(0.5);

    this.powerRuneAmountTxt = this.add.bitmapText(
      this.powerRune.x,
      this.powerRune.y,
      'sakkalbold',
      '50',
      30
    );
    this.powerRuneAmountTxt.setOrigin(0.5);

    this.powerRuneTxt = this.add.bitmapText(
      this.powerRune.x,
      this.powerRune.y + 30,
      'sakkalbold',
      Utils.GlobalSettings.text.setupPlayer.power,
      35
    );
    this.powerRuneTxt.setOrigin(0.5);

    this.magicRune = this.add.image(
      this.statsDialog.x + 130,
      this.statsDialog.y - 5,
      'atlas1',
      'rune_magic'
    );
    this.magicRune.setScale(0.5);

    this.magicRuneAmountTxt = this.add.bitmapText(
      this.magicRune.x,
      this.magicRune.y,
      'sakkalbold',
      '50',
      30
    );
    this.magicRuneAmountTxt.setOrigin(0.5);

    this.magicRuneTxt = this.add.bitmapText(
      this.magicRune.x,
      this.magicRune.y + 30,
      'sakkalbold',
      Utils.GlobalSettings.text.setupPlayer.magic,
      35
    );
    this.magicRuneTxt.setOrigin(0.5);

    this.toggleStatsPanel(false);
  }

  setStats(hp, pow, mp) {
    this.healthRuneAmountTxt.setText('+' + hp);
    this.powerRuneAmountTxt.setText('+' + pow);
    this.magicRuneAmountTxt.setText('+' + mp);
  }

  toggleStatsPanel(visible) {
    this.statsBackground.visible = visible;
    this.statsDialog.visible = visible;
    this.healthRune.visible = visible;
    this.healthRuneAmountTxt.visible = visible;
    this.healthRuneTxt.visible = visible;
    this.powerRune.visible = visible;
    this.powerRuneAmountTxt.visible = visible;
    this.powerRuneTxt.visible = visible;
    this.magicRune.visible = visible;
    this.magicRuneAmountTxt.visible = visible;
    this.magicRuneTxt.visible = visible;
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    } else if (key === 'home' && data) {
      this.registry.set('showHome', false);
      this.registry.set('showParty', false);
      this.scene.start('main', { from: 'levelUp' });
    }
  }

  update() {
    this.messageWaveIdx = 0;

    if (this.messageDelay++ === 6) {
      this.messageColorOffset =
        (this.messageColorOffset + 1) % this.messageColor.length;
      this.messageDelay = 0;
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.message) {
      this.message.destroy();
      this.message = null;
    }

    if (this.playerIcon) {
      this.playerIcon.destroy();
      this.playerIcon = null;
    }

    if (this.playerName) {
      this.playerName.destroy();
      this.playerName = null;
    }

    if (this.playerLevel) {
      this.playerLevel.destroy();
      this.playerLevel = null;
    }

    this.messageColor = null;
    this.player = null;

    if (this.particles) {
      this.particles.destroy();
      this.particles = null;
    }

    if (this.goButton) {
      this.goButton.destroy();
      this.goButton = null;
    }

    if (this.clickSnd) {
      this.clickSnd.destroy();
      this.clickSnd = null;
    }

    if (this.statsBackground) {
      this.statsBackground.destroy();
      this.statsBackground = null;
    }

    if (this.statsDialog) {
      this.statsDialog.destroy();
      this.statsDialog = null;
    }

    if (this.healthRune) {
      this.healthRune.destroy();
      this.healthRune = null;
    }

    if (this.healthRuneAmountTxt) {
      this.healthRuneAmountTxt.destroy();
      this.healthRuneAmountTxt = null;
    }

    if (this.healthRuneTxt) {
      this.healthRuneTxt.destroy();
      this.healthRuneTxt = null;
    }

    if (this.powerRune) {
      this.powerRune.destroy();
      this.powerRune = null;
    }

    if (this.powerRuneAmountTxt) {
      this.powerRuneAmountTxt.destroy();
      this.powerRuneAmountTxt = null;
    }

    if (this.powerRuneTxt) {
      this.powerRuneTxt.destroy();
      this.powerRuneTxt = null;
    }

    if (this.magicRune) {
      this.magicRune.destroy();
      this.magicRune = null;
    }

    if (this.magicRuneAmountTxt) {
      this.magicRuneAmountTxt.destroy();
      this.magicRuneAmountTxt = null;
    }

    if (this.magicRuneTxt) {
      this.magicRuneTxt.destroy();
      this.magicRuneTxt = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill level up');
  }
}
