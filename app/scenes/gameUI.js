import Utils from 'utils/utils';

export default class GameUI extends Phaser.Scene {
  constructor() {
    super({
      key: 'gameUI',
      active: false
    });
  }

  create() {
    this.clickSnd = this.sound.add('click');

    this.sprGroup = this.add.group();

    this.soundButton = this.add.sprite(
      Utils.GlobalSettings.width - 50,
      10,
      'atlas1',
      'systembuttons0'
    );
    this.soundButton.setOrigin(0, 0);
    this.soundButton.alpha = 0;
    this.soundButton.setInteractive();

    if (Utils.SavedSettings.muted) {
      this.soundButton.setFrame('systembuttons2');
    }

    this.soundButton.on('pointerup', () => this.soundClick());

    this.soundButton.on('pointerover', event => {
      if (Utils.SavedSettings.muted) {
        this.soundButton.setFrame('systembuttons3');
      } else {
        this.soundButton.setFrame('systembuttons1');
      }
    });

    this.soundButton.on('pointerout', event => {
      if (Utils.SavedSettings.muted) {
        this.soundButton.setFrame('systembuttons2');
      } else {
        this.soundButton.setFrame('systembuttons0');
      }
    });

    this.homeButton = this.add.sprite(
      Utils.GlobalSettings.width - 50,
      60,
      'atlas1',
      'systembuttons8'
    );
    this.homeButton.visible = false;
    this.homeButton.setOrigin(0, 0);
    this.homeButton.setInteractive();

    this.homeButton.on('pointerup', () => this.homeClick());

    this.homeButton.on('pointerover', event => {
      this.homeButton.setFrame('systembuttons9');
    });

    this.homeButton.on('pointerout', event => {
      this.homeButton.setFrame('systembuttons8');
    });

    this.partyButton = this.add.sprite(
      Utils.GlobalSettings.width - 50,
      110,
      'atlas1',
      'systembuttons12'
    );
    this.partyButton.visible = false;
    this.partyButton.setOrigin(0, 0);
    this.partyButton.setInteractive();

    this.partyButton.on('pointerup', () => this.partyClick());

    this.partyButton.on('pointerover', event => {
      this.partyButton.setFrame('systembuttons13');
    });

    this.partyButton.on('pointerout', event => {
      this.partyButton.setFrame('systembuttons12');
    });

    this.tweens.add({
      targets: this.soundButton,
      alpha: 1,
      ease: 'Linear',
      duration: 1000
    });

    this.bgOverlay = this.add.image(0, 0, 'atlas1', 'overlay');
    this.bgOverlay.setOrigin(0);
    this.bgOverlay.setDisplaySize(
      Utils.GlobalSettings.width,
      Utils.GlobalSettings.height
    );
    this.bgOverlay.alpha = 0;
    this.bgOverlay.visible = false;
    this.bgOverlay.setTint(0x000000);
    this.bgOverlay.setInteractive();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted' && data) {
      Utils.SavedSettings.muted = true;
      Utils.save();
      this.soundButton.setFrame('systembuttons2');
    } else if (key === 'showHome') {
      if (data) {
        if (this.homeButton) {
          this.showHomeButton();
        }
      } else {
        if (this.homeButton) {
          this.hideHomeButton();
        }
      }
    } else if (key === 'showParty') {
      if (data) {
        if (this.partyButton) {
          this.showPartyButton();
        }
      } else {
        if (this.partyButton) {
          this.hidePartyButton();
        }
      }
    }
  }

  soundClick() {
    Utils.SavedSettings.muted = !Utils.SavedSettings.muted;
    Utils.save();
    if (!Utils.SavedSettings.muted) {
      this.clickSnd.play();
      this.soundButton.setFrame('systembuttons0');

      this.registry.set('muted', false);
    } else {
      this.soundButton.setFrame('systembuttons2');
      this.registry.set('muted', true);
    }
  }

  showHomeButton() {
    this.homeButton.visible = true;
  }

  hideHomeButton() {
    this.homeButton.visible = false;
  }

  homeClick() {
    if (!Utils.SavedSettings.muted) {
      this.clickSnd.play();
    }

    this.registry.set('home', true);
  }

  showPartyButton() {
    this.partyButton.visible = true;
  }

  hidePartyButton() {
    this.partyButton.visible = false;
  }

  partyClick() {
    if (!Utils.SavedSettings.muted) {
      this.clickSnd.play();
    }

    // this.bgOverlay.visible = true;
    // this.tweens.add({
    //   targets: this.bgOverlay,
    //   alpha: { from: 0, to: 0.8 },
    //   ease: 'Linear',
    //   duration: 500
    // });

    this.registry.set('party', true);
  }

  shutdown() {
    if (this.sprGroup) {
      this.sprGroup.destroy();
      this.sprGroup = null;
    }

    if (this.soundButton) {
      this.soundButton.destroy();
      this.soundButton = null;
    }

    if (this.homeButton) {
      this.homeButton.destroy();
      this.homeButton = null;
    }

    if (this.clickSnd) {
      this.clickSnd.destroy();
      this.clickSnd = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill ui');
  }
}
