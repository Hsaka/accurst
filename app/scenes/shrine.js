import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import SwapWeaponPanel from 'utils/shared/swapWeaponPanel';
import DropItemPanel from 'utils/shared/dropItemPanel';
import GameCalculations from 'utils/gameCalculations';

export default class ShrineScene extends Phaser.Scene {
  constructor() {
    super('shrine');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    this.clickSnd = this.sound.add('click');
    this.turnSnd = this.sound.add('uiTurn');

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'shrine');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.itemRewardID = -1;
    this.weaponRewardID = -1;

    this.dropItemPanel = new DropItemPanel(this);
    this.swapWeaponPanel = new SwapWeaponPanel(this);

    this.currentPlayer =
      Utils.GlobalSettings.gameState.players[
        Utils.GlobalSettings.gameState.turn
      ];

    this.endsWithS = UIHelper.endsWithS(this.currentPlayer.name);

    this.infoDialog = UIHelper.createTextDialog(
      this,
      Utils.GlobalSettings.width / 2,
      50,
      500,
      75,
      this.endsWithS
        ? this.currentPlayer.name + "' " + Utils.GlobalSettings.text.shrine.turn
        : this.currentPlayer.name +
            "'s " +
            Utils.GlobalSettings.text.shrine.turn
    );
    this.infoDialog.textContent.alpha = 0;
    this.infoDialog.setScale(0);

    this.playerIcon = this.add.image(
      this.infoDialog.x - 250,
      this.infoDialog.y,
      'atlas1',
      Utils.GlobalSettings.heroList[this.currentPlayer.classType].image
    );
    this.playerIcon.setScale(0);

    this.tweens.add({
      targets: [this.infoDialog],
      scale: { from: 0, to: 1 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    this.tweens.add({
      targets: [this.playerIcon],
      scale: { from: 0, to: 0.3 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    this.tweens.add({
      targets: [this.infoDialog.textContent],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 1000,
    });

    if (!Utils.SavedSettings.muted) {
      this.turnSnd.play();
    }

    this.instructionBackground = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.instructionDialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2,
        y: 210,
        width: 600,
        height: 120,
      })
      .addBackground(this.instructionBackground)
      .layout();
    this.instructionDialog.alpha = 0;

    this.instructionTxt = this.add.bitmapText(
      this.instructionDialog.x,
      this.instructionDialog.y,
      'sakkal',
      Utils.GlobalSettings.text.shrine.instructions,
      40
    );
    this.instructionTxt.alpha = 0;
    this.instructionTxt.setOrigin(0.5);

    this.tweens.add({
      targets: [this.instructionDialog, this.instructionTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: 500,
    });

    this.particles = this.add.particles('effectAtlas1');
    var circle = new Phaser.Geom.Circle(0, 0, 50);

    this.particles.createEmitter({
      frame: 'circle_05',
      x: Utils.GlobalSettings.width / 2,
      y: Utils.GlobalSettings.height / 2 - 30,
      blendMode: 'ADD',
      angle: { min: 180, max: 360 },
      speed: 250,
      gravityY: 200,
      lifespan: 4000,
      quantity: 6,
      scale: { start: 0.1, end: 1 },
      alpha: { start: 1, end: 0 },
      tint: [0x222222, 0x111111, 0x555555, 0x444444],
    });

    this.symbolIcons = [];

    for (var i = 0; i < 3; i++) {
      var spr = this.add.image(
        Utils.GlobalSettings.width / 2 - 200 + i * 200,
        Utils.GlobalSettings.height / 2 + 50,
        'atlas1',
        i === 0 ? 'rune_power' : i === 1 ? 'rune_health' : 'rune_magic'
      );
      spr.setOrigin(0.5);
      spr.setScale(0);
      spr.alpha = 0;
      spr.setInteractive();
      spr._index = i;
      this.symbolIcons.push(spr);

      spr.on(
        'pointerover',
        function (spr) {
          spr.setTint(0x00ff00);
        }.bind(this, spr)
      );

      spr.on(
        'pointerout',
        function (spr) {
          spr.setTint(0xffffff);
        }.bind(this, spr)
      );

      spr.on(
        'pointerup',
        function (spr) {
          this.tweens.add({
            targets: [
              this.instructionDialog,
              this.instructionTxt,
              this.message,
            ],
            alpha: { from: 1, to: 0 },
            ease: 'Linear.easeOut',
            duration: 500,
          });

          for (var j = 0; j < this.symbolIcons.length; j++) {
            this.symbolIcons[j].disableInteractive();
            if (spr._index !== j) {
              this.tweens.add({
                targets: [this.symbolIcons[j]],
                alpha: { from: 1, to: 0 },
                scale: { from: 1, to: 0 },
                ease: 'Back.easeOut',
                duration: 500,
              });
            }
          }

          this.tweens.add({
            targets: [spr],
            y: Utils.GlobalSettings.height / 2,
            x: Utils.GlobalSettings.width / 2,
            ease: 'Sine.easeOut',
            duration: 1000,
          });

          this.tweens.add({
            targets: [spr],
            y: Utils.GlobalSettings.height / 2,
            x: Utils.GlobalSettings.width / 2,
            alpha: { from: 1, to: 0 },
            scale: { from: 1, to: 0 },
            ease: 'Back.easeOut',
            duration: 1000,
            delay: 1000,
            onComplete: () => {
              this.checkOffering(spr.frame.name);
            },
          });
        }.bind(this, spr)
      );

      this.tweens.add({
        targets: [spr],
        alpha: { from: 0, to: 1 },
        scale: { from: 0, to: 1 },
        ease: 'Back.easeOut',
        duration: 500,
        delay: 500 * i,
      });

      this.tweens.add({
        targets: spr,
        y: Utils.GlobalSettings.height / 2 + 30,
        duration: 5000,
        ease: 'Sine.easeInOut',
        yoyo: true,
        loop: -1,
        delay: 500 * i,
      });
    }

    this.message = this.add.dynamicBitmapText(
      0,
      Utils.GlobalSettings.height - 250,
      'sakkalbold',
      Utils.GlobalSettings.text.shrine.title,
      80
    );
    this.message.setOrigin(0, 0);

    this.messageWaveIdx = 0;
    this.messageWave = 0;
    this.messageDelay = 0;
    this.messageColor = [
      0xffa8a8,
      0xffacec,
      0xffa8d3,
      0xfea9f3,
      0xefa9fe,
      0xe7a9fe,
      0xc4abfe,
    ];
    this.messageColorOffset = 0;

    this.message.setDisplayCallback((data) => {
      data.color = this.messageColor[
        (this.messageColorOffset + this.messageWaveIdx) %
          this.messageColor.length
      ];
      this.messageWaveIdx =
        (this.messageWaveIdx + 1) % this.messageColor.length;
      data.y = Math.cos(this.messageWave + this.messageWaveIdx) * 5;
      this.messageWave += 0.005;

      return data;
    });
    this.message.x = Utils.GlobalSettings.width / 2 - this.message.width / 2;
    this.message.alpha = 0;

    this.tweens.add({
      targets: [this.message],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: 1500,
    });

    this.setupRewardPanel();

    this.goButton = UIHelper.createNavButton(
      this,
      Utils.GlobalSettings.width - 50,
      'forward'
    );
    this.goButton.alpha = 0;
    this.goButton.on('pointerup', () => {
      if (!Utils.SavedSettings.muted) {
        Utils.GlobalSettings.selectSound.play();
      }

      this.exitRoom();
    });

    this.tweens.add({
      targets: [this.goButton],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 500,
      delay: 1500,
    });

    this.dropItemPanel.setupDropItemPanel(this.currentPlayer);

    this.dropItemPanel.onItemPressed = () => {
      if (this.itemRewardID !== -1) {
        this.itemRewardID = -1;
      }
    };

    this.swapWeaponPanel.setupSwapWeaponPanel(this.currentPlayer);

    this.swapWeaponPanel.onConfirmPressed = () => {
      if (this.weaponRewardID !== -1) {
        this.currentPlayer.weapon = this.weaponRewardID;
      }
    };

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI');
    }
    this.scene.bringToTop('gameUI');
  }

  setupRewardPanel() {
    this.rewardBg = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2,
        y: Utils.GlobalSettings.height / 2 - 30,
        width: 500,
        height: 80,
      })
      .addBackground(
        this.rexUI.add
          .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
          .setStrokeStyle(1, UIHelper.COLOR_LIGHT)
      )
      .layout();
    this.rewardBg.visible = false;

    this.rewardIcon = this.add.image(
      this.rewardBg.x - 170,
      this.rewardBg.y,
      'atlas1',
      Utils.GlobalSettings.heroList[this.currentPlayer.classType].image
    );
    this.rewardIcon.visible = false;
    this.rewardIcon.setDisplaySize(100, 100);

    this.rewardIconName = this.add.bitmapText(
      this.rewardIcon.x,
      this.rewardIcon.y + 50,
      'sakkalbold',
      this.currentPlayer.name,
      30
    );
    this.rewardIconName.visible = false;
    this.rewardIconName.setOrigin(0.5);

    this.rewardText = this.add.bitmapText(
      this.rewardIcon.x + 70,
      this.rewardIcon.y,
      'sakkalbold',
      '',
      35
    );
    this.rewardText.visible = false;
    this.rewardText.setOrigin(0, 0.5);
  }

  showRewardPanel(delay = 0, offering, accepted) {
    this.rewardBg.visible = true;
    this.rewardIcon.visible = true;
    this.rewardIconName.visible = true;
    this.rewardText.visible = true;

    this.rewardBg.alpha = 0;
    this.rewardIcon.alpha = 0;
    this.rewardIconName.alpha = 0;
    this.rewardText.alpha = 0;

    var randMax =
      offering === 'rune_power' ? 2 : offering === 'rune_health' ? 4 : 6;
    var randResult = Utils.GlobalSettings.rnd.integerInRange(0, randMax);
    var text = '';
    var value = 0;

    if (accepted) {
      switch (randResult) {
        case 0: //gain gold
          value = GameCalculations.calculateGoldGain(this.currentPlayer.level);
          this.currentPlayer.gold += value;
          text = `Gold!\nGot +${value} Gold! (Total: ${this.currentPlayer.gold})`;
          break;

        case 1: //gain hp
          value =
            offering === 'rune_power'
              ? Math.ceil(this.currentPlayer.maxHealth / 3)
              : offering === 'rune_health'
              ? Math.ceil(this.currentPlayer.maxHealth / 2)
              : this.currentPlayer.maxHealth;
          this.currentPlayer.health += value;
          var maxHP = GameCalculations.getMaxHealth(this.currentPlayer);

          if (this.currentPlayer.health > maxHP) {
            this.currentPlayer.health = maxHP;
          }
          text = `Health!\nGot +${value} HP! (HP: ${this.currentPlayer.health}/${maxHP})`;
          break;

        case 2: //gain mp
          value =
            offering === 'rune_power'
              ? Math.ceil(this.currentPlayer.maxMagic / 3)
              : offering === 'rune_health'
              ? Math.ceil(this.currentPlayer.maxMagic / 2)
              : this.currentPlayer.maxMagic;
          this.currentPlayer.magic += value;
          var maxMP = GameCalculations.getMaxMagic(this.currentPlayer);

          if (this.currentPlayer.magic > maxMP) {
            this.currentPlayer.magic = maxMP;
          }
          text = `Magic!\nGot +${value} MP! (MP: ${this.currentPlayer.magic}/${maxMP})`;
          break;

        case 3: //remove status effect
          this.currentPlayer.status =
            Utils.GlobalSettings.text.battle.status_normal;
          text = `Cure All!\nStatus Ailments Removed!`;
          break;

        case 4: //heal all
          for (var i = 0; i < Utils.GlobalSettings.gameState.numPlayers; i++) {
            var player = Utils.GlobalSettings.gameState.players[i];
            player.health = player.maxHealth;
          }
          text = `Heal All!\nAll Health Restored!`;
          break;

        case 5: //item
          this.itemRewardID = GameCalculations.getItemReward(1);
          var item = Utils.GlobalSettings.itemsList[this.itemRewardID];

          if (this.currentPlayer.items.length < 4) {
            this.currentPlayer.items.push(this.itemRewardID);
          } else {
            this.dropItemPanel.setPlayer(this.currentPlayer);
            this.dropItemPanel.showDropItemPanel(1000, this.itemRewardID);
          }

          text = `Item!\nGot Item: ${item.name}!`;
          break;

        case 6: //weapon
          this.weaponRewardID = GameCalculations.getWeaponReward(
            'red_chest',
            this.currentPlayer.weapon,
            this.currentPlayer.weaponProficiency
          );
          this.swapWeaponPanel.showSwapWeaponPanel(1000, this.weaponRewardID);
          var weapon = Utils.GlobalSettings.weaponsList[this.weaponRewardID];

          text = `Weapon!\nGot Weapon: ${weapon.name}!`;
          break;
      }

      this.rewardText.setTint(0x00ff00);
      this.rewardText.setText(`Reward: ${text}`);
    } else {
      switch (randResult) {
        case 0: //lose gold
          value = GameCalculations.calculateGoldGain(this.currentPlayer.level);
          this.currentPlayer.gold -= value;
          if (this.currentPlayer.gold < 0) {
            this.currentPlayer.gold = 0;
          }
          text = `Gold Loss...\nLost ${value} Gold! (Total: ${this.currentPlayer.gold})`;
          break;

        case 1: //lose hp
          value =
            offering === 'rune_power'
              ? Math.ceil(this.currentPlayer.maxHealth / 4)
              : offering === 'rune_health'
              ? Math.ceil(this.currentPlayer.maxHealth / 3)
              : Math.ceil(this.currentPlayer.maxHealth / 2);
          this.currentPlayer.health -= value;
          if (this.currentPlayer.health <= 0) {
            this.currentPlayer.health = 1;
          }
          var maxHP = GameCalculations.getMaxHealth(this.currentPlayer);

          text = `Health Loss...\nLost ${value} HP! (HP: ${this.currentPlayer.health}/${maxHP})`;
          break;

        case 2: //lose mp
          value =
            offering === 'rune_power'
              ? Math.ceil(this.currentPlayer.maxMagic / 3)
              : offering === 'rune_health'
              ? Math.ceil(this.currentPlayer.maxMagic / 2)
              : this.currentPlayer.maxMagic;
          this.currentPlayer.magic -= value;
          if (this.currentPlayer.magic < 0) {
            this.currentPlayer.magic = 0;
          }
          var maxMP = GameCalculations.getMaxMagic(this.currentPlayer);

          text = `Magic Loss...\nLost ${value} MP! (MP: ${this.currentPlayer.magic}/${maxMP})`;
          break;

        case 3: //lose power
          value =
            offering === 'rune_power'
              ? Math.ceil(this.currentPlayer.maxPower / 4)
              : offering === 'rune_health'
              ? Math.ceil(this.currentPlayer.maxPower / 3)
              : Math.ceil(this.currentPlayer.maxPower / 2);
          this.currentPlayer.power -= value;
          if (this.currentPlayer.power <= 0) {
            this.currentPlayer.power = 1;
          }
          var maxPow = GameCalculations.getMaxPower(this.currentPlayer);

          text = `Power Loss...\nLost ${value} Power! (Power: ${this.currentPlayer.power}/${maxPow})`;
          break;

        case 4: //lose speed
          value =
            offering === 'rune_power'
              ? Math.ceil(this.currentPlayer.speed / 4)
              : offering === 'rune_health'
              ? Math.ceil(this.currentPlayer.speed / 3)
              : Math.ceil(this.currentPlayer.speed / 2);
          this.currentPlayer.speed -= value;
          if (this.currentPlayer.speed <= 0) {
            this.currentPlayer.speed = 1;
          }

          text = `Speed Loss...\nLost ${value} Speed! (Speed: ${this.currentPlayer.speed})`;
          break;

        case 5: //lose item or inflict status
          if (this.currentPlayer.items.length > 0) {
            var itemIndex = Utils.GlobalSettings.rnd.integerInRange(
              0,
              this.currentPlayer.items.length - 1
            );
            var itemID = this.currentPlayer.items[itemIndex];
            var item = Utils.GlobalSettings.itemsList[itemID];
            this.currentPlayer.items.splice(itemIndex, 1);
            text = `Item Loss...\nLost Item: ${item.name}!`;
          } else {
            this.currentPlayer.status = GameCalculations.getRandomStatus();
            text = `Cursed...\nStatus Effect: ${this.currentPlayer.status}!`;
          }
          break;

        case 6: //curse
          this.currentPlayer.status = GameCalculations.getRandomStatus();
          text = `Cursed...\nStatus Effect: ${this.currentPlayer.status}!`;
          break;
      }

      this.rewardText.setTint(0xff0000);
      this.rewardText.setText(`Punishment: ${text}`);
    }

    this.tweens.add({
      targets: [this.rewardBg, this.rewardIcon, this.rewardIconName],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: delay,
    });

    this.tweens.add({
      targets: [this.rewardText],
      alpha: { from: 0, to: 1 },
      x: { from: this.rewardText.x + 10, to: this.rewardText.x },
      ease: 'Sine.easeOut',
      duration: 500,
      delay: delay + 500,
    });
  }

  checkOffering(offering) {
    var factor =
      offering === 'rune_power'
        ? 1 / 3
        : offering === 'rune_health'
        ? 1 / 2
        : 2 / 3;

    var rand = Utils.GlobalSettings.rnd.frac();
    var accepted = false;
    if (rand <= factor) {
      accepted = true;
    }

    this.message.alpha = 1;
    if (accepted) {
      this.message.setText('Offering  Accepted!');
      this.message.x = Utils.GlobalSettings.width / 2 - this.message.width / 2;
      this.cameras.main.flash(1000, 0, 255, 0);
    } else {
      this.message.setText('Offering  Rejected!');
      this.message.x = Utils.GlobalSettings.width / 2 - this.message.width / 2;
      this.cameras.main.shake(1000, 0.01);
      this.cameras.main.flash(1000, 255, 0, 0);
    }

    this.showRewardPanel(1000, offering, accepted);
  }

  exitRoom() {
    Utils.GlobalSettings.gameState.rooms[
      Utils.GlobalSettings.gameState.lastRoomId
    ].isCompleted = true;
    Utils.GlobalSettings.gameState.encounter = {};
    Utils.GlobalSettings.gameState.phase = 'exploration';

    Utils.GlobalSettings.gameState.turn++;
    if (
      Utils.GlobalSettings.gameState.turn >=
      Utils.GlobalSettings.gameState.numPlayers
    ) {
      Utils.GlobalSettings.gameState.turn = 0;
    }
    Utils.saveGameState();

    this.scene.start('mapExploration', { from: 'shrine' });
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    } else if (key === 'home' && data) {
      this.registry.set('showHome', false);
      this.registry.set('showParty', false);
      this.scene.start('main', { from: 'shrine' });
    }
  }

  update() {
    this.messageWaveIdx = 0;

    if (this.messageDelay++ === 6) {
      this.messageColorOffset =
        (this.messageColorOffset + 1) % this.messageColor.length;
      this.messageDelay = 0;
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.clickSnd) {
      this.clickSnd.destroy();
      this.clickSnd = null;
    }

    if (this.turnSnd) {
      this.turnSnd.destroy();
      this.turnSnd = null;
    }

    this.currentPlayer = null;

    if (this.particles) {
      this.particles.destroy();
      this.particles = null;
    }

    if (this.infoDialog) {
      this.infoDialog.destroy();
      this.infoDialog = null;
    }

    if (this.playerIcon) {
      this.playerIcon.destroy();
      this.playerIcon = null;
    }

    if (this.instructionBackground) {
      this.instructionBackground.destroy();
      this.instructionBackground = null;
    }

    if (this.instructionDialog) {
      this.instructionDialog.destroy();
      this.instructionDialog = null;
    }

    if (this.instructionTxt) {
      this.instructionTxt.destroy();
      this.instructionTxt = null;
    }

    for (var i = 0; i < this.symbolIcons.length; i++) {
      if (this.symbolIcons[i]) {
        this.symbolIcons[i].destroy();
        this.symbolIcons[i] = null;
      }
    }

    this.symbolIcons = null;

    if (this.message) {
      this.message.destroy();
      this.message = null;
    }

    if (this.goButton) {
      this.goButton.destroy();
      this.goButton = null;
    }

    if (this.swapWeaponPanel) {
      this.swapWeaponPanel.destroy();
      this.swapWeaponPanel = null;
    }

    if (this.dropItemPanel) {
      this.dropItemPanel.destroy();
      this.dropItemPanel = null;
    }

    if (this.rewardBg) {
      this.rewardBg.destroy();
      this.rewardBg = null;
    }

    if (this.rewardIcon) {
      this.rewardIcon.destroy();
      this.rewardIcon = null;
    }

    if (this.rewardIconName) {
      this.rewardIconName.destroy();
      this.rewardIconName = null;
    }

    if (this.rewardText) {
      this.rewardText.destroy();
      this.rewardText = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill shrine room');
  }
}
