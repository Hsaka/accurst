import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import DropItemPanel from 'utils/shared/dropItemPanel';
import GameCalculations from 'utils/gameCalculations';

export default class ShopScene extends Phaser.Scene {
  constructor() {
    super('shop');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    this.clickSnd = this.sound.add('click');
    this.turnSnd = this.sound.add('uiTurn');

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'shop');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.dropItemPanel = new DropItemPanel(this);

    this.currentPlayer =
      Utils.GlobalSettings.gameState.players[
        Utils.GlobalSettings.gameState.turn
      ];

    this.selectedItemIndex = -1;

    this.endsWithS = UIHelper.endsWithS(this.currentPlayer.name);

    this.infoDialog = UIHelper.createTextDialog(
      this,
      Utils.GlobalSettings.width / 2,
      50,
      500,
      75,
      this.endsWithS
        ? this.currentPlayer.name +
            "' " +
            Utils.GlobalSettings.text.shop.gold +
            ': ' +
            this.currentPlayer.gold
        : this.currentPlayer.name +
            "'s " +
            Utils.GlobalSettings.text.shop.gold +
            ': ' +
            this.currentPlayer.gold
    );
    this.infoDialog.textContent.alpha = 0;
    this.infoDialog.setScale(0);

    this.playerIcon = this.add.image(
      this.infoDialog.x - 250,
      this.infoDialog.y,
      'atlas1',
      Utils.GlobalSettings.heroList[this.currentPlayer.classType].image
    );
    this.playerIcon.setScale(0);

    this.numItemsTxt = this.add.bitmapText(
      this.playerIcon.x + 40,
      this.playerIcon.y + 40,
      'sakkal',
      `You have ${this.currentPlayer.items.length} ${
        this.currentPlayer.items.length === 1 ? 'item' : 'items'
      }. You can only carry up to 4 items at once.`,
      25
    );
    this.numItemsTxt.alpha = 0;

    this.tweens.add({
      targets: [this.infoDialog],
      scale: { from: 0, to: 1 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    this.tweens.add({
      targets: [this.playerIcon],
      scale: { from: 0, to: 0.3 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    this.tweens.add({
      targets: [this.infoDialog.textContent, this.numItemsTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 1000,
    });

    if (!Utils.SavedSettings.muted) {
      this.turnSnd.play();
    }

    this.instructionBackground = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.instructionDialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2,
        y: 210,
        width: 600,
        height: 120,
      })
      .addBackground(this.instructionBackground)
      .layout();
    this.instructionDialog.alpha = 0;

    this.instructionTxt = this.add.bitmapText(
      this.instructionDialog.x,
      this.instructionDialog.y,
      'sakkal',
      Utils.GlobalSettings.text.shop.instructions,
      40
    );
    this.instructionTxt.alpha = 0;
    this.instructionTxt.setOrigin(0.5);

    this.tweens.add({
      targets: [this.instructionDialog, this.instructionTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: 500,
    });

    this.particles = this.add.particles('effectAtlas1');
    var triangle = new Phaser.Geom.Triangle.BuildEquilateral(0, -250, 400);

    this.particles.createEmitter({
      frame: 'circle_05',
      x: Utils.GlobalSettings.width / 2,
      y: Utils.GlobalSettings.height / 2 + 150,
      speed: 0,
      lifespan: 500,
      delay: 500,
      frequency: 0,
      quantity: 1,
      scale: 1.5,
      tint: [0xffffff],
      blendMode: 'ADD',
      emitZone: { type: 'edge', source: triangle, quantity: 48 },
    });

    this.itemBases = [];
    this.itemIcons = [];
    this.itemPanels = [];

    this.itemIDs = GameCalculations.getItemReward(3);

    this.toast = UIHelper.createToast(this);

    for (var i = 0; i < 3; i++) {
      var item = Utils.GlobalSettings.itemsList[this.itemIDs[i]];

      var spr = this.add.image(
        Utils.GlobalSettings.width / 2 - 200 + i * 200,
        Utils.GlobalSettings.height / 2 - 75,
        'atlas1',
        'rune_weapon'
      );
      spr.setOrigin(0.5);
      if (i === 0 || i === 2) {
        spr.y = Utils.GlobalSettings.height / 2 + 225;
      }
      spr.setScale(0);
      spr.alpha = 0;
      spr.setInteractive();
      spr._item = item;
      spr._itemID = this.itemIDs[i];
      spr._index = i;
      this.itemBases.push(spr);

      var spr2 = this.add.image(spr.x, spr.y, 'effectAtlas1', item.image);
      spr2.setOrigin(0.5);
      spr2.setScale(0);
      spr2.alpha = 0;
      this.itemIcons.push(spr2);

      var spr3 = UIHelper.createTextDialog(
        this,
        spr.x,
        spr.y + 110,
        200,
        75,
        `${item.name}\n${item.cost} Gold`,
        'dialog9patch3',
        15,
        32
      );
      spr3.setScale(0);
      spr3.alpha = 0;
      spr3.textContent.alpha = 0;
      this.itemPanels.push(spr3);

      spr.on(
        'pointerover',
        function (spr) {
          spr.setTint(0x00ff00);
        }.bind(this, spr)
      );

      spr.on(
        'pointerout',
        function (spr) {
          spr.setTint(0xffffff);
        }.bind(this, spr)
      );

      spr.on(
        'pointerup',
        function (spr, spr2, spr3) {
          if (this.currentPlayer.gold >= spr._item.cost) {
            this.selectedItemIndex = spr._index;
            if (this.currentPlayer.items.length < 4) {
              this.buyItem(this.selectedItemIndex);
              this.currentPlayer.items.push(spr._itemID);

              this.numItemsTxt.setText(
                `You have ${this.currentPlayer.items.length} ${
                  this.currentPlayer.items.length === 1 ? 'item' : 'items'
                }. You can only carry up to 4 items at once.`
              );
            } else {
              this.dropItemPanel.setPlayer(this.currentPlayer);
              this.dropItemPanel.showDropItemPanel(0, spr._itemID);
            }
          } else {
            UIHelper.showToast(
              this,
              this.toast,
              spr.x - 80,
              spr.y - 110,
              'Not enough gold!'
            );
          }
        }.bind(this, spr, spr2, spr3)
      );

      this.tweens.add({
        targets: [spr, spr2, spr3],
        alpha: { from: 0, to: 1 },
        scale: { from: 0, to: 1 },
        ease: 'Back.easeOut',
        duration: 500,
        delay: 500 * i,
      });

      this.tweens.add({
        targets: [spr3.textContent],
        alpha: { from: 0, to: 1 },
        ease: 'Linear.easeOut',
        duration: 500,
        delay: 500 * i,
      });
    }

    this.goButton = UIHelper.createNavButton(
      this,
      Utils.GlobalSettings.width - 50,
      'forward'
    );
    this.goButton.alpha = 0;
    this.goButton.on('pointerup', () => {
      if (!Utils.SavedSettings.muted) {
        Utils.GlobalSettings.selectSound.play();
      }

      Utils.GlobalSettings.gameState.rooms[
        Utils.GlobalSettings.gameState.lastRoomId
      ].isCompleted = true;
      Utils.GlobalSettings.gameState.encounter = {};
      Utils.GlobalSettings.gameState.phase = 'exploration';

      Utils.GlobalSettings.gameState.turn++;
      if (
        Utils.GlobalSettings.gameState.turn >=
        Utils.GlobalSettings.gameState.numPlayers
      ) {
        Utils.GlobalSettings.gameState.turn = 0;
      }
      Utils.saveGameState();

      this.scene.start('mapExploration', { from: 'shop' });
    });

    this.tweens.add({
      targets: [this.goButton],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 500,
      delay: 1500,
    });

    this.dropItemPanel.setupDropItemPanel(this.currentPlayer);

    this.dropItemPanel.onItemPressed = () => {
      if (this.selectedItemIndex !== -1) {
        this.buyItem(this.selectedItemIndex);
        this.selectedItemIndex = -1;
      }
    };

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI');
    }
    this.scene.bringToTop('gameUI');
  }

  buyItem(index) {
    var spr = this.itemBases[index];
    var spr2 = this.itemIcons[index];
    var spr3 = this.itemPanels[index];

    var newGoldAmt = this.currentPlayer.gold - spr._item.cost;
    spr.disableInteractive();

    this.tweens.addCounter({
      from: this.currentPlayer.gold,
      to: newGoldAmt,
      duration: 500,
      onUpdate: (tween) => {
        this.infoDialog.textContent.setText(
          this.endsWithS
            ? this.currentPlayer.name +
                "' " +
                Utils.GlobalSettings.text.shop.gold +
                ': ' +
                Math.ceil(tween.getValue())
            : this.currentPlayer.name +
                "'s " +
                Utils.GlobalSettings.text.shop.gold +
                ': ' +
                Math.ceil(tween.getValue())
        );
      },
      onComplete: () => {
        this.currentPlayer.gold = newGoldAmt;
        this.infoDialog.textContent.setText(
          this.endsWithS
            ? this.currentPlayer.name +
                "' " +
                Utils.GlobalSettings.text.shop.gold +
                ': ' +
                this.currentPlayer.gold
            : this.currentPlayer.name +
                "'s " +
                Utils.GlobalSettings.text.shop.gold +
                ': ' +
                this.currentPlayer.gold
        );
      },
    });

    this.tweens.add({
      targets: [spr, spr3],
      alpha: { from: 1, to: 0 },
      scale: { from: 1, to: 0 },
      ease: 'Back.easeIn',
      duration: 500,
    });

    this.tweens.add({
      targets: [spr3.textContent],
      alpha: { from: 1, to: 0 },
      ease: 'Linear.easeOut',
      duration: 500,
    });

    this.tweens.add({
      targets: [spr2],
      x: this.playerIcon.x,
      y: this.playerIcon.y,
      ease: 'Back.easeIn',
      duration: 500,
    });

    this.tweens.add({
      targets: [spr2],
      scale: { from: 1, to: 0 },
      ease: 'Sine.easeOut',
      duration: 500,
      delay: 500,
    });
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    } else if (key === 'home' && data) {
      this.registry.set('showHome', false);
      this.registry.set('showParty', false);
      this.scene.start('main', { from: 'shop' });
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.clickSnd) {
      this.clickSnd.destroy();
      this.clickSnd = null;
    }

    if (this.turnSnd) {
      this.turnSnd.destroy();
      this.turnSnd = null;
    }

    this.currentPlayer = null;

    if (this.infoDialog) {
      this.infoDialog.destroy();
      this.infoDialog = null;
    }

    if (this.playerIcon) {
      this.playerIcon.destroy();
      this.playerIcon = null;
    }

    if (this.instructionBackground) {
      this.instructionBackground.destroy();
      this.instructionBackground = null;
    }

    if (this.instructionDialog) {
      this.instructionDialog.destroy();
      this.instructionDialog = null;
    }

    if (this.instructionTxt) {
      this.instructionTxt.destroy();
      this.instructionTxt = null;
    }

    if (this.numItemsTxt) {
      this.numItemsTxt.destroy();
      this.numItemsTxt = null;
    }

    if (this.particles) {
      this.particles.destroy();
      this.particles = null;
    }

    if (this.goButton) {
      this.goButton.destroy();
      this.goButton = null;
    }

    if (this.toast) {
      this.toast.destroy();
      this.toast = null;
    }

    for (var i = 0; i < this.itemBases.length; i++) {
      if (this.itemBases[i] && this.itemIcons[i] && this.itemPanels[i]) {
        this.itemBases[i].destroy();
        this.itemIcons[i].destroy();
        this.itemPanels[i].destroy();

        this.itemBases[i] = null;
        this.itemIcons[i] = null;
        this.itemPanels[i] = null;
      }
    }

    this.itemBases = null;
    this.itemIcons = null;
    this.itemPanels = null;

    if (this.dropItemPanel) {
      this.dropItemPanel.destroy();
      this.dropItemPanel = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill shop');
  }
}
