import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';

export default class MapExplorationScene extends Phaser.Scene {
  constructor() {
    super('mapExploration');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    this.clickSnd = this.sound.add('click');
    this.turnSnd = this.sound.add('uiTurn');

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'bg1');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.currentPlayer =
      Utils.GlobalSettings.gameState.players[
        Utils.GlobalSettings.gameState.turn
      ];

    var endsWithS = UIHelper.endsWithS(this.currentPlayer.name);

    this.infoDialog = UIHelper.createTextDialog(
      this,
      Utils.GlobalSettings.width / 2,
      50,
      500,
      75,
      endsWithS
        ? this.currentPlayer.name +
            "' " +
            Utils.GlobalSettings.text.mapExploration.turn
        : this.currentPlayer.name +
            "'s " +
            Utils.GlobalSettings.text.mapExploration.turn
    );
    this.infoDialog.textContent.alpha = 0;
    this.infoDialog.setScale(0);

    this.playerIcon = this.add.image(
      this.infoDialog.x - 250,
      this.infoDialog.y,
      'atlas1',
      Utils.GlobalSettings.heroList[this.currentPlayer.classType].image
    );
    this.playerIcon.setScale(0);

    this.tweens.add({
      targets: [this.infoDialog],
      scale: { from: 0, to: 1 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    this.tweens.add({
      targets: [this.playerIcon],
      scale: { from: 0, to: 0.3 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    this.tweens.add({
      targets: [this.infoDialog.textContent],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 1000,
    });

    if (!Utils.SavedSettings.muted) {
      this.turnSnd.play();
    }

    this.instructionBackground = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.instructionDialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2,
        y: 210,
        width: 600,
        height: 120,
      })
      .addBackground(this.instructionBackground)
      .layout();
    this.instructionDialog.alpha = 0;

    var instructionStr =
      Utils.GlobalSettings.text.mapExploration.instruction_first_turn;
    if (Object.keys(Utils.GlobalSettings.gameState.rooms).length > 0) {
      instructionStr =
        Utils.GlobalSettings.text.mapExploration.instruction_other_turns;
    }

    this.instructionTxt = this.add.bitmapText(
      this.instructionDialog.x,
      this.instructionDialog.y,
      'sakkal',
      instructionStr,
      40
    );
    this.instructionTxt.alpha = 0;
    this.instructionTxt.setOrigin(0.5);

    this.tweens.add({
      targets: [this.instructionDialog, this.instructionTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: 500,
    });

    if (this.scene.isSleeping('map')) {
      this.scene.wake('map');
    } else if (!this.scene.isActive('map')) {
      this.scene.launch('map');
    }
    this.scene.bringToTop('map');

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI');
    }
    this.scene.bringToTop('gameUI');
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    } else if (key === 'home' && data) {
      this.registry.set('showHome', false);
      this.registry.set('showParty', false);
      this.scene.stop('map');
      this.scene.start('main', { from: 'deckSetup' });
    } else if (key === 'nextScene' && data) {
      Utils.GlobalSettings.gameState.lastRoomId = data.roomId;

      Utils.GlobalSettings.gameState.encounter = {
        type: data.roomType,
        combatTurn: Utils.GlobalSettings.gameState.turn,
        combatRound: 1,
        enemy: undefined,
      };
      Utils.GlobalSettings.gameState.phase = 'encounter';
      Utils.saveGameState();

      this.scene.stop('map');

      switch (data.roomType) {
        case 'green':
          this.scene.start('battle', { from: 'mapExploration' });
          break;
        case 'blue':
          this.scene.start('battle', { from: 'mapExploration' });
          break;
        case 'shop':
          this.scene.start('shop', { from: 'mapExploration' });
          break;
        case 'treasure':
          this.scene.start('treasure', { from: 'mapExploration' });
          break;
        case 'shrine':
          this.scene.start('shrine', { from: 'mapExploration' });
          break;
      }
    } else if (key === 'updateInfoTxt') {
      this.instructionTxt.setText(data);
    } else if (key === 'skipTurn' && data) {
      Utils.GlobalSettings.gameState.turn++;
      if (
        Utils.GlobalSettings.gameState.turn >=
        Utils.GlobalSettings.gameState.numPlayers
      ) {
        Utils.GlobalSettings.gameState.turn = 0;
      }
      Utils.saveGameState();

      this.scene.stop('map');
      this.scene.start('mapExploration', { from: 'mapExploration' });
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.clickSnd) {
      this.clickSnd.destroy();
      this.clickSnd = null;
    }

    if (this.turnSnd) {
      this.turnSnd.destroy();
      this.turnSnd = null;
    }

    this.currentPlayer = null;

    if (this.infoDialog) {
      this.infoDialog.destroy();
      this.infoDialog = null;
    }

    if (this.playerIcon) {
      this.playerIcon.destroy();
      this.playerIcon = null;
    }

    if (this.instructionBackground) {
      this.instructionBackground.destroy();
      this.instructionBackground = null;
    }

    if (this.instructionDialog) {
      this.instructionDialog.destroy();
      this.instructionDialog = null;
    }

    if (this.instructionTxt) {
      this.instructionTxt.destroy();
      this.instructionTxt = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill map exploration');
  }
}
