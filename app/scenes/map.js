import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';

export default class MapScene extends Phaser.Scene {
  constructor() {
    super('map');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    this.selectSnd = this.sound.add('uiSelect');
    this.removeSnd = this.sound.add('uiError');
    this.clickSnd = this.sound.add('click');

    this.setupGrid();

    this.cardGroup = this.add.group();

    for (var i = 0; i < 36; i++) {
      var spr = this.add.image(0, 0, 'atlas1', 'roomback2');
      spr.setDisplaySize(this.cardWidth, this.cardHeight);
      spr.setOrigin(0);
      spr.setInteractive();
      spr._roomId = Phaser.Utils.String.UUID();

      spr.on('pointerover', function (event) {
        this.setTint(0x00ff00);
      });

      spr.on('pointerout', function (event) {
        this.setTint(0xffffff);
      });

      spr.on(
        'pointerup',
        function (selected) {
          this.handleClick(selected);
        }.bind(this, spr)
      );

      this.cardGroup.add(spr);
      this.cardGroup.killAndHide(spr);
    }

    this.setupCards();
    this.setupMysterySpots();
    this.setupPartyLocation();
    this.setupPopup();

    this.screenGroup.alpha = 0;
    this.tweens.add({
      targets: [this.screenGroup],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 1000,
      delay: 1000,
    });

    this.goButton = this.add.sprite(
      Utils.GlobalSettings.width - 80,
      550,
      'atlas1',
      'rune_room'
    );
    this.goButton.setScale(0.5);
    this.goButton.setInteractive();

    this.goButtonTxt = this.add.bitmapText(
      this.goButton.x,
      this.goButton.y,
      'sakkalbold',
      Utils.GlobalSettings.text.mapExploration.enter_room,
      25
    );
    this.goButtonTxt.alpha = 0;
    this.goButtonTxt.visible = false;
    this.goButtonTxt.setOrigin(0.5);

    this.goButton.on('pointerover', (event) => {
      this.goButton.setTint(0x00ff00);
    });

    this.goButton.on('pointerout', (event) => {
      this.goButton.setTint(0xffffff);
    });

    this.goButton.alpha = 0;
    this.goButton.visible = false;
    this.goButton.on('pointerup', () => {
      if (this.selectedCard) {
        if (!Utils.SavedSettings.muted) {
          Utils.GlobalSettings.selectSound.play();
        }
        this.registry.set('nextScene', {
          roomType: this.getEncounterFromRoom(this.selectedCard.frame.name),
          roomId: this.selectedCard._roomId,
        });
      }
    });

    this.skipButton = this.add.sprite(
      this.goButton.x,
      this.goButton.y - 75,
      'atlas1',
      'rune_endturn'
    );
    this.skipButton.setScale(0.5);
    this.skipButton.setInteractive();

    this.skipButtonTxt = this.add.bitmapText(
      this.skipButton.x,
      this.skipButton.y,
      'sakkalbold',
      Utils.GlobalSettings.text.mapExploration.skip_turn,
      25
    );
    this.skipButtonTxt.alpha = 0;
    this.skipButtonTxt.visible = false;
    this.skipButtonTxt.setOrigin(0.5);

    this.skipButton.on('pointerover', (event) => {
      this.skipButton.setTint(0x00ff00);
    });

    this.skipButton.on('pointerout', (event) => {
      this.skipButton.setTint(0xffffff);
    });

    this.skipButton.alpha = 0;
    this.skipButton.visible = false;
    this.skipButton.on('pointerup', () => {
      if (this.selectedCard) {
        if (!Utils.SavedSettings.muted) {
          Utils.GlobalSettings.backSound.play();
        }
        this.registry.set('skipTurn', true);
      }
    });

    this.toast = UIHelper.createToast(this);
  }

  setupMysterySpots() {
    this.mysterySpots = [];
    for (
      var i = 0;
      i < Utils.GlobalSettings.gameState.mysterySpots.length;
      i++
    ) {
      var spot = Utils.GlobalSettings.gameState.mysterySpots[i];

      var x = this.midCard.x + spot.x * this.cardWidth + this.cardWidth / 2;
      var y = this.midCard.y + spot.y * this.cardHeight + this.cardHeight / 2;

      var spr = this.add.image(x, y, 'atlas1', 'rune_mystery');
      spr.setScale(0.35);
      this.screenGroup.add(spr);
      this.mysterySpots.push(spr);
    }
  }

  hasMysterySpot(room) {
    for (
      var i = 0;
      i < Utils.GlobalSettings.gameState.mysterySpots.length;
      i++
    ) {
      var spot = Utils.GlobalSettings.gameState.mysterySpots[i];

      var x = this.midCard.x + spot.x * this.cardWidth;
      var y = this.midCard.y + spot.y * this.cardHeight;

      if (x === room.x && y === room.y) {
        return true;
      }
    }

    return false;
  }

  setupPartyLocation() {
    this.startingPartyLocationX = -200;
    this.startingPartyLocationY = -200;
    this.partyLocation = this.add.image(
      this.startingPartyLocationX,
      this.startingPartyLocationY,
      'atlas1',
      'location'
    );
    this.partyLocation.visible = false;
    this.screenGroup.add(this.partyLocation);

    if (Utils.GlobalSettings.gameState.lastRoomId) {
      var lastRoom =
        Utils.GlobalSettings.gameState.rooms[
          Utils.GlobalSettings.gameState.lastRoomId
        ];
      if (lastRoom) {
        this.startingPartyLocationX = lastRoom.x + this.cardWidth / 2;
        this.startingPartyLocationY = lastRoom.y + this.cardHeight / 2;
        this.partyLocation.x = this.startingPartyLocationX;
        this.partyLocation.y = this.startingPartyLocationY;
        this.partyLocation.visible = true;
        this.scrollToLocation(lastRoom.x, lastRoom.y);
      }
    }
  }

  scrollToLocation(x, y) {
    var newX = -x + this.viewWidth / 2 - this.cardWidth / 2;
    var newY = -y + this.viewHeight / 4 + this.cardHeight / 2;

    if (newX > this.minX) {
      newX = this.minX;
    }

    if (newY > this.minY) {
      newY = this.minY;
    }

    if (newX <= this.maxX) {
      newX = this.maxX;
    }

    if (newY <= this.maxY) {
      newY = this.maxY;
    }

    this.screenGroup.x = newX;
    this.screenGroup.y = newY;
  }

  getEncounterFromRoom(roomFrame) {
    switch (roomFrame) {
      case 'greenroom_small':
        return 'green';
        break;

      case 'blueroom_small':
        return 'blue';
        break;

      case 'shop_small':
        return 'shop';
        break;

      case 'treasureroom_small':
        return 'treasure';
        break;

      case 'shrine_small':
        return 'shrine';
        break;

      case 'bossroom_small':
        return 'boss';
        break;
    }
    return null;
  }

  handleClick(selected) {
    if (!Utils.SavedSettings.muted) {
      this.selectSnd.play();
    }
    this.selectedCard = selected;

    if (this.selectedCard._isNew) {
      this.showPopup();
    } else {
      this.movePartyLocation(this.selectedCard);
    }
  }

  setupGrid() {
    this.gridWidth = 676;
    this.gridHeight = 744;
    this.screenGroup = this.add.container(0, 0);
    this.screenGroup.setSize(this.gridWidth * 10, this.gridHeight * 10);

    this.screenGroup.setInteractive();

    this.input.setDraggable(this.screenGroup);

    this.bg = [];

    this.maxTilesX = 2;
    this.maxTilesY = 2;

    for (var y = 0; y < this.maxTilesY; y++) {
      for (var x = 0; x < this.maxTilesX; x++) {
        var spr = this.add.image(
          x * this.gridWidth,
          y * this.gridHeight,
          'grid'
        );
        spr.setOrigin(0, 0);
        spr.alpha = 0.8;
        this.screenGroup.add(spr);
        this.bg.push(spr);
      }
    }

    this.cardWidth = this.gridWidth / 4;
    this.cardHeight = this.gridHeight / 6;

    this.viewWidth = Utils.GlobalSettings.width - 40;
    this.viewHeight = 600;
    this.minX = 0;
    this.minY = 0;
    this.maxX = -(this.gridWidth * this.maxTilesX - this.viewWidth);
    this.maxY = -(this.gridHeight * this.maxTilesY - this.viewHeight);

    this.screenGroup.x =
      -(this.gridWidth * this.maxTilesX) / 2 +
      this.viewWidth / 2 -
      this.cardWidth / 2;
    this.screenGroup.y =
      -(this.gridHeight * this.maxTilesY) / 2 +
      this.viewHeight / 4 +
      this.cardHeight / 2;

    this.input.on('drag', (pointer, gameObject, dragX, dragY) => {
      if (
        dragX <= this.minX &&
        dragX > this.maxX &&
        dragY <= this.minY &&
        dragY > this.maxY
      ) {
        gameObject.x = dragX;
        gameObject.y = dragY;
      } else {
        if (dragX > this.minX) {
          gameObject.x = this.minX;
        }

        if (dragY > this.minY) {
          gameObject.y = this.minY;
        }

        if (dragX <= this.maxX) {
          gameObject.x = this.maxX;
        }

        if (dragY <= this.maxY) {
          gameObject.y = this.maxY;
        }
      }
    });

    this.cameras.main.setViewport(20, 300, this.viewWidth, this.viewHeight);
  }

  addCard(x, y) {
    var spr = this.cardGroup.getFirstDead();
    if (spr) {
      spr.setActive(true);
      spr.setVisible(true);
      spr.x = x;
      spr.y = y;

      var tween = this.tweens.add({
        targets: [spr],
        alpha: { from: 1, to: 0.7 },
        ease: 'Sine.easeInOut',
        duration: 1000,
        repeat: -1,
        yoyo: true,
      });
      tween.pause();
      spr.tween = tween;

      this.screenGroup.add(spr);

      return spr;
    }
    return null;
  }

  changeFrame(spr, frame) {
    spr.setFrame(frame);
    spr.setDisplaySize(this.cardWidth, this.cardHeight);
    spr.setSize(this.cardWidth, this.cardHeight);
    spr.input.hitArea.setTo(0, 0, spr.width * 2, spr.height * 2);
  }

  setupCards() {
    var midX = (this.gridWidth * this.maxTilesX) / 2;
    var midY = (this.gridHeight * this.maxTilesY) / 2;

    this.midCard = null;

    if (Object.keys(Utils.GlobalSettings.gameState.rooms).length > 0) {
      for (var key in Utils.GlobalSettings.gameState.rooms) {
        var room = Utils.GlobalSettings.gameState.rooms[key];
        if (room) {
          var card = this.addCard(room.x, room.y);
          if (card) {
            card.disableInteractive();
            card._roomId = room.id;
            card._isMiddle = room.isMiddle;
            card._isCompleted = room.isCompleted;

            if (room.isMiddle) {
              this.midCard = card;
            }

            if (room.isCompleted) {
              this.changeFrame(card, 'roomback3');
            } else {
              this.changeFrame(card, room.roomFrame);
            }
          }
        }
      }

      this.screenGroup.each((card) => {
        if (card._roomId && card._isCompleted) {
          this.createAdjacentRooms(card);
        }
      }, this);
    } else {
      this.midCard = this.addCard(midX, midY);
      this.midCard._isMiddle = true;
      this.midCard._isNew = true;
      this.midCard.tween.restart();
      this.midCard.tween.resume();
    }
  }

  isSpotEmpty(x, y) {
    var isEmpty = true;
    this.screenGroup.iterate((room) => {
      if (room._roomId && room.x === x && room.y === y) {
        isEmpty = false;
        return;
      }
    }, this);
    return isEmpty;
  }

  toggleAdjacentRoomVisibility(visible) {
    this.screenGroup.iterate((room) => {
      if (room.frame.name === 'roomback2') {
        room.visible = visible;
      }
    }, this);
  }

  toggleUnexploredRooms(enable) {
    this.screenGroup.each((card) => {
      if (card._roomId && !card._isCompleted && !card._isNew) {
        if (enable) {
          card.setInteractive();
          card.alpha = 1;
          card.tween.restart();
          card.tween.resume();
        } else {
          card.disableInteractive();
          card.alpha = 1;
          card.tween.pause();
        }
      }
    }, this);
  }

  createAdjacentRooms(room) {
    if (room && this.isSpotEmpty(room.x, room.y - this.cardHeight)) {
      var topRoom = this.addCard(room.x, room.y - this.cardHeight);
      if (topRoom) {
        topRoom._isMiddle = false;
        topRoom._isNew = true;
        topRoom.tween.restart();
        topRoom.tween.resume();
      }
    }

    if (room && this.isSpotEmpty(room.x - this.cardWidth, room.y)) {
      var leftRoom = this.addCard(room.x - this.cardWidth, room.y);
      if (leftRoom) {
        leftRoom._isMiddle = false;
        leftRoom._isNew = true;
        leftRoom.tween.restart();
        leftRoom.tween.resume();
      }
    }

    if (room && this.isSpotEmpty(room.x + this.cardWidth, room.y)) {
      var rightRoom = this.addCard(room.x + this.cardWidth, room.y);
      if (rightRoom) {
        rightRoom._isMiddle = false;
        rightRoom._isNew = true;
        rightRoom.tween.restart();
        rightRoom.tween.resume();
      }
    }

    if (room && this.isSpotEmpty(room.x, room.y + this.cardHeight)) {
      var bottomRoom = this.addCard(room.x, room.y + this.cardHeight);
      if (bottomRoom) {
        bottomRoom._isMiddle = false;
        bottomRoom._isNew = true;
        bottomRoom.tween.restart();
        bottomRoom.tween.resume();
      }
    }
  }

  setupPopup() {
    this.popupBgOverlay = this.add.image(0, 0, 'atlas1', 'overlay');
    this.popupBgOverlay.setOrigin(0);
    this.popupBgOverlay.setDisplaySize(
      Utils.GlobalSettings.width,
      Utils.GlobalSettings.height
    );
    this.popupBgOverlay.alpha = 0;
    this.popupBgOverlay.visible = false;
    this.popupBgOverlay.setTint(0x000000);
    this.popupBgOverlay.setInteractive();

    this.popupBackground = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.popupDialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2 - 20,
        y: 300,
        width: 430,
        height: 500,
      })
      .addBackground(this.popupBackground)
      .layout();
    this.popupDialog.setInteractive();

    this.popupCloseButton = this.add.sprite(
      this.popupDialog.x + 215,
      this.popupDialog.y - 250,
      'atlas1',
      'systembuttons10'
    );
    this.popupCloseButton.setInteractive();

    this.popupCloseButton.on('pointerover', (event) => {
      this.popupCloseButton.setFrame('systembuttons11');
    });

    this.popupCloseButton.on('pointerout', (event) => {
      this.popupCloseButton.setFrame('systembuttons10');
    });

    this.popupCloseButton.on('pointerup', () => {
      if (!Utils.SavedSettings.muted) {
        this.clickSnd.play();
      }
      this.hidePopup();
    });

    this.popupGreenRoom = this.createPopupRoomButton(
      this.popupDialog.x - 100,
      this.popupDialog.y - 160,
      'greenroom_small',
      Utils.GlobalSettings.text.mapExploration.green_room
    );

    this.popupBlueRoom = this.createPopupRoomButton(
      this.popupDialog.x - 100,
      this.popupDialog.y,
      'blueroom_small',
      Utils.GlobalSettings.text.mapExploration.blue_room
    );

    this.popupBossRoom = this.createPopupRoomButton(
      this.popupDialog.x - 100,
      this.popupDialog.y + 160,
      'bossroom_small',
      Utils.GlobalSettings.text.mapExploration.boss_room
    );

    this.popupShopRoom = this.createPopupRoomButton(
      this.popupDialog.x + 100,
      this.popupDialog.y - 160,
      'shop_small',
      Utils.GlobalSettings.text.mapExploration.shop_room
    );

    this.popupTreasureRoom = this.createPopupRoomButton(
      this.popupDialog.x + 100,
      this.popupDialog.y,
      'treasureroom_small',
      Utils.GlobalSettings.text.mapExploration.treasure_room
    );

    this.popupShrineRoom = this.createPopupRoomButton(
      this.popupDialog.x + 100,
      this.popupDialog.y + 160,
      'shrine_small',
      Utils.GlobalSettings.text.mapExploration.shrine_room
    );

    this.popupBossRoomLock = this.add.image(
      this.popupBossRoom.x,
      this.popupBossRoom.y,
      'atlas1',
      'locked'
    );

    this.popupRemoveBackground = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.popupRemoveDialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2 - 20,
        y: this.popupDialog.y + 270,
        width: 150,
        height: 50,
      })
      .addBackground(this.popupRemoveBackground)
      .layout();
    this.popupRemoveDialog.setInteractive();
    this.popupRemoveTxt = this.add.bitmapText(
      this.popupRemoveDialog.x,
      this.popupRemoveDialog.y,
      'sakkalbold',
      Utils.GlobalSettings.text.mapExploration.remove,
      30
    );
    this.popupRemoveTxt.setOrigin(0.5);

    this.popupRemoveDialog.on('pointerover', (event) => {
      this.popupRemoveTxt.setTint(0x00ff00);
    });

    this.popupRemoveDialog.on('pointerout', (event) => {
      this.popupRemoveTxt.setTint(0xffffff);
    });

    this.popupRemoveDialog.on('pointerup', () => {
      this.handleRemoveButtonClick();
    });

    this.togglePopupVisibility(false);
    this.togglePopupRemoveButtonVisibility(false);
  }

  togglePopupRemoveButtonVisibility(visible) {
    this.popupRemoveDialog.visible = visible;
    this.popupRemoveTxt.visible = visible;
  }

  togglePopupVisibility(visible) {
    this.popupBgOverlay.visible = visible;
    this.popupDialog.visible = visible;
    this.popupGreenRoom.visible = visible;
    this.popupGreenRoom.buttonTxt.visible = visible;
    this.popupBlueRoom.visible = visible;
    this.popupBlueRoom.buttonTxt.visible = visible;
    this.popupBossRoom.visible = visible;
    this.popupBossRoom.buttonTxt.visible = visible;
    this.popupBossRoomLock.visible = visible;
    this.popupShopRoom.visible = visible;
    this.popupShopRoom.buttonTxt.visible = visible;
    this.popupTreasureRoom.visible = visible;
    this.popupTreasureRoom.buttonTxt.visible = visible;
    this.popupShrineRoom.visible = visible;
    this.popupShrineRoom.buttonTxt.visible = visible;
    this.popupCloseButton.visible = visible;
  }

  togglePopupAlpha(alpha) {
    this.popupBgOverlay.alpha = alpha;
    this.popupDialog.alpha = alpha;
    this.popupGreenRoom.alpha = alpha;
    this.popupGreenRoom.buttonTxt.alpha = alpha;
    this.popupBlueRoom.alpha = alpha;
    this.popupBlueRoom.buttonTxt.alpha = alpha;
    this.popupBossRoom.alpha = alpha;
    this.popupBossRoom.buttonTxt.alpha = alpha;
    this.popupBossRoomLock.alpha = alpha;
    this.popupShopRoom.alpha = alpha;
    this.popupShopRoom.buttonTxt.alpha = alpha;
    this.popupTreasureRoom.alpha = alpha;
    this.popupTreasureRoom.buttonTxt.alpha = alpha;
    this.popupShrineRoom.alpha = alpha;
    this.popupShrineRoom.buttonTxt.alpha = alpha;
    this.popupCloseButton.alpha = alpha;
    this.popupRemoveDialog.alpha = alpha;
    this.popupRemoveTxt.alpha = alpha;
  }

  showPopup() {
    this.togglePopupVisibility(true);
    if (this.selectedCard && this.selectedCard.frame.name !== 'roomback2') {
      this.togglePopupRemoveButtonVisibility(true);
    }
    this.togglePopupAlpha(0);

    this.tweens.add({
      targets: [
        this.popupBgOverlay,
        this.popupDialog,
        this.popupGreenRoom,
        this.popupGreenRoom.buttonTxt,
        this.popupBlueRoom,
        this.popupBlueRoom.buttonTxt,
        this.popupBossRoom,
        this.popupBossRoom.buttonTxt,
        this.popupShopRoom,
        this.popupShopRoom.buttonTxt,
        this.popupTreasureRoom,
        this.popupTreasureRoom.buttonTxt,
        this.popupShrineRoom,
        this.popupShrineRoom.buttonTxt,
        this.popupCloseButton,
        this.popupRemoveDialog,
        this.popupRemoveTxt,
        this.popupBossRoomLock,
      ],
      alpha: { from: 0, to: 1.0 },
      ease: 'Linear.easeInOut',
      duration: 250,
    });
  }

  hidePopup() {
    this.togglePopupAlpha(1);

    this.tweens.add({
      targets: [
        this.popupBgOverlay,
        this.popupDialog,
        this.popupGreenRoom,
        this.popupGreenRoom.buttonTxt,
        this.popupBlueRoom,
        this.popupBlueRoom.buttonTxt,
        this.popupBossRoom,
        this.popupBossRoom.buttonTxt,
        this.popupShopRoom,
        this.popupShopRoom.buttonTxt,
        this.popupTreasureRoom,
        this.popupTreasureRoom.buttonTxt,
        this.popupShrineRoom,
        this.popupShrineRoom.buttonTxt,
        this.popupCloseButton,
        this.popupRemoveDialog,
        this.popupRemoveTxt,
        this.popupBossRoomLock,
      ],
      alpha: { from: 1.0, to: 0.0 },
      ease: 'Linear.easeInOut',
      duration: 100,
      onComplete: () => {
        this.togglePopupVisibility(false);
        this.togglePopupRemoveButtonVisibility(false);
      },
    });
  }

  createPopupRoomButton(x, y, frame, text) {
    var button = this.add.image(x, y, 'atlas1', frame);
    button.setScale(0.5);
    button.setInteractive();
    var buttonTxt = this.add.bitmapText(
      button.x,
      button.y + 65,
      'sakkalbold',
      text,
      25
    );
    buttonTxt.setOrigin(0.5);

    button.buttonTxt = buttonTxt;

    button.on('pointerover', (event) => {
      button.setTint(0x00ff00);
    });

    button.on('pointerout', (event) => {
      button.setTint(0xffffff);
    });

    button.on('pointerup', () => {
      this.handleRoomChosen(button);
    });

    return button;
  }

  handleRoomChosen(roomButton) {
    if (this.selectedCard) {
      if (roomButton.frame.name === 'bossroom_small') {
        UIHelper.showToast(
          this,
          this.toast,
          this.popupBossRoomLock.x,
          this.popupBossRoomLock.y,
          Utils.GlobalSettings.text.mapExploration.boss_key_required,
          500,
          250
        );
      } else {
        if (!Utils.SavedSettings.muted) {
          Utils.GlobalSettings.selectSound.play();
        }

        this.changeFrame(this.selectedCard, roomButton.frame.name);
        this.selectedCard.alpha = 1;
        this.selectedCard.tween.pause();
        this.hidePopup();

        Utils.GlobalSettings.gameState.rooms[this.selectedCard._roomId] = {
          id: this.selectedCard._roomId,
          roomFrame: this.selectedCard.frame.name,
          x: this.selectedCard.x,
          y: this.selectedCard.y,
          isMiddle: this.selectedCard._isMiddle,
          isCompleted: false,
          hasMystery: this.hasMysterySpot(this.selectedCard),
        };

        this.hasMysterySpot(this.selectedCard);

        this.toggleAdjacentRoomVisibility(false);
        this.toggleUnexploredRooms(true);

        this.movePartyLocation(this.selectedCard);

        var onePlayer = Utils.GlobalSettings.gameState.numPlayers === 1;

        if (Object.keys(Utils.GlobalSettings.gameState.rooms).length >= 2) {
          if (Object.keys(Utils.GlobalSettings.gameState.rooms).length === 2) {
            this.registry.set(
              'updateInfoTxt',
              onePlayer
                ? Utils.GlobalSettings.text.mapExploration
                    .instruction_one_player_flip
                : Utils.GlobalSettings.text.mapExploration
                    .instruction_second_turn_flip
            );
          } else {
            this.registry.set(
              'updateInfoTxt',
              onePlayer
                ? Utils.GlobalSettings.text.mapExploration
                    .instruction_one_player_flip
                : Utils.GlobalSettings.text.mapExploration
                    .instruction_other_turns_flip
            );
          }

          if (onePlayer) {
            this.skipButton.alpha = 0;
            this.skipButton.visible = false;
            this.skipButtonTxt.alpha = 0;
            this.skipButtonTxt.visible = false;
          } else {
            this.skipButton.alpha = 0;
            this.skipButton.visible = true;
            this.skipButtonTxt.alpha = 0;
            this.skipButtonTxt.visible = true;
            this.tweens.add({
              targets: [this.skipButton, this.skipButtonTxt],
              alpha: { from: 0, to: 1 },
              ease: 'Sine.easeInOut',
              duration: 500,
              delay: 0,
            });
          }
        }

        this.goButton.alpha = 0;
        this.goButton.visible = true;
        this.goButtonTxt.alpha = 0;
        this.goButtonTxt.visible = true;
        this.tweens.add({
          targets: [this.goButton, this.goButtonTxt],
          alpha: { from: 0, to: 1 },
          ease: 'Sine.easeInOut',
          duration: 500,
          delay: 0,
        });
      }
    }
  }

  handleRemoveButtonClick() {
    if (this.selectedCard) {
      if (!Utils.SavedSettings.muted) {
        this.removeSnd.play();
      }

      this.selectedCard.setFrame('roomback2');
      this.selectedCard.setDisplaySize(this.cardWidth, this.cardHeight);
      this.selectedCard.setSize(this.cardWidth, this.cardHeight);
      this.selectedCard.input.hitArea.setTo(
        0,
        0,
        this.selectedCard.width,
        this.selectedCard.height
      );

      this.selectedCard.alpha = 1;
      this.selectedCard.tween.restart();
      this.selectedCard.tween.resume();
      this.hidePopup();

      delete Utils.GlobalSettings.gameState.rooms[this.selectedCard._roomId];

      if (Object.keys(Utils.GlobalSettings.gameState.rooms).length === 0) {
        this.registry.set(
          'updateInfoTxt',
          Utils.GlobalSettings.text.mapExploration.instruction_first_turn
        );
        this.resetPartyLocation(false);
      } else {
        this.registry.set(
          'updateInfoTxt',
          Utils.GlobalSettings.text.mapExploration.instruction_other_turns
        );
        this.resetPartyLocation(true);
      }

      this.toggleAdjacentRoomVisibility(true);
      this.toggleUnexploredRooms(false);

      this.goButton.alpha = 1;
      this.goButtonTxt.alpha = 1;
      this.tweens.add({
        targets: [this.goButton, this.goButtonTxt],
        alpha: { from: 1, to: 0 },
        ease: 'Sine.easeInOut',
        duration: 500,
        delay: 0,
        onComplete: () => {
          this.goButton.visible = false;
          this.goButtonTxt.visible = false;
        },
      });

      if (this.skipButton.visible) {
        this.skipButton.alpha = 1;
        this.skipButtonTxt.alpha = 1;
        this.tweens.add({
          targets: [this.skipButton, this.skipButtonTxt],
          alpha: { from: 1, to: 0 },
          ease: 'Sine.easeInOut',
          duration: 500,
          delay: 0,
          onComplete: () => {
            this.skipButton.visible = false;
            this.skipButtonTxt.visible = false;
          },
        });
      }

      this.selectedCard = null;
    }
  }

  movePartyLocation(room) {
    this.partyLocation.visible = true;
    this.tweens.add({
      targets: [this.partyLocation],
      x: room.x + room.width / 2,
      y: room.y + room.height / 2,
      ease: 'Sine.easeInOut',
      duration: 500,
    });
  }

  resetPartyLocation(setVisible) {
    this.tweens.add({
      targets: [this.partyLocation],
      x: this.startingPartyLocationX,
      y: this.startingPartyLocationY,
      ease: 'Sine.easeInOut',
      duration: 500,
      onComplete: () => {
        this.partyLocation.visible = setVisible;
      },
    });
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {}

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      for (var i = 0; i < this.bg.length; i++) {
        if (this.bg[i]) {
          this.bg[i].destroy();
          this.bg[i] = null;
        }
      }
      this.bg = null;
    }

    if (this.mysterySpots) {
      for (var i = 0; i < this.mysterySpots.length; i++) {
        if (this.mysterySpots[i]) {
          this.mysterySpots[i].destroy();
          this.mysterySpots[i] = null;
        }
      }
      this.mysterySpots = null;
    }

    if (this.cardGroup) {
      this.cardGroup.destroy(true);
      this.cardGroup = null;
    }

    this.midCard = null;

    if (this.popupBackground) {
      this.popupBackground.destroy();
      this.popupBackground = null;
    }

    if (this.popupDialog) {
      this.popupDialog.destroy();
      this.popupDialog = null;
    }

    if (this.popupCloseButton) {
      this.popupCloseButton.destroy();
      this.popupCloseButton = null;
    }

    if (this.popupGreenRoom && this.popupGreenRoom.buttonTxt) {
      this.popupGreenRoom.buttonTxt.destroy();
      this.popupGreenRoom.buttonTxt = null;
      this.popupGreenRoom.destroy();
      this.popupGreenRoom = null;
    }

    if (this.popupBlueRoom && this.popupBlueRoom.buttonTxt) {
      this.popupBlueRoom.buttonTxt.destroy();
      this.popupBlueRoom.buttonTxt = null;
      this.popupBlueRoom.destroy();
      this.popupBlueRoom = null;
    }

    if (this.popupBossRoom && this.popupBossRoom.buttonTxt) {
      this.popupBossRoom.buttonTxt.destroy();
      this.popupBossRoom.buttonTxt = null;
      this.popupBossRoom.destroy();
      this.popupBossRoom = null;
    }

    if (this.popupShopRoom && this.popupShopRoom.buttonTxt) {
      this.popupShopRoom.buttonTxt.destroy();
      this.popupShopRoom.buttonTxt = null;
      this.popupShopRoom.destroy();
      this.popupShopRoom = null;
    }

    if (this.popupTreasureRoom && this.popupTreasureRoom.buttonTxt) {
      this.popupTreasureRoom.buttonTxt.destroy();
      this.popupTreasureRoom.buttonTxt = null;
      this.popupTreasureRoom.destroy();
      this.popupTreasureRoom = null;
    }

    if (this.popupShrineRoom && this.popupShrineRoom.buttonTxt) {
      this.popupShrineRoom.buttonTxt.destroy();
      this.popupShrineRoom.buttonTxt = null;
      this.popupShrineRoom.destroy();
      this.popupShrineRoom = null;
    }

    if (this.popupRemoveBackground) {
      this.popupRemoveBackground.destroy();
      this.popupRemoveBackground = null;
    }

    if (this.popupRemoveDialog) {
      this.popupRemoveDialog.destroy();
      this.popupRemoveDialog = null;
    }

    if (this.popupRemoveTxt) {
      this.popupRemoveTxt.destroy();
      this.popupRemoveTxt = null;
    }

    if (this.goButton) {
      this.goButton.destroy();
      this.goButton = null;
    }

    if (this.goButtonTxt) {
      this.goButtonTxt.destroy();
      this.goButtonTxt = null;
    }

    if (this.skipButton) {
      this.skipButton.destroy();
      this.skipButton = null;
    }

    if (this.skipButtonTxt) {
      this.skipButtonTxt.destroy();
      this.skipButtonTxt = null;
    }

    if (this.partyLocation) {
      this.partyLocation.destroy();
      this.partyLocation = null;
    }

    if (this.toast) {
      this.toast.destroy();
      this.toast = null;
    }

    if (this.clickSnd) {
      this.clickSnd.destroy();
      this.clickSnd = null;
    }

    if (this.selectSnd) {
      this.selectSnd.destroy();
      this.selectSnd = null;
    }

    if (this.removeSnd) {
      this.removeSnd.destroy();
      this.removeSnd = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill map');
  }
}
