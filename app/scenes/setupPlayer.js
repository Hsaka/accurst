import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import NameGenerator from 'utils/nameGenerator';

export default class SetupPlayerScene extends Phaser.Scene {
  constructor() {
    super('setupPlayer');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    this.clickSnd = this.sound.add('click');
    this.selectSnd = this.sound.add('uiSelect');

    Utils.GlobalSettings.editingText = true;

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'bg1');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.title = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      100,
      'sakkalbold',
      Utils.GlobalSettings.text.setupPlayer.title +
        ' ' +
        (Utils.GlobalSettings.gameState.currentPlayerSetup + 1),
      80
    );
    this.title.setOrigin(0.5);
    this.screenGroup.add(this.title);

    this.existingPlayer = undefined;
    if (
      Utils.GlobalSettings.gameState.players &&
      Utils.GlobalSettings.gameState.players.length >
        Utils.GlobalSettings.gameState.currentPlayerSetup
    ) {
      this.existingPlayer =
        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ];
    }

    this.playerNameLabel = this.add.bitmapText(
      85,
      210,
      'sakkalbold',
      Utils.GlobalSettings.text.setupPlayer.enter_name,
      40
    );
    this.playerNameLabel.setOrigin(0);
    this.screenGroup.add(this.playerNameLabel);

    var gen = new NameGenerator();
    var names = this.cache.json.get('names');
    gen.setNameSet(names, 'fantasy');

    this.playerName = gen.generate_name('fantasy');
    if (this.existingPlayer) {
      this.playerName = this.existingPlayer.name;
    }
    names = null;
    this.setupTextBox();

    this.playerClass = 0;
    this.playerClassLabel = this.add.bitmapText(
      85,
      350,
      'sakkalbold',
      Utils.GlobalSettings.text.setupPlayer.select_class,
      40
    );
    this.playerClassLabel.setOrigin(0);
    this.screenGroup.add(this.playerClassLabel);

    this.setupClassList();
    this.setupClassStats();

    this.backButton = UIHelper.createNavButton(this, 50, 'back');
    this.backButton.alpha = 0;
    this.backButton.on('pointerup', () => {
      if (!Utils.SavedSettings.muted) {
        Utils.GlobalSettings.backSound.play();
      }

      if (Utils.GlobalSettings.gameState.currentPlayerSetup > 0) {
        Utils.GlobalSettings.gameState.currentPlayerSetup--;
        Utils.saveGameState();
        this.scene.start('setupPlayer', { from: 'setupPlayer' });
      } else {
        this.scene.start('playerSelect', { from: 'setupPlayer' });
      }
    });

    this.tweens.add({
      targets: [this.backButton],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 500,
      delay: 0
    });

    if (this.initData.from === 'confirmPlayers') {
      this.backButton.visible = false;
    }

    this.goButton = UIHelper.createNavButton(
      this,
      Utils.GlobalSettings.width - 50,
      'forward'
    );
    this.goButton.alpha = 0;
    this.goButton.visible = false;
    this.goButton.on('pointerup', () => {
      if (!Utils.SavedSettings.muted) {
        Utils.GlobalSettings.selectSound.play();
      }

      if (!this.existingPlayer) {
        Utils.GlobalSettings.gameState.players.push({
          name: this.playerName,
          classType: this.playerClass,
          health: Utils.GlobalSettings.heroList[this.playerClass].health,
          magic: Utils.GlobalSettings.heroList[this.playerClass].magic,
          power: Utils.GlobalSettings.heroList[this.playerClass].power,
          maxHealth: Utils.GlobalSettings.heroList[this.playerClass].health,
          maxMagic: Utils.GlobalSettings.heroList[this.playerClass].magic,
          maxPower: Utils.GlobalSettings.heroList[this.playerClass].power,
          defenseBonus: 0,
          powerBonus: 0,
          experience: 0,
          speed: Utils.GlobalSettings.heroList[this.playerClass].speed,
          requiredExperience: Utils.getEXP(
            2,
            Utils.GlobalSettings.heroList[this.playerClass].exp_rate
          ),
          level: 1,
          deathSaveCount: 0,
          gold: Utils.GlobalSettings.heroList[this.playerClass].gold,
          skills: Utils.GlobalSettings.heroList[this.playerClass].skills,
          items: Utils.GlobalSettings.heroList[this.playerClass].items,
          status: Utils.GlobalSettings.text.battle.status_normal,
          weaponProficiency:
            Utils.GlobalSettings.heroList[this.playerClass].weapon_proficiency,
          weapon: Utils.GlobalSettings.heroList[this.playerClass].weapon
        });
      } else {
        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].name = this.playerName;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].classType = this.playerClass;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].health = Utils.GlobalSettings.heroList[this.playerClass].health;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].magic = Utils.GlobalSettings.heroList[this.playerClass].magic;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].power = Utils.GlobalSettings.heroList[this.playerClass].power;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].maxHealth = Utils.GlobalSettings.heroList[this.playerClass].health;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].maxMagic = Utils.GlobalSettings.heroList[this.playerClass].magic;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].maxPower = Utils.GlobalSettings.heroList[this.playerClass].power;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].skills = Utils.GlobalSettings.heroList[this.playerClass].skills;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].items = Utils.GlobalSettings.heroList[this.playerClass].items;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].gold = Utils.GlobalSettings.heroList[this.playerClass].gold;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].speed = Utils.GlobalSettings.heroList[this.playerClass].speed;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].weapon = Utils.GlobalSettings.heroList[this.playerClass].weapon;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].weaponProficiency =
          Utils.GlobalSettings.heroList[this.playerClass].weapon_proficiency;

        Utils.GlobalSettings.gameState.players[
          Utils.GlobalSettings.gameState.currentPlayerSetup
        ].requiredExperience = Utils.getEXP(
          2,
          Utils.GlobalSettings.heroList[this.playerClass].exp_rate
        );
      }

      Utils.GlobalSettings.gameState.currentPlayerSetup++;
      Utils.saveGameState();

      if (this.initData.from === 'confirmPlayers') {
        this.scene.start('confirmPlayers', { from: 'setupPlayer' });
      } else {
        if (
          Utils.GlobalSettings.gameState.currentPlayerSetup >=
          Utils.GlobalSettings.gameState.numPlayers
        ) {
          this.scene.start('confirmPlayers', { from: 'setupPlayer' });
        } else {
          this.scene.start('setupPlayer', { from: 'setupPlayer' });
        }
      }
    });

    if (this.existingPlayer) {
      this.setClassSelected(
        this.existingPlayer.classType,
        this.getClassButtonByIndex(this.existingPlayer.classType)
      );

      this.goButton.visible = true;
      this.tweens.add({
        targets: [this.goButton],
        alpha: { from: 0, to: 1 },
        ease: 'Sine.easeInOut',
        duration: 500,
        delay: 0
      });
    }

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI');
      this.scene.bringToTop('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI');
      this.scene.bringToTop('gameUI');
    }
  }

  setupTextBox() {
    this.textBoxField = this.rexUI.add
      .label({
        orientation: 'x',
        background: this.rexUI.add
          .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
          .setStrokeStyle(2, UIHelper.COLOR_LIGHT),
        text: this.rexUI.add.BBCodeText(0, 0, this.playerName, {
          fixedWidth: 450,
          fixedHeight: 46,
          fontSize: '20px',
          valign: 'center'
        }),
        space: { top: 5, bottom: 5, left: 5, right: 5, icon: 10 }
      })
      .setInteractive()
      .on('pointerdown', () => {
        var config = {
          onTextChanged: (textObject, text) => {
            this.playerName = text;
            textObject.text = text;
          }
        };
        this.rexUI.edit(this.textBoxField.getElement('text'), config);
      });

    this.textboxDialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2,
        y: 450,
        width: 500,
        height: 400
      })
      .add(
        this.textBoxField,
        0,
        'left',
        { bottom: 10, left: 10, right: 10 },
        true
      )
      .layout();
  }

  createClassButton(x, y, frame, text) {
    var button = this.add.image(x, y, 'atlas1', frame);
    button.setScale(0.4);
    button.setInteractive();

    var buttonTxt = this.add.bitmapText(
      button.x,
      button.y + 70,
      'sakkalbold',
      text,
      30
    );
    buttonTxt.setOrigin(0.5);

    button.buttonTxt = buttonTxt;

    button.on('pointerover', event => {
      button.setScale(0.35);
    });

    button.on('pointerout', event => {
      button.setScale(0.4);
    });

    return button;
  }

  setupClassList() {
    this.classBackground = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.classDialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2,
        y: 560,
        width: 480,
        height: 340
      })
      .addBackground(this.classBackground)
      .layout();

    this.classKnight = this.createClassButton(
      this.classDialog.x - 150,
      this.classDialog.y - 90,
      Utils.GlobalSettings.heroList[0].image,
      Utils.GlobalSettings.heroList[0].name
    );

    this.classKnight.on('pointerup', () => {
      this.setClassSelected(0, this.classKnight);
    });

    this.classWizard = this.createClassButton(
      this.classDialog.x,
      this.classDialog.y - 90,
      Utils.GlobalSettings.heroList[1].image,
      Utils.GlobalSettings.heroList[1].name
    );

    this.classWizard.on('pointerup', () => {
      this.setClassSelected(1, this.classWizard);
    });

    this.classMonk = this.createClassButton(
      this.classDialog.x + 150,
      this.classDialog.y - 90,
      Utils.GlobalSettings.heroList[2].image,
      Utils.GlobalSettings.heroList[2].name
    );

    this.classMonk.on('pointerup', () => {
      this.setClassSelected(2, this.classMonk);
    });

    this.classRanger = this.createClassButton(
      this.classDialog.x - 150,
      this.classDialog.y + 80,
      Utils.GlobalSettings.heroList[3].image,
      Utils.GlobalSettings.heroList[3].name
    );

    this.classRanger.on('pointerup', () => {
      this.setClassSelected(3, this.classRanger);
    });

    this.classRogue = this.createClassButton(
      this.classDialog.x,
      this.classDialog.y + 80,
      Utils.GlobalSettings.heroList[4].image,
      Utils.GlobalSettings.heroList[4].name
    );

    this.classRogue.on('pointerup', () => {
      this.setClassSelected(4, this.classRogue);
    });

    this.classBard = this.createClassButton(
      this.classDialog.x + 150,
      this.classDialog.y + 80,
      Utils.GlobalSettings.heroList[5].image,
      Utils.GlobalSettings.heroList[5].name
    );

    this.classBard.on('pointerup', () => {
      this.setClassSelected(5, this.classBard);
    });
  }

  getClassButtonByIndex(index) {
    switch (index) {
      case 0:
        return this.classKnight;
        break;

      case 1:
        return this.classWizard;
        break;

      case 2:
        return this.classMonk;
        break;

      case 3:
        return this.classRanger;
        break;

      case 4:
        return this.classRogue;
        break;

      case 5:
        return this.classBard;
        break;
    }
  }

  setClassSelected(classType, selectedClass) {
    if (!Utils.SavedSettings.muted) {
      this.selectSnd.play();
    }

    this.playerClass = classType;

    this.classKnight.setTint(0xffffff);
    this.classWizard.setTint(0xffffff);
    this.classMonk.setTint(0xffffff);
    this.classRanger.setTint(0xffffff);
    this.classRogue.setTint(0xffffff);
    this.classBard.setTint(0xffffff);

    var hero = Utils.GlobalSettings.heroList[classType];
    this.setStats(hero.health, hero.power, hero.magic, hero.description);

    selectedClass.setTint(0x00ff00);

    this.toggleStatsPanel(true);

    if (!this.goButton.visible) {
      this.goButton.visible = true;
      this.tweens.add({
        targets: [this.goButton],
        alpha: { from: 0, to: 1 },
        ease: 'Sine.easeInOut',
        duration: 500,
        delay: 0
      });
    }
  }

  setupClassStats() {
    this.statsBackground = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.statsDialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2,
        y: 830,
        width: 350,
        height: 160
      })
      .addBackground(this.statsBackground)
      .layout();

    this.healthRune = this.add.image(
      this.statsDialog.x - 130,
      this.statsDialog.y - 30,
      'atlas1',
      'rune_health'
    );
    this.healthRune.setScale(0.5);
    this.healthRuneAmountTxt = this.add.bitmapText(
      this.healthRune.x,
      this.healthRune.y,
      'sakkalbold',
      '50',
      30
    );
    this.healthRuneAmountTxt.setOrigin(0.5);

    this.healthRuneTxt = this.add.bitmapText(
      this.healthRune.x,
      this.healthRune.y + 30,
      'sakkalbold',
      Utils.GlobalSettings.text.setupPlayer.health,
      35
    );
    this.healthRuneTxt.setOrigin(0.5);

    this.powerRune = this.add.image(
      this.statsDialog.x,
      this.statsDialog.y - 30,
      'atlas1',
      'rune_power'
    );
    this.powerRune.setScale(0.5);

    this.powerRuneAmountTxt = this.add.bitmapText(
      this.powerRune.x,
      this.powerRune.y,
      'sakkalbold',
      '50',
      30
    );
    this.powerRuneAmountTxt.setOrigin(0.5);

    this.powerRuneTxt = this.add.bitmapText(
      this.powerRune.x,
      this.powerRune.y + 30,
      'sakkalbold',
      Utils.GlobalSettings.text.setupPlayer.power,
      35
    );
    this.powerRuneTxt.setOrigin(0.5);

    this.magicRune = this.add.image(
      this.statsDialog.x + 130,
      this.statsDialog.y - 30,
      'atlas1',
      'rune_magic'
    );
    this.magicRune.setScale(0.5);

    this.magicRuneAmountTxt = this.add.bitmapText(
      this.magicRune.x,
      this.magicRune.y,
      'sakkalbold',
      '50',
      30
    );
    this.magicRuneAmountTxt.setOrigin(0.5);

    this.magicRuneTxt = this.add.bitmapText(
      this.magicRune.x,
      this.magicRune.y + 30,
      'sakkalbold',
      Utils.GlobalSettings.text.setupPlayer.magic,
      35
    );
    this.magicRuneTxt.setOrigin(0.5);

    this.flavourTxt = this.add.bitmapText(
      this.statsDialog.x,
      this.statsDialog.y + 50,
      'sakkalbold',
      '',
      35
    );
    this.flavourTxt.setOrigin(0.5);

    this.toggleStatsPanel(false);
  }

  toggleStatsPanel(visible) {
    this.statsBackground.visible = visible;
    this.statsDialog.visible = visible;
    this.healthRune.visible = visible;
    this.healthRuneAmountTxt.visible = visible;
    this.healthRuneTxt.visible = visible;
    this.powerRune.visible = visible;
    this.powerRuneAmountTxt.visible = visible;
    this.powerRuneTxt.visible = visible;
    this.magicRune.visible = visible;
    this.magicRuneAmountTxt.visible = visible;
    this.magicRuneTxt.visible = visible;
    this.flavourTxt.visible = visible;
  }

  setStats(hp, pow, mp, desc) {
    this.healthRuneAmountTxt.setText('' + hp);
    this.powerRuneAmountTxt.setText('' + pow);
    this.magicRuneAmountTxt.setText('' + mp);
    this.flavourTxt.setText('' + desc);
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    Utils.GlobalSettings.editingText = false;

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.title) {
      this.title.destroy();
      this.title = null;
    }

    if (this.playerClassLabel) {
      this.playerClassLabel.destroy();
      this.playerClassLabel = null;
    }

    if (this.textBoxField) {
      this.textBoxField.destroy();
      this.textBoxField = null;
    }

    if (this.textboxDialog) {
      this.textboxDialog.destroy();
      this.textboxDialog = null;
    }

    if (this.classBackground) {
      this.classBackground.destroy();
      this.classBackground = null;
    }

    if (this.classDialog) {
      this.classDialog.destroy();
      this.classDialog = null;
    }

    if (this.clickSnd) {
      this.clickSnd.destroy();
      this.clickSnd = null;
    }

    if (this.playerNameLabel) {
      this.playerNameLabel.destroy();
      this.playerNameLabel = null;
    }

    if (this.backButton) {
      this.backButton.destroy();
      this.backButton = null;
    }

    if (this.goButton) {
      this.goButton.destroy();
      this.goButton = null;
    }

    if (this.classKnight && this.classKnight.buttonTxt) {
      this.classKnight.buttonTxt.destroy();
      this.classKnight.buttonTxt = null;
      this.classKnight.destroy();
      this.classKnight = null;
    }

    if (this.classWizard && this.classWizard.buttonTxt) {
      this.classWizard.buttonTxt.destroy();
      this.classWizard.buttonTxt = null;
      this.classWizard.destroy();
      this.classWizard = null;
    }

    if (this.classMonk && this.classMonk.buttonTxt) {
      this.classMonk.buttonTxt.destroy();
      this.classMonk.buttonTxt = null;
      this.classMonk.destroy();
      this.classMonk = null;
    }

    if (this.classRanger && this.classRanger.buttonTxt) {
      this.classRanger.buttonTxt.destroy();
      this.classRanger.buttonTxt = null;
      this.classRanger.destroy();
      this.classRanger = null;
    }

    if (this.classRogue && this.classRogue.buttonTxt) {
      this.classRogue.buttonTxt.destroy();
      this.classRogue.buttonTxt = null;
      this.classRogue.destroy();
      this.classRogue = null;
    }

    if (this.classBard && this.classBard.buttonTxt) {
      this.classBard.buttonTxt.destroy();
      this.classBard.buttonTxt = null;
      this.classBard.destroy();
      this.classBard = null;
    }

    if (this.statsBackground) {
      this.statsBackground.destroy();
      this.statsBackground = null;
    }

    if (this.statsDialog) {
      this.statsDialog.destroy();
      this.statsDialog = null;
    }

    if (this.healthRune) {
      this.healthRune.destroy();
      this.healthRune = null;
    }

    if (this.healthRuneAmountTxt) {
      this.healthRuneAmountTxt.destroy();
      this.healthRuneAmountTxt = null;
    }

    if (this.healthRuneTxt) {
      this.healthRuneTxt.destroy();
      this.healthRuneTxt = null;
    }

    if (this.powerRune) {
      this.powerRune.destroy();
      this.powerRune = null;
    }

    if (this.powerRuneAmountTxt) {
      this.powerRuneAmountTxt.destroy();
      this.powerRuneAmountTxt = null;
    }

    if (this.powerRuneTxt) {
      this.powerRuneTxt.destroy();
      this.powerRuneTxt = null;
    }

    if (this.magicRune) {
      this.magicRune.destroy();
      this.magicRune = null;
    }

    if (this.magicRuneAmountTxt) {
      this.magicRuneAmountTxt.destroy();
      this.magicRuneAmountTxt = null;
    }

    if (this.magicRuneTxt) {
      this.magicRuneTxt.destroy();
      this.magicRuneTxt = null;
    }

    if (this.flavourTxt) {
      this.flavourTxt.destroy();
      this.flavourTxt = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill setup player');
  }
}
