import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import SwapWeaponPanel from 'utils/shared/swapWeaponPanel';
import GameCalculations from 'utils/gameCalculations';

export default class TreasureScene extends Phaser.Scene {
  constructor() {
    super('treasure');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    if (!Utils.GlobalSettings.bgm.isPlaying) {
      Utils.GlobalSettings.bgm.setVolume(0.2);
      Utils.GlobalSettings.bgm.setLoop(true);
      Utils.GlobalSettings.bgm.play();
    }

    if (Utils.SavedSettings.muted) {
      Utils.GlobalSettings.bgm.pause();
    }

    this.clickSnd = this.sound.add('click');
    this.turnSnd = this.sound.add('uiTurn');

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'treasure');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.swapWeaponPanel = new SwapWeaponPanel(this);

    this.currentPlayer =
      Utils.GlobalSettings.gameState.players[
        Utils.GlobalSettings.gameState.turn
      ];

    this.weaponID = -1;

    this.endsWithS = UIHelper.endsWithS(this.currentPlayer.name);

    this.infoDialog = UIHelper.createTextDialog(
      this,
      Utils.GlobalSettings.width / 2,
      50,
      500,
      75,
      this.endsWithS
        ? this.currentPlayer.name +
            "' " +
            Utils.GlobalSettings.text.treasure.turn
        : this.currentPlayer.name +
            "'s " +
            Utils.GlobalSettings.text.treasure.turn
    );
    this.infoDialog.textContent.alpha = 0;
    this.infoDialog.setScale(0);

    this.playerIcon = this.add.image(
      this.infoDialog.x - 250,
      this.infoDialog.y,
      'atlas1',
      Utils.GlobalSettings.heroList[this.currentPlayer.classType].image
    );
    this.playerIcon.setScale(0);

    this.tweens.add({
      targets: [this.infoDialog],
      scale: { from: 0, to: 1 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    this.tweens.add({
      targets: [this.playerIcon],
      scale: { from: 0, to: 0.3 },
      ease: 'Back.easeOut',
      duration: 500,
    });

    this.tweens.add({
      targets: [this.infoDialog.textContent],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 1000,
    });

    if (!Utils.SavedSettings.muted) {
      this.turnSnd.play();
    }

    this.instructionBackground = this.rexUI.add
      .roundRectangle(0, 0, 10, 10, 10, UIHelper.COLOR_DARK)
      .setStrokeStyle(2, UIHelper.COLOR_LIGHT);

    this.instructionDialog = this.rexUI.add
      .sizer({
        orientation: 'y',
        x: Utils.GlobalSettings.width / 2,
        y: 210,
        width: 600,
        height: 120,
      })
      .addBackground(this.instructionBackground)
      .layout();
    this.instructionDialog.alpha = 0;

    this.instructionTxt = this.add.bitmapText(
      this.instructionDialog.x,
      this.instructionDialog.y,
      'sakkal',
      Utils.GlobalSettings.text.treasure.instructions,
      40
    );
    this.instructionTxt.alpha = 0;
    this.instructionTxt.setOrigin(0.5);

    this.tweens.add({
      targets: [this.instructionDialog, this.instructionTxt],
      alpha: { from: 0, to: 1 },
      ease: 'Linear.easeOut',
      duration: 500,
      delay: 500,
    });

    this.particles = this.add.particles('effectAtlas1');
    var circle = new Phaser.Geom.Circle(0, 0, 50);

    this.particles.createEmitter({
      frame: 'circle_05',
      x: Utils.GlobalSettings.width / 2,
      y: Utils.GlobalSettings.height / 2 + 50,
      blendMode: 'SCREEN',
      scale: { start: 0.5, end: 0 },
      speed: { min: -100, max: 100 },
      quantity: 10,
      tint: [0xff0000],
      emitZone: { type: 'edge', source: circle, quantity: 10 },
    });

    this.particles.createEmitter({
      frame: 'circle_05',
      x: Utils.GlobalSettings.width / 2 - 200,
      y: Utils.GlobalSettings.height / 2 + 225,
      blendMode: 'SCREEN',
      scale: { start: 0.5, end: 0 },
      speed: { min: -100, max: 100 },
      quantity: 10,
      tint: [0x00ff00],
      emitZone: { type: 'edge', source: circle, quantity: 10 },
    });

    this.particles.createEmitter({
      frame: 'circle_05',
      x: Utils.GlobalSettings.width / 2 + 200,
      y: Utils.GlobalSettings.height / 2 + 225,
      blendMode: 'SCREEN',
      scale: { start: 0.5, end: 0 },
      speed: { min: -100, max: 100 },
      quantity: 10,
      tint: [0x0000ff],
      emitZone: { type: 'edge', source: circle, quantity: 10 },
    });

    this.treasureChests = [];
    this.treasurePanels = [];

    for (var i = 0; i < 3; i++) {
      var spr = this.add.image(
        Utils.GlobalSettings.width / 2 - 200 + i * 200,
        Utils.GlobalSettings.height / 2 + 50,
        'atlas1',
        i === 0 ? 'green_chest' : i === 1 ? 'red_chest' : 'blue_chest'
      );
      spr.setOrigin(0.5);
      if (i === 0 || i === 2) {
        spr.y = Utils.GlobalSettings.height / 2 + 225;
      }
      spr.setScale(0);
      spr.alpha = 0;
      spr.setInteractive();
      this.treasureChests.push(spr);

      var spr2 = UIHelper.createTextDialog(
        this,
        spr.x,
        spr.y + 100,
        120,
        50,
        i === 0 ? 'Green Chest' : i === 1 ? 'Red Chest' : 'Blue Chest',
        'dialog9patch3',
        15,
        32
      );
      spr2.setScale(0);
      spr2.alpha = 0;
      spr2.textContent.alpha = 0;
      this.treasurePanels.push(spr2);

      spr.on(
        'pointerover',
        function (spr) {
          spr.setTint(0x555555);
        }.bind(this, spr)
      );

      spr.on(
        'pointerout',
        function (spr) {
          spr.setTint(0xffffff);
        }.bind(this, spr)
      );

      spr.on(
        'pointerup',
        function (spr, spr2) {
          for (var j = 0; j < this.treasureChests.length; j++) {
            this.treasureChests[j].disableInteractive();
            this.tweens.add({
              targets: [
                this.treasureChests[j],
                this.treasurePanels[j],
                ,
                this.treasurePanels[j].textContent,
              ],
              alpha: { from: 1, to: 0 },
              scale: { from: 1, to: 0 },
              ease: 'Back.easeOut',
              duration: 500,
            });
          }

          this.weaponID = GameCalculations.getWeaponReward(
            spr.frame.name,
            this.currentPlayer.weapon,
            this.currentPlayer.weaponProficiency
          );
          this.swapWeaponPanel.showSwapWeaponPanel(200, this.weaponID);
        }.bind(this, spr, spr2)
      );

      this.tweens.add({
        targets: [spr, spr2],
        alpha: { from: 0, to: 1 },
        scale: { from: 0, to: 1 },
        ease: 'Back.easeOut',
        duration: 500,
        delay: 500 * i,
      });

      this.tweens.add({
        targets: [spr2.textContent],
        alpha: { from: 0, to: 1 },
        ease: 'Linear.easeOut',
        duration: 500,
        delay: 500 * i,
      });
    }

    this.goButton = UIHelper.createNavButton(
      this,
      Utils.GlobalSettings.width - 50,
      'forward'
    );
    this.goButton.alpha = 0;
    this.goButton.on('pointerup', () => {
      if (!Utils.SavedSettings.muted) {
        Utils.GlobalSettings.selectSound.play();
      }

      this.exitRoom();
    });

    this.tweens.add({
      targets: [this.goButton],
      alpha: { from: 0, to: 1 },
      ease: 'Sine.easeInOut',
      duration: 500,
      delay: 1500,
    });

    this.swapWeaponPanel.setupSwapWeaponPanel(this.currentPlayer);

    this.swapWeaponPanel.onConfirmPressed = () => {
      if (this.weaponID !== -1) {
        this.currentPlayer.weapon = this.weaponID;
      }
      this.exitRoom();
    };

    this.swapWeaponPanel.onCancelPressed = () => {
      this.exitRoom();
    };

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI');
    }
    this.scene.bringToTop('gameUI');
  }

  exitRoom() {
    Utils.GlobalSettings.gameState.rooms[
      Utils.GlobalSettings.gameState.lastRoomId
    ].isCompleted = true;
    Utils.GlobalSettings.gameState.encounter = {};
    Utils.GlobalSettings.gameState.phase = 'exploration';

    Utils.GlobalSettings.gameState.turn++;
    if (
      Utils.GlobalSettings.gameState.turn >=
      Utils.GlobalSettings.gameState.numPlayers
    ) {
      Utils.GlobalSettings.gameState.turn = 0;
    }
    Utils.saveGameState();

    this.scene.start('mapExploration', { from: 'treasure' });
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
      if (data) {
        Utils.GlobalSettings.bgm.pause();
      } else {
        Utils.GlobalSettings.bgm.resume();
      }
    } else if (key === 'home' && data) {
      this.registry.set('showHome', false);
      this.registry.set('showParty', false);
      this.scene.start('main', { from: 'treasure' });
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.clickSnd) {
      this.clickSnd.destroy();
      this.clickSnd = null;
    }

    if (this.turnSnd) {
      this.turnSnd.destroy();
      this.turnSnd = null;
    }

    this.currentPlayer = null;

    if (this.infoDialog) {
      this.infoDialog.destroy();
      this.infoDialog = null;
    }

    if (this.playerIcon) {
      this.playerIcon.destroy();
      this.playerIcon = null;
    }

    if (this.instructionBackground) {
      this.instructionBackground.destroy();
      this.instructionBackground = null;
    }

    if (this.instructionDialog) {
      this.instructionDialog.destroy();
      this.instructionDialog = null;
    }

    if (this.instructionTxt) {
      this.instructionTxt.destroy();
      this.instructionTxt = null;
    }

    if (this.particles) {
      this.particles.destroy();
      this.particles = null;
    }

    if (this.goButton) {
      this.goButton.destroy();
      this.goButton = null;
    }

    for (var i = 0; i < this.treasureChests.length; i++) {
      if (this.treasureChests[i] && this.treasurePanels[i]) {
        this.treasureChests[i].destroy();
        this.treasurePanels[i].destroy();

        this.treasureChests[i] = null;
        this.treasurePanels[i] = null;
      }
    }

    this.treasureChests = null;
    this.treasurePanels = null;

    if (this.swapWeaponPanel) {
      this.swapWeaponPanel.destroy();
      this.swapWeaponPanel = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill treasure room');
  }
}
